<?php include("includes/session.php"); ?>
<?php include("includes/connection.php"); ?>
<?php include("includes/functions.php"); ?>
<!DOCTYPE html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> 
<![endif]-->
<!--[if IE 7]> <html class="no-js lt-ie9 lt-ie8" lang="en"> 
<![endif]-->
<!--[if IE 8]> <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
    <title>Universe - Education for All</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Prototype of ID assignment">
    <meta name="author" content="Esmet">
    <meta charset="UTF-8">

    <link href='css/googlefont.css' rel='stylesheet' type='text/css'>
        
    <!-- CSS Bootstrap & Custom -->
    <link href="css/bootstrap.css" rel="stylesheet" media="screen">
    <link href="css/font-awesome.min.css" rel="stylesheet" media="screen">
    <link href="css/animate.css" rel="stylesheet" media="screen">
    
    <link href="style.css" rel="stylesheet" media="screen">
        
    <!-- Favicons -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="img/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="img/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="img/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="img/apple-touch-icon-57-precomposed.png">
    <link rel="shortcut icon" href="img/favicon.ico">
    
    <!-- JavaScripts -->
    <script src="js/jquery-1.10.2.min.js"></script>
    <script src="js/jquery-migrate-1.2.1.min.js"></script>
    <script src="js/modernizr.js"></script>
    <!--[if lt IE 8]>
	<div style=' clear: both; text-align:center; position: relative;'>
            <a href="http://www.microsoft.com/windows/internet-explorer/default.aspx?ocid=ie6_countdown_bannercode"><img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" alt="" /></a>
        </div>
    <![endif]-->
</head>
<body>

    <!-- This one in here is responsive menu for tablet and mobiles -->
    <div class="responsive-navigation visible-sm visible-xs">
        <a href="#" class="menu-toggle-btn">
            <i class="fa fa-bars"></i>
        </a>
        <div class="responsive_menu">
            <ul class="main_menu">
                <li><a href="index.php"><i class="fa fa-home"></i> Home</a></li>
                <li><a href="#">Lectures</a>
                    <ul>
                        <li><a href="lectures.php">Upcoming Lectures</a></li>
                        <li><a href="all-lectures.php">All Lectures</a></li>
                    </ul>
                </li>
                <?php if(isset($_SESSION['login'])) { ?>
                <li><a href="all-lectures.php">My Lectures</a></li>
                <?php } ?>
                <li><a href="employee.php">Employees</a></li>
                <li><a href="contact.php">Contact</a></li>
            </ul> <!-- /.main_menu -->
            <ul class="social_icons">
                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                <li><a href="#"><i class="fa fa-pinterest"></i></a></li>
                <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                <li><a href="#"><i class="fa fa-rss"></i></a></li>
            </ul> <!-- /.social_icons -->
        </div> <!-- /.responsive_menu -->
    </div> <!-- /responsive_navigation -->


    <header class="site-header">
        <div class="container">
            <div class="row">
                <div class="col-md-4 header-left">
                    <p><i class="fa fa-phone"></i> +01 2334 853</p>
                    <p><i class="fa fa-envelope"></i> <a href="mailto:email@universe.com">email@universe.com</a></p>
                    <?php if(!isset($_SESSION['login'])) { ?>
                    <p><i class="fa fa-user"></i> <a href="http://ewsd.thecccbd.org/admin/">Secure Login</a></p>
                    <?php } ?>
                </div> <!-- /.header-left -->

                <div class="col-md-4">
                    <div class="logo">
                        <a href="index.php" title="Universe" rel="home">
                            <img src="img/logo.png" alt="Universe">
                        </a>
                    </div> <!-- /.logo -->
                </div> <!-- /.col-md-4 -->

                <div class="col-md-4 header-right">
                    <div class="search-form">
                        <?php if(isset($_SESSION['login'])) { ?>
                        <ul class="small-links">
                            <li><a href="account.php">My Account</a></li>
                            <li><a href="logout.php">Log Out</a></li>
                        </ul>
                        <?php 
                        }else {
                            if(isset($_GET['action'])){
                                if($_GET['action'] == "faild"){
                                    echo "Username and Password did not matched";
                                }else if($_GET['action'] == "adminpage"){
                                    echo "Please login to access admin section of this apps";
                                }else if($_GET['action'] == "admin" || $_GET['action'] == "organizer"){
                                    echo 'Hello '. $_GET['action'] .'...! please <a href="http://ewsd.thecccbd.org/admin/">CLICK HERE</a> to login securely';
                                }
                            }
                        ?>
                        <form name="search_form" method="post" class="search_form" action="login-submit.php">
                            <input type="text" name="user_name" placeholder="User Name or Email" title="User Name or Email" class="field_search">
                            <input type="password" name="pass" placeholder="Password" title="Password" class="field_search"><br />
                            No Account? <a href="registar.php">REGISTER</a>
                            <input class="mainBtn" type="submit" name="login-submit" value="Login">
                        </form> 
                        <?php } ?>
                    </div>
                </div> <!-- /.header-right -->
            </div>
        </div> <!-- /.container -->

        <div class="nav-bar-main" role="navigation">
            <div class="container">
                <nav class="main-navigation clearfix visible-md visible-lg" role="navigation">
                        <ul class="main-menu sf-menu">
                            <li><a href="index.php"><i class="fa fa-home"></i> Home</a></li>
                            <li><a href="#">Lectures</a>
                                <ul class="sub-menu">
                                    <li><a href="lectures.php">Upcoming Lectures</a></li>
                                    <li><a href="all-lectures.php">All Lectures</a></li>
                                </ul>
                            </li>
                            <?php if(isset($_SESSION['login'])) { ?>
                            <li><a href="lectures-query.php">My Lectures</a></li>
                            <?php } ?>
                            <li><a href="employee.php">Employees</a></li>
                            <li><a href="contact.php">Contact</a></li>
                        </ul> <!-- /.main-menu -->

                        <ul class="social-icons pull-right">
                            <li><a href="#" data-toggle="tooltip" title="Facebook"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#" data-toggle="tooltip" title="Twitter"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#" data-toggle="tooltip" title="Pinterest"><i class="fa fa-pinterest"></i></a></li>
                            <li><a href="#" data-toggle="tooltip" title="Google+"><i class="fa fa-google-plus"></i></a></li>
                            <li><a href="#" data-toggle="tooltip" title="RSS"><i class="fa fa-rss"></i></a></li>
                        </ul> <!-- /.social-icons -->
                </nav> <!-- /.main-navigation -->
            </div> <!-- /.container -->
        </div> <!-- /.nav-bar-main -->

    </header> <!-- /.site-header -->
    