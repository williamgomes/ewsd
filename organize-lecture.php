<?php include("header.php"); ?>
<script type="text/javascript" src="ckeditor/ckeditor.js"></script>
    <!-- Being Page Title -->
    <div class="container">
        <div class="page-title clearfix">
            <div class="row">
                <div class="col-md-12">
                    <h6><a href="index.html">Home</a></h6>
                    <h6><span class="page-active">Contact</span></h6>
                </div>
            </div>
        </div>
    </div>


    <div class="container">
        <div class="row">
            
            <div class="col-md-12">
                <div class="contact-page-content">
                    <div class="contact-heading">
                        <h3>Add Lecture</h3>
                        <p>&nbsp;</p>
                    </div>
                    <div class="contact-form clearfix">
                        <p class="full-row">
                            <span class="contact-label">
                                <label for="name-id">Title</label>
                            </span>
                            <input type="text" id="name-id" name="title">
                        </p>
                        <p class="full-row"> 
                            <span class="contact-label">
                                <label for="surname-id">Department:</label>
                            </span>
                                <div class="input-select">
                                    <select name="depertment" id="cat" class="postform">
                                        <option value="-1">-- select --</option>
                                        <option value="Accounting and Finance">Accounting and Finance</option>
                                    </select>
                                </div> <!-- /.input-select -->

                        </p>
                        <p class="full-row">
                            <span class="contact-label">
                                <label for="email-id">Room:</label>
                            </span>
                                 <div class="input-select">
                                    <select name="room" id="cat" class="postform">
                                        <option value="-1">-- select --</option>
                                        <option value="Room NO 1">Room NO 1</option>
                                    </select>
                                </div> <!-- /.input-select -->

                        </p>
                        <p class="full-row">
                            <span class="contact-label">
                                <label for="email-id">Presenter:</label>
                            </span>
                                  <div class="input-select">
                                    <select name="presentor" id="cat" class="postform">
                                        <option value="-1">-- select --</option>
                                        <option value="Mr. Moon">Mr. Moon</option>
                                    </select>
                                </div> <!-- /.input-select -->

                        </p>
                        <p class="full-row">
                            <span class="contact-label">
                                <label for="email-id">Staff:</label>
                            </span>
                               <div class="input-select">
                                    <select name="staff" id="cat" class="postform">
                                        <option value="-1">-- select --</option>
                                        <option value="Mr.Jamil">Mr.Jamil</option>
                                    </select>
                                </div> <!-- /.input-select -->

                        </p>
                        <p class="full-row">
                            <span class="contact-label">
                                <label for="email-id">Staff Responsibility:</label>
                            </span>
                            <input type="text" id="email-id" name="responsibility">
                        </p>
                        <p class="full-row">
                            <span class="contact-label">
                                <label for="message">Description:</label>
                            </span><br />
                            <textarea name="description" id="message"  class="ckeditor" rows="" cols=""></textarea>
                        </p>
                        <p class="full-row">
                            <input class="mainBtn" type="submit" name="add_lecture" value="Add lecture">
                        </p>
                    </div>
                </div>
            </div> <!-- /.col-md-7 -->

        </div> <!-- /.row -->
    </div> <!-- /.container -->
<?php include("footer.php"); ?>