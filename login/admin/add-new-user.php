<?php include("includes/session.php"); ?>
<?php confirm_admin(); ?>
<?php include("includes/header.php"); ?>
<div id="man-body-right">
	<h3>Add New User</h3>
	<div id="content">
    	<script type="text/javascript">
function validateForm()
{
var x=document.forms["add_user"]["email"].value;
var atpos=x.indexOf("@");
var dotpos=x.lastIndexOf(".");
if (atpos<1 || dotpos<atpos+2 || dotpos+2>=x.length)
  {
  alert("Please enter a valid e-mail address");
  return false;
  }
}
</script>
		<form name="add_user" method="post" onsubmit="return validateForm();" action="save-user.php">
			<p>
				<label>User Name</label>
				<input type="text" name="username" />
			</p>
			<p>
				<label>Password</label>
				<input type="password" name="password" />
			</p>
			<p>
				<label>Role</label>
				<select name="role">
					<option value="admin">Admin</option>
					<option value="organiser">Organiser</option>
					<option value="stuff">Stuff</option>
				</select>
			</p>
			<p>
				<label>&nbsp;</label>
				<input type="Submit" name="adduser" value="Add User" />
			</p>
		</form>
	</div>
</div>
<?php include("includes/footer.php"); ?>