<?php
if(!isset($_GET['user_id'])){
	header("Location: all-users.php");
}
?>
<?php include("includes/connection.php"); ?>
<?php include("includes/session.php"); ?>
<?php confirm_admin(); ?>
<?php include("includes/header.php"); ?>
<?php
	$user_id	= $_GET['user_id'];
	$sql		= "SELECT * FROM users WHERE user_id = {$user_id}";
	$result		= mysql_query($sql, $con);
?>

<div id="man-body-right">
	<h3>Update User</h3>
	<div id="content">
		<form name="add-post" method="post" action="update-user.php">
		<input type="hidden" name="user_id" value ="<?php echo $_GET['user_id']; ?>" />
		<?php if($result){
			$row	= mysql_fetch_array($result);
		?>
			<p>
				<label>First Name</label>
				<input type="text" name="first_name" value = "<?php echo $row['first_name']; ?>" />
			</p>
			<p>
				<label>Last Name</label>
				<input type="text" name="last_name" value = "<?php echo $row['last_name']; ?>" />
			</p>
			<p>
				<label>Email</label>
				<input type="text" name="email" value = "<?php echo $row['email']; ?>" />
			</p>
			<p>
				<label>User Name</label>
				<input type="text" name="username" value = "<?php echo $row['username']; ?>" disabled="disabled" />
			</p>
			<p>
				<label>Password</label>
				<input type="password" name="password" />
			</p>
			<p>
				<label>&nbsp;</label>
				<input type="Submit" name="updateuser" value="Update User" />
			</p>
		<?php } ?>
		</form>
	</div>
</div>
<?php include("includes/footer.php"); ?>