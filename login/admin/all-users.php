<?php include("includes/connection.php"); ?>
<?php include("includes/session.php"); ?>
<?php confirm_admin(); ?>
<?php include("includes/header.php"); ?>
<div id="man-body-right">
	<center><h3>All Users</h3></center>
	<div id="content">
		<table border= "1" cellpadding= "2" width="100%">
			<tr>
				<th>Sr.</th>
                <th>User Name</th>
                <th>Role</th>
                <th>Status</th>
				<th>Action</th>
			</tr>
			
			<?php
				
				// Query
				$sql	= "SELECT * FROM users";
				$result	= mysql_query($sql, $con);
				
				// Use return Data
				$sr = 0;
				if($result){
					while($row	= mysql_fetch_array($result)){ $sr++; ?>
					<tr>
						<td><?php echo $sr; ?></td>
						<td><?php echo $row['user_name'] ; ?></td>
						<td><?php echo $row['role']; ?></td>
                        <td><?php echo ($row['status'] == 1 ? "Active" : "Inactive"); ?></td>
						<td><a href="view-user.php?user_id=<?php echo $row['user_id']; ?>">View</a> &nbsp; <a href="edit-user.php?user_id=<?php echo $row['user_id']; ?>">Edit</a> &nbsp; <a href="delete-user.php?user_id=<?php echo $row['user_id']; ?>">Delete</a></td>
					</tr>
				
				<?php }
				}
			
			?>
			
		</table>
	</div>
</div>
<?php include("includes/footer.php"); ?>
