<?php
	session_start();
	
	function is_logged_in(){
		return $_SESSION['login'];
	}
	
	function confirm_admin(){
		if(!is_logged_in()){
			header("Location: ../login.php?action=adminpage");
		}
	}
?>