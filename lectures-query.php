<?php include("header.php"); ?>
    <!-- Being Page Title -->
    <div class="container">
        <div class="page-title clearfix">
            <div class="row">
                <div class="col-md-12">

                    <h6><a href="index.php">Home</a></h6>
                    <h6><span class="page-active">Lectures</span></h6>

                </div>
            </div>
        </div>
    </div>


    <div class="container">
        <div class="row">

            <!-- Here begin Main Content -->
            <div class="col-md-8">

                <div class="row">
                    <div class="col-md-12">
                        <div class="widget-main">
                            <div class="widget-inner">
                            
                                <h3>All Lectures</h3>
                                <dl class="course-list" role="tablist">
                                    <?php 
                                    $sql    = "SELECT lecture_id,title,department_name FROM lectures,department WHERE lectures.status = 'active' AND lectures.department_id = department.department_id";
                                    if(isset($_POST['dep']) && !empty($_POST['dep'])){
                                        $sql .= " AND lectures.department_id =".$_POST['dep'];
                                    }
                                    $result = mysql_query($sql, $con);
                                    while ($row    = mysql_fetch_array($result)){ ?>

                                    <dt>
                                        <i class="fa fa-caret-right ui-icon"></i>
                                        <span class="level"><?php echo $row['department_name'] ?></span>
                                        <a href="lecture-single.php?lecture_id=<?php echo $row['lecture_id'] ?>"><?php echo $row['title'] ?></a>
                                    </dt>
                                    <?php } ?>


                                </dl>

                            </div> <!-- /.widget-inner -->
                        </div> <!-- /.widget-main -->
                    </div> <!-- /.col-md-12 -->
                </div> <!-- /.row -->

            </div> <!-- /.col-md-8 -->


            <!-- Here begin Sidebar -->
            <div class="col-md-4">

                <?php include("lecturers.php"); ?>

            </div> <!-- /.col-md-4 -->
    
        </div> <!-- /.row -->
    </div> <!-- /.container -->
<?php include("footer.php"); ?>