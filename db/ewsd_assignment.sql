/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50616
Source Host           : localhost:3306
Source Database       : ewsd_assignment

Target Server Type    : MYSQL
Target Server Version : 50616
File Encoding         : 65001

Date: 2014-05-05 16:58:54
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `delegates`
-- ----------------------------
DROP TABLE IF EXISTS `delegates`;
CREATE TABLE `delegates` (
  `delegate_id` int(11) NOT NULL AUTO_INCREMENT,
  `person_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `study` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`delegate_id`),
  KEY `fk_8` (`person_id`),
  KEY `fk_9` (`user_id`),
  CONSTRAINT `fk_8` FOREIGN KEY (`person_id`) REFERENCES `person` (`person_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_9` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of delegates
-- ----------------------------
INSERT INTO `delegates` VALUES ('3', '8', '10', 'Post Graduation');
INSERT INTO `delegates` VALUES ('4', '9', '11', 'Post Graduation');

-- ----------------------------
-- Table structure for `department`
-- ----------------------------
DROP TABLE IF EXISTS `department`;
CREATE TABLE `department` (
  `department_id` int(11) NOT NULL AUTO_INCREMENT,
  `department_name` varchar(150) NOT NULL,
  `status` enum('active','inactive') NOT NULL,
  PRIMARY KEY (`department_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of department
-- ----------------------------
INSERT INTO `department` VALUES ('1', 'Physicsssssss', 'active');
INSERT INTO `department` VALUES ('2', 'Math', 'active');

-- ----------------------------
-- Table structure for `lectures`
-- ----------------------------
DROP TABLE IF EXISTS `lectures`;
CREATE TABLE `lectures` (
  `lecture_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(150) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `overview` text,
  `department_id` int(11) DEFAULT NULL,
  `room_id` int(11) DEFAULT NULL,
  `start_time` time NOT NULL,
  `end_time` time NOT NULL,
  `organiser_id` int(11) DEFAULT NULL,
  `presenter_id` int(11) DEFAULT NULL,
  `status` enum('active','inactive') NOT NULL DEFAULT 'active',
  PRIMARY KEY (`lecture_id`),
  KEY `fk_1` (`room_id`),
  KEY `fk_2` (`organiser_id`),
  KEY `fk_3` (`presenter_id`),
  KEY `fk_letr_dep` (`department_id`),
  CONSTRAINT `fk_1` FOREIGN KEY (`room_id`) REFERENCES `rooms` (`room_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_2` FOREIGN KEY (`organiser_id`) REFERENCES `organisers` (`organiser_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_3` FOREIGN KEY (`presenter_id`) REFERENCES `presenters` (`presenter_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_letr_dep` FOREIGN KEY (`department_id`) REFERENCES `department` (`department_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of lectures
-- ----------------------------
INSERT INTO `lectures` VALUES ('5', 'lecture 1', '2014-05-14', 'sgsfhdghd dfhgdfg', '1', '2', '06:30:17', '06:30:21', '1', '2', 'active');
INSERT INTO `lectures` VALUES ('6', 'lecture', '2014-05-06', 'ghfgnfxn', '2', '2', '09:45:20', '10:45:28', '1', '3', 'active');

-- ----------------------------
-- Table structure for `lecture_delegate`
-- ----------------------------
DROP TABLE IF EXISTS `lecture_delegate`;
CREATE TABLE `lecture_delegate` (
  `lecture_id` int(11) NOT NULL,
  `delegate_id` int(11) NOT NULL,
  `booking_time` datetime NOT NULL,
  PRIMARY KEY (`lecture_id`,`delegate_id`),
  KEY `fk_5` (`delegate_id`),
  CONSTRAINT `fk_4` FOREIGN KEY (`lecture_id`) REFERENCES `lectures` (`lecture_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_5` FOREIGN KEY (`delegate_id`) REFERENCES `delegates` (`delegate_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of lecture_delegate
-- ----------------------------
INSERT INTO `lecture_delegate` VALUES ('5', '3', '2014-05-05 08:08:34');
INSERT INTO `lecture_delegate` VALUES ('5', '4', '2014-05-05 10:53:01');

-- ----------------------------
-- Table structure for `lecture_staff`
-- ----------------------------
DROP TABLE IF EXISTS `lecture_staff`;
CREATE TABLE `lecture_staff` (
  `lecture_id` int(11) NOT NULL,
  `staff_id` int(11) NOT NULL,
  PRIMARY KEY (`lecture_id`,`staff_id`),
  KEY `fk_7` (`staff_id`),
  CONSTRAINT `fk_6` FOREIGN KEY (`lecture_id`) REFERENCES `lectures` (`lecture_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_7` FOREIGN KEY (`staff_id`) REFERENCES `staffs` (`staff_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of lecture_staff
-- ----------------------------
INSERT INTO `lecture_staff` VALUES ('5', '1');
INSERT INTO `lecture_staff` VALUES ('5', '2');

-- ----------------------------
-- Table structure for `organisers`
-- ----------------------------
DROP TABLE IF EXISTS `organisers`;
CREATE TABLE `organisers` (
  `organiser_id` int(11) NOT NULL AUTO_INCREMENT,
  `person_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`organiser_id`),
  KEY `fk_10` (`person_id`),
  KEY `fk_11` (`user_id`),
  CONSTRAINT `fk_10` FOREIGN KEY (`person_id`) REFERENCES `person` (`person_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_11` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of organisers
-- ----------------------------
INSERT INTO `organisers` VALUES ('1', '2', '2');

-- ----------------------------
-- Table structure for `person`
-- ----------------------------
DROP TABLE IF EXISTS `person`;
CREATE TABLE `person` (
  `person_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(150) DEFAULT NULL,
  `photo` varchar(150) DEFAULT NULL,
  `address` varchar(150) DEFAULT NULL,
  `phone` varchar(150) DEFAULT NULL,
  `dob` date DEFAULT NULL,
  PRIMARY KEY (`person_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of person
-- ----------------------------
INSERT INTO `person` VALUES ('1', 'Fond Du Lac Storage', 'prof3.jpg', 'N8030 Sales Rd', '9209231716', '2014-05-03');
INSERT INTO `person` VALUES ('2', 'William', 'prof2.jpg', 'N8030 Sales Rd', '9209231716', '2014-05-03');
INSERT INTO `person` VALUES ('3', 'Presenter1', 'prof1.jpg', 'N8030 Sales Rd', '9209231716', '2014-05-04');
INSERT INTO `person` VALUES ('6', 'pagla', 'prof2.jpg', 'qwertg', '23456', '1990-10-23');
INSERT INTO `person` VALUES ('7', 'moon', 'prof3.jpg', 'qwertg', '8801670737590', '2014-05-20');
INSERT INTO `person` VALUES ('8', 'moon', 'prof1.jpg', 'trtrtr', '7757', '1990-10-23');
INSERT INTO `person` VALUES ('9', 'Riaz Uddin', 'prof2.jpg', 'sibir area', '674574657', '0000-00-00');
INSERT INTO `person` VALUES ('10', 'Sk Shaikat', 'prof3.jpg', '10/267, rupnagar, pallabi, mirpur', '1670737590', '2014-05-21');

-- ----------------------------
-- Table structure for `presenters`
-- ----------------------------
DROP TABLE IF EXISTS `presenters`;
CREATE TABLE `presenters` (
  `presenter_id` int(11) NOT NULL AUTO_INCREMENT,
  `person_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `experience` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`presenter_id`),
  KEY `fk_12` (`person_id`),
  KEY `fk_13` (`user_id`),
  CONSTRAINT `fk_12` FOREIGN KEY (`person_id`) REFERENCES `person` (`person_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_13` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of presenters
-- ----------------------------
INSERT INTO `presenters` VALUES ('2', '2', '2', 'sdfdbgfn');
INSERT INTO `presenters` VALUES ('3', '3', '3', 'xghsj');
INSERT INTO `presenters` VALUES ('4', '10', '12', 'dfgsg');

-- ----------------------------
-- Table structure for `rooms`
-- ----------------------------
DROP TABLE IF EXISTS `rooms`;
CREATE TABLE `rooms` (
  `room_id` int(11) NOT NULL AUTO_INCREMENT,
  `room_name` varchar(150) NOT NULL,
  `capacity` int(11) NOT NULL,
  `status` enum('active','inactive') NOT NULL,
  PRIMARY KEY (`room_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of rooms
-- ----------------------------
INSERT INTO `rooms` VALUES ('1', 'Conference Hall1', '20', 'active');
INSERT INTO `rooms` VALUES ('2', 'Conference Hall2', '10', 'active');
INSERT INTO `rooms` VALUES ('3', 'Conference Hall3', '15', 'active');

-- ----------------------------
-- Table structure for `staffs`
-- ----------------------------
DROP TABLE IF EXISTS `staffs`;
CREATE TABLE `staffs` (
  `staff_id` int(11) NOT NULL AUTO_INCREMENT,
  `person_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `responsibility` text NOT NULL,
  PRIMARY KEY (`staff_id`),
  KEY `fk_14` (`person_id`),
  KEY `fk_15` (`user_id`),
  CONSTRAINT `fk_14` FOREIGN KEY (`person_id`) REFERENCES `person` (`person_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_15` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of staffs
-- ----------------------------
INSERT INTO `staffs` VALUES ('1', '1', '3', 'onek');
INSERT INTO `staffs` VALUES ('2', '2', '4', 'gula');

-- ----------------------------
-- Table structure for `users`
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(150) NOT NULL,
  `email` varchar(150) NOT NULL,
  `pass` varchar(150) NOT NULL,
  `role` varchar(150) NOT NULL,
  `status` enum('active','inactive') NOT NULL DEFAULT 'active',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES ('1', 'sk', 'sk@ewsd.com', 'e10adc3949ba59abbe56e057f20f883e', 'admin', 'active');
INSERT INTO `users` VALUES ('2', 'moon', 'moon@ewsd.com', 'e10adc3949ba59abbe56e057f20f883e', 'presenter', 'active');
INSERT INTO `users` VALUES ('3', 'staff', 'staff@ewsd.com', 'e10adc3949ba59abbe56e057f20f883e', 'presenter', 'active');
INSERT INTO `users` VALUES ('4', 'staff123', 'staff123@ewsd.com', '202cb962ac59075b964b07152d234b70', 'staff', 'active');
INSERT INTO `users` VALUES ('8', 'pagla', 'riaz@ewsd.com', 'e10adc3949ba59abbe56e057f20f883e', 'delegate', 'active');
INSERT INTO `users` VALUES ('9', 'moon', 'Sk.shaikat@yahoo.com', 'e10adc3949ba59abbe56e057f20f883e', 'delegate', 'active');
INSERT INTO `users` VALUES ('10', 'noon', 'moon@yahoo.com', 'e10adc3949ba59abbe56e057f20f883e', 'delegate', 'active');
INSERT INTO `users` VALUES ('11', 'riaz123', 'riaz@gm.com', 'e10adc3949ba59abbe56e057f20f883e', 'delegate', 'active');
INSERT INTO `users` VALUES ('12', 'sksha', 'Sk.shaikat@yahoo.com', 'e10adc3949ba59abbe56e057f20f883e', 'presenter', 'active');
