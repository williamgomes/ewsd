-- phpMyAdmin SQL Dump
-- version 4.1.6
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 05, 2014 at 01:01 AM
-- Server version: 5.5.36
-- PHP Version: 5.4.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `ewsd_assignment`
--

-- --------------------------------------------------------

--
-- Table structure for table `delegates`
--

CREATE TABLE IF NOT EXISTS `delegates` (
  `delegate_id` int(11) NOT NULL AUTO_INCREMENT,
  `person_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `stady level` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`delegate_id`),
  KEY `fk_8` (`person_id`),
  KEY `fk_9` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `department`
--

CREATE TABLE IF NOT EXISTS `department` (
  `department_id` int(11) NOT NULL AUTO_INCREMENT,
  `department_name` varchar(150) NOT NULL,
  `status` enum('active','inactive') NOT NULL,
  PRIMARY KEY (`department_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `department`
--

INSERT INTO `department` (`department_id`, `department_name`, `status`) VALUES
(1, 'Physicsssssss', 'inactive'),
(2, 'Math', 'active');

-- --------------------------------------------------------

--
-- Table structure for table `lectures`
--

CREATE TABLE IF NOT EXISTS `lectures` (
  `lecture_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(150) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `overview` varchar(150) DEFAULT NULL,
  `department_id` int(11) DEFAULT NULL,
  `room_id` int(11) DEFAULT NULL,
  `start_time` time NOT NULL,
  `end_time` time NOT NULL,
  `organiser_id` int(11) DEFAULT NULL,
  `presentor_id` int(11) DEFAULT NULL,
  `status` enum('active','inactive') NOT NULL DEFAULT 'active',
  PRIMARY KEY (`lecture_id`),
  KEY `fk_1` (`room_id`),
  KEY `fk_2` (`organiser_id`),
  KEY `fk_3` (`presentor_id`),
  KEY `fk_letr_dep` (`department_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `lectures`
--

INSERT INTO `lectures` (`lecture_id`, `title`, `date`, `overview`, `department_id`, `room_id`, `start_time`, `end_time`, `organiser_id`, `presentor_id`, `status`) VALUES
(4, 'Test1', '2014-05-04', 'At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestia', 2, 1, '11:00:00', '13:00:00', 1, 1, 'active'),
(5, 'Test1', '2014-05-05', 'Test1', 2, 1, '14:00:00', '16:00:00', 1, 1, 'active'),
(6, 'Test1', '2014-05-05', 'Test1', 2, 1, '11:00:00', '12:00:00', 1, 1, 'active'),
(7, 'Test1', '2014-05-05', 'Test1', 2, 1, '12:05:00', '14:00:00', 1, 1, 'active'),
(8, 'Test1', '2014-05-05', 'Test1', 2, 1, '10:00:00', '11:00:00', 1, 1, 'active');

-- --------------------------------------------------------

--
-- Table structure for table `lecture_delegate`
--

CREATE TABLE IF NOT EXISTS `lecture_delegate` (
  `lecture_id` int(11) NOT NULL,
  `delegate_id` int(11) NOT NULL,
  `booking_time` datetime NOT NULL,
  PRIMARY KEY (`lecture_id`,`delegate_id`),
  KEY `fk_5` (`delegate_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `lecture_staff`
--

CREATE TABLE IF NOT EXISTS `lecture_staff` (
  `lecture_id` int(11) NOT NULL,
  `staff_id` int(11) NOT NULL,
  PRIMARY KEY (`lecture_id`,`staff_id`),
  KEY `fk_7` (`staff_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lecture_staff`
--

INSERT INTO `lecture_staff` (`lecture_id`, `staff_id`) VALUES
(4, 1),
(4, 2);

-- --------------------------------------------------------

--
-- Table structure for table `organisers`
--

CREATE TABLE IF NOT EXISTS `organisers` (
  `organiser_id` int(11) NOT NULL AUTO_INCREMENT,
  `person_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`organiser_id`),
  KEY `fk_10` (`person_id`),
  KEY `fk_11` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `organisers`
--

INSERT INTO `organisers` (`organiser_id`, `person_id`, `user_id`) VALUES
(1, 5, 7);

-- --------------------------------------------------------

--
-- Table structure for table `person`
--

CREATE TABLE IF NOT EXISTS `person` (
  `person_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(150) DEFAULT NULL,
  `photo` varchar(150) DEFAULT NULL,
  `address` varchar(150) DEFAULT NULL,
  `phone` varchar(150) DEFAULT NULL,
  `dob` date DEFAULT NULL,
  PRIMARY KEY (`person_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `person`
--

INSERT INTO `person` (`person_id`, `name`, `photo`, `address`, `phone`, `dob`) VALUES
(1, 'Fond Du Lac Storage', NULL, 'N8030 Sales Rd', '9209231716', '2014-05-03'),
(2, 'William', NULL, 'N8030 Sales Rd', '9209231716', '2014-05-03'),
(3, 'Presenter1', NULL, 'N8030 Sales Rd', '9209231716', '2014-05-04'),
(5, 'Organiser', NULL, 'N8030 Sales Rd', '9209231716', '2014-05-04');

-- --------------------------------------------------------

--
-- Table structure for table `presentors`
--

CREATE TABLE IF NOT EXISTS `presentors` (
  `presentor_id` int(11) NOT NULL AUTO_INCREMENT,
  `person_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `experience` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`presentor_id`),
  KEY `fk_12` (`person_id`),
  KEY `fk_13` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `presentors`
--

INSERT INTO `presentors` (`presentor_id`, `person_id`, `user_id`, `experience`) VALUES
(1, 3, 5, 'test');

-- --------------------------------------------------------

--
-- Table structure for table `rooms`
--

CREATE TABLE IF NOT EXISTS `rooms` (
  `room_id` int(11) NOT NULL AUTO_INCREMENT,
  `room_name` varchar(150) NOT NULL,
  `capacity` int(11) NOT NULL,
  `status` enum('active','inactive') NOT NULL,
  PRIMARY KEY (`room_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `rooms`
--

INSERT INTO `rooms` (`room_id`, `room_name`, `capacity`, `status`) VALUES
(1, 'Conference Hall', 200, 'inactive'),
(2, 'Conference Hall', 160, 'active'),
(3, 'Conference Hall', 2000, 'active');

-- --------------------------------------------------------

--
-- Table structure for table `staffs`
--

CREATE TABLE IF NOT EXISTS `staffs` (
  `staff_id` int(11) NOT NULL AUTO_INCREMENT,
  `person_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `responsibility` text NOT NULL,
  PRIMARY KEY (`staff_id`),
  KEY `fk_14` (`person_id`),
  KEY `fk_15` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `staffs`
--

INSERT INTO `staffs` (`staff_id`, `person_id`, `user_id`, `responsibility`) VALUES
(1, 1, 3, ''),
(2, 2, 4, '');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(150) NOT NULL,
  `email` varchar(150) NOT NULL,
  `pass` varchar(150) NOT NULL,
  `role` varchar(150) NOT NULL,
  `status` enum('active','inactive') NOT NULL DEFAULT 'active',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `user_name`, `email`, `pass`, `role`, `status`) VALUES
(1, 'sk', '', 'e10adc3949ba59abbe56e057f20f883e', 'admin', 'active'),
(2, 'moon', '', 'e10adc3949ba59abbe56e057f20f883e', 'organiser', 'active'),
(3, 'staff', '', '202cb962ac59075b964b07152d234b70', 'staff', 'active'),
(4, 'staff123', '', '202cb962ac59075b964b07152d234b70', 'staff', 'active'),
(5, 'presenter', '', '202cb962ac59075b964b07152d234b70', 'delegate', 'active'),
(7, 'organiser', '', 'e10adc3949ba59abbe56e057f20f883e', 'organiser', 'active');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `delegates`
--
ALTER TABLE `delegates`
  ADD CONSTRAINT `fk_8` FOREIGN KEY (`person_id`) REFERENCES `person` (`person_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_9` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `lectures`
--
ALTER TABLE `lectures`
  ADD CONSTRAINT `fk_1` FOREIGN KEY (`room_id`) REFERENCES `rooms` (`room_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_2` FOREIGN KEY (`organiser_id`) REFERENCES `organisers` (`organiser_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_3` FOREIGN KEY (`presentor_id`) REFERENCES `presentors` (`presentor_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_letr_dep` FOREIGN KEY (`department_id`) REFERENCES `department` (`department_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `lecture_delegate`
--
ALTER TABLE `lecture_delegate`
  ADD CONSTRAINT `fk_4` FOREIGN KEY (`lecture_id`) REFERENCES `lectures` (`lecture_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_5` FOREIGN KEY (`delegate_id`) REFERENCES `delegates` (`delegate_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `lecture_staff`
--
ALTER TABLE `lecture_staff`
  ADD CONSTRAINT `fk_6` FOREIGN KEY (`lecture_id`) REFERENCES `lectures` (`lecture_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_7` FOREIGN KEY (`staff_id`) REFERENCES `staffs` (`staff_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `organisers`
--
ALTER TABLE `organisers`
  ADD CONSTRAINT `fk_10` FOREIGN KEY (`person_id`) REFERENCES `person` (`person_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_11` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `presentors`
--
ALTER TABLE `presentors`
  ADD CONSTRAINT `fk_12` FOREIGN KEY (`person_id`) REFERENCES `person` (`person_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_13` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `staffs`
--
ALTER TABLE `staffs`
  ADD CONSTRAINT `fk_14` FOREIGN KEY (`person_id`) REFERENCES `person` (`person_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_15` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
