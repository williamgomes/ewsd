<?php
include('../config/config.php');

//checking if admin already logged in
if(checkAdminLogin()){
    $link = 'dashboard.php?msg=' . base64_encode('You already logged in.');
    redirect($link);
}



$email = '';
$password = '';

if (isset($_POST['submit'])) {
    extract($_POST);
    
    if (isset($email) AND $email == '') {
        $err = 'Username field is required!';
    } elseif (isset($password) AND $password == '') {
        $err = 'Password filed is required!!';
    } elseif (isset($password) AND (strlen($password) > $config['ADMIN_PASSWORD_LENGTH_MAX'] OR strlen($password) < $config['ADMIN_PASSWORD_LENGTH_MIN'])) {
        $err = 'Password length is not correct!';
    }

    if ($err == '') {
        $securedPass = md5(mysqli_real_escape_string($con, trim($password)));
        $adminSql = "SELECT * FROM users WHERE user_name = '" . mysqli_real_escape_string($con, trim($email)) . "' AND pass= '$securedPass'";
        $adminSqlResult = mysqli_query($con, $adminSql);
        $adminCount = mysqli_num_rows($adminSqlResult);
        if($adminSqlResult){
            if ($adminCount > 0) {
                $adminSqlResultRowObj = mysqli_fetch_object($adminSqlResult);
                if (isset($adminSqlResultRowObj->role) AND $adminSqlResultRowObj->role == "admin" OR $adminSqlResultRowObj->role == "organiser") {

                    if(isset($adminSqlResultRowObj->status) AND $adminSqlResultRowObj->status == 'active'){
                            /* Start: setting session for login */
                            setSession('admin_username',$email);
                            setSession('admin_password',$securedPass);
                            setSession('admin_id',$adminSqlResultRowObj->user_id);
                            setSession('admin_status',true);
                            setSession('admin_type',$adminSqlResultRowObj->role);


                            $link = 'dashboard.php?msg=' . base64_encode('Welcome to admin panel');
                            redirect($link);
                    } else {
                        $err = "Your account is not active.";
                    }
                            /* End: setting session for login */
                            /* Start: set COOKIE */

    //                        if (isset($_POST['remember']) AND $_POST['remember'] == 'yes') {
    //                            setcookie("email", $_POST['email'], time() + $config['ADMIN_COOKIE_EXPIRE_DURATION']);
    //                            setcookie("password", $_POST['password'], time() + $config['ADMIN_COOKIE_EXPIRE_DURATION']);
    //                        } else {
    //
    //                            setcookie("email", $_POST['email'], time() - 3600);
    //                            setcookie("password", $_POST['password'], time() - 3600);
    //                        }
                            /* End: set COOKIE */

                } else {
                    $err = 'You dont have enough privilage.';
                }
            } else {
                $err = 'Username or Password didnt match.';
            }
        } else {
            if(DEBUG){
                $err = "adminSqlResult error: " . mysqli_error($con);
            } else {
                $err = "adminSqlResult query failed.";
            }
        }
    }
}
?>

<!DOCTYPE html>
<!-- 
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.0.3
Version: 1.5.5
Author: KeenThemes
Website: http://www.keenthemes.com/
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->
    <head>
        <meta charset="utf-8"/>
        <title><?php echo $config['SITE_NAME']; ?></title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1.0" name="viewport"/>
        <meta content="" name="description"/>
        <meta content="" name="author"/>
        <meta name="MobileOptimized" content="320">
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
        <link href="assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="assets/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL STYLES -->
        <link rel="stylesheet" type="text/css" href="assets/plugins/select2/select2_metro.css"/>
        <!-- END PAGE LEVEL SCRIPTS -->
        <!-- BEGIN THEME STYLES -->
        <link href="assets/css/style-metronic.css" rel="stylesheet" type="text/css"/>
        <link href="assets/css/style.css" rel="stylesheet" type="text/css"/>
        <link href="assets/css/style-responsive.css" rel="stylesheet" type="text/css"/>
        <link href="assets/css/plugins.css" rel="stylesheet" type="text/css"/>
        <link href="assets/css/themes/default.css" rel="stylesheet" type="text/css" id="style_color"/>
        <link href="assets/css/pages/login.css" rel="stylesheet" type="text/css"/>
        <link href="assets/css/custom.css" rel="stylesheet" type="text/css"/>
        <!-- END THEME STYLES -->
        <link rel="shortcut icon" href="favicon.ico"/>
    </head>
    <!-- BEGIN BODY -->
    <body class="login">
        <!-- BEGIN LOGO -->
        <div class="logo">
            <img src="assets/img/logo-big.png" alt=""/>
        </div>
        <!-- END LOGO -->
        <!-- BEGIN LOGIN -->
        <div class="content">
            <!-- BEGIN LOGIN FORM -->
            <form class="login-form" action="<?php echo baseUrl('admin/index.php'); ?>" method="post">
                <h3 class="form-title">Login to your account</h3>
                <?php include './alert.php'; ?>
                <div class="form-group">
			<!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
			<label class="control-label visible-ie8 visible-ie9">Username</label>
			<div class="input-icon">
				<i class="fa fa-user"></i>
				<input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="Username" name="email" value="<?php echo $email; ?>"/>
			</div>
		</div>
		<div class="form-group">
			<label class="control-label visible-ie8 visible-ie9">Password</label>
			<div class="input-icon">
				<i class="fa fa-lock"></i>
				<input class="form-control placeholder-no-fix" type="password" autocomplete="off" placeholder="Password" name="password" value="<?php echo $password; ?>"/>
			</div>
		</div>
		<div class="form-actions">
			<label class="checkbox">
			<input type="checkbox" name="remember" value="1"/> Remember me </label>
			<button type="submit" name="submit" class="btn green pull-right">
			Login <i class="m-icon-swapright m-icon-white"></i>
			</button>
		</div>


            </form>
            <!-- END LOGIN FORM -->


        </div>
        <!-- END LOGIN -->
        <!-- BEGIN COPYRIGHT -->
        <div class="copyright">
            2014 &copy; EWSD Admin panel.
        </div>
        <!-- END COPYRIGHT -->
        <!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
        <!-- BEGIN CORE PLUGINS -->
        <!--[if lt IE 9]>
                <script src="assets/plugins/respond.min.js"></script>
                <script src="assets/plugins/excanvas.min.js"></script> 
                <![endif]-->
        <script src="assets/plugins/jquery-1.10.2.min.js" type="text/javascript"></script>
        <script src="assets/plugins/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
        <script src="assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="assets/plugins/bootstrap-hover-dropdown/twitter-bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
        <script src="assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
        <script src="assets/plugins/jquery.blockui.min.js" type="text/javascript"></script>
        <script src="assets/plugins/jquery.cokie.min.js" type="text/javascript"></script>
        <script src="assets/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
        <!-- END CORE PLUGINS -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="assets/plugins/jquery-validation/dist/jquery.validate.min.js" type="text/javascript"></script>
        <script type="text/javascript" src="assets/plugins/select2/select2.min.js"></script>
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN PAGE LEVEL SCRIPTS -->
        <script src="assets/scripts/app.js" type="text/javascript"></script>
        <script src="assets/scripts/login.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL SCRIPTS -->
        <script>
//            jQuery(document).ready(function() {
//                App.init();
//                Login.init();
//            });
        </script>
        <!-- END JAVASCRIPTS -->
    </body>
    <!-- END BODY -->
</html>