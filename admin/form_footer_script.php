<!-- END FOOTER -->
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="<?php echo baseUrl('admin/'); ?>assets/plugins/respond.min.js"></script>
<script src="<?php echo baseUrl('admin/'); ?>assets/plugins/excanvas.min.js"></script> 
<![endif]-->
<script src="<?php echo baseUrl('admin/'); ?>assets/plugins/jquery-1.10.2.min.js" type="text/javascript"></script>
<script src="<?php echo baseUrl('admin/'); ?>assets/plugins/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
<script src="<?php echo baseUrl('admin/'); ?>assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?php echo baseUrl('admin/'); ?>assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="<?php echo baseUrl('admin/'); ?>assets/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="<?php echo baseUrl('admin/'); ?>assets/plugins/jquery.cokie.min.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo baseUrl('admin/'); ?>assets/plugins/fuelux/js/spinner.min.js"></script>
<script src="<?php echo baseUrl('admin/'); ?>assets/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>

<script type="text/javascript" src="<?php echo baseUrl('admin/'); ?>assets/plugins/ckeditor/ckeditor.js"></script>


<script type="text/javascript" src="<?php echo baseUrl('admin/'); ?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="<?php echo baseUrl('admin/'); ?>assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>
<script type="text/javascript" src="<?php echo baseUrl('admin/'); ?>assets/plugins/bootstrap-daterangepicker/moment.min.js"></script>
<script type="text/javascript" src="<?php echo baseUrl('admin/'); ?>assets/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
<script type="text/javascript" src="<?php echo baseUrl('admin/'); ?>assets/plugins/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
<script src="<?php echo baseUrl('admin/'); ?>assets/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js" type="text/javascript"></script>
<script src="<?php echo baseUrl('admin/'); ?>assets/plugins/bootstrap-touchspin/bootstrap.touchspin.js" type="text/javascript"></script>


<!-- END CORE PLUGINS -->
<script src="<?php echo baseUrl('admin/'); ?>assets/scripts/app.js"></script>
<script src="<?php echo baseUrl('admin/'); ?>assets/scripts/form-components.js"></script>

<script>
jQuery(document).ready(function() { 
   // initiate layout and plugins
   App.init();
   FormComponents.init();
});   
</script>
<!-- BEGIN GOOGLE RECAPTCHA -->
<script type="text/javascript">
var RecaptchaOptions = {
   theme : 'custom',
   custom_theme_widget: 'recaptcha_widget'
};
</script>
<!-- END JAVASCRIPTS -->