<?php
include ('../../config/config.php');
if (!checkAdminLogin()) {
    $link = baseUrl('admin/index.php?err=' . base64_encode('Please login to access admin panel'));
    redirect($link);
}

$title = '';
$overview = '';
$date = '';
$department = '';
$room = '';
$presenter = '';
$start = '';
$end = '';

if (isset($_POST['save']) AND $_POST['save'] == 'Submit') {
    
    extract($_POST);

    if ($title == '') {
        $err = 'Lecture Title field is required!!';
    } elseif ($overview == '') {
        $err = 'Lecture Overview field is required!!';
    } elseif ($date == '') {
        $err = 'Lecture Date field is required';
    } elseif ($department == '') {
        $err = 'Department field is required!!';
    } elseif ($room == '') {
        $err = 'Room field is required!!';
    } elseif ($presenter == '') {
        $err = 'Presenter field is required!!';
    } elseif ($start == '') {
        $err = 'Start Time  field is required!!';
    } elseif ($end == '') {
        $err = 'End Time  field is required!!';
    } else {
        /* Start :Checking the user already exist or not */
        $CheckSql = "SELECT * FROM lectures WHERE room_id='" . mysqli_real_escape_string($con, $room) . "' AND DATE_FORMAT(date,'%Y-%m-%d')='" . mysqli_real_escape_string($con, $date) . "' AND ('$start' BETWEEN start_time AND end_time OR '$end' BETWEEN start_time AND end_time)";
        $CheckSqlResult = mysqli_query($con, $CheckSql);
        if ($CheckSqlResult) {
            $countCheckSqlResult = mysqli_num_rows($CheckSqlResult);
            $CheckSqlResultRowObj = mysqli_fetch_object($CheckSqlResult);
            if ($countCheckSqlResult > 0) {
                $err = "This room is already booked from <b>'$CheckSqlResultRowObj->start_time'</b> to <b>'$CheckSqlResultRowObj->end_time'</b> on <b>'$CheckSqlResultRowObj->date'</b> ";
            }
            mysqli_free_result($CheckSqlResult);
        } else {
            if (DEBUG) {
                $err = 'CheckSqlResult Error: ' . mysqli_error($con);
            } else {
                $err = "CheckSqlResult Query failed.";
            }
        }
    }
    
    
    
    if ($err == '') {
        
        $organiserUserID = getSession("admin_id");
        $organiserID = 0;
        $sqlOrganiserID = "SELECT organiser_id FROM organisers WHERE user_id=$organiserUserID";
        $executeOrganiserID = mysqli_query($con, $sqlOrganiserID);
        if($executeOrganiserID){
            $executeOrganiserIDObj = mysqli_fetch_object($executeOrganiserID);
            if(isset($executeOrganiserIDObj->organiser_id)){
                $organiserID = $executeOrganiserIDObj->organiser_id;
            }
        } else {
            if(DEBUG){
                $err = 'executeOrganiserID Error: ' . mysqli_error($con);
            } else {
                $err = 'executeOrganiserID query failed.';
            }
        }
        
        $addLecture = '';
        $addLecture .=' title = "' . mysqli_real_escape_string($con, $title) . '"';
        $addLecture .=', date = "' . mysqli_real_escape_string($con, $date) . '"';
        $addLecture .=', overview = "' . mysqli_real_escape_string($con, $overview) . '"';
        $addLecture .=', department_id = "' . mysqli_real_escape_string($con, $department) . '"';
        $addLecture .=', room_id = "' . mysqli_real_escape_string($con, $room) . '"';
        $addLecture .=', start_time = "' . mysqli_real_escape_string($con, $start) . '"';
        $addLecture .=', end_time = "' . mysqli_real_escape_string($con, $end) . '"';
        $addLecture .=', organiser_id = "' . mysqli_real_escape_string($con, $organiserID) . '"';
        $addLecture .=', presenter_id = "' . mysqli_real_escape_string($con, $presenter) . '"';

        $lectureInsSql = "INSERT INTO lectures SET $addLecture";
        $lectureInsSqlResult = mysqli_query($con, $lectureInsSql);
        if ($lectureInsSqlResult) {
            $msg = "Lecture created successfully.";
            $link = "index.php?msg=" . base64_encode($msg);
            redirect($link);
                
        } else {
            if (DEBUG) {
                $err = 'lectureInsSqlResult Error: ' . mysqli_error($con);
            } else {
                $err = "lectureInsSqlResult Query failed.";
            }
        }
    }
}


//getting all presenters from database
$presenterArray = array();
$sqlPresenter = "SELECT * FROM presenters"
        . " LEFT JOIN person ON person.person_id=presenters.person_id"
        . " LEFT JOIN users ON users.user_id=presenters.user_id";
$executePresenter = mysqli_query($con,$sqlPresenter);
if($executePresenter){
    while($executePresenterObj = mysqli_fetch_object($executePresenter)){
        $presenterArray[] = $executePresenterObj;
    }
} else {
    if(DEBUG){
        $err = "executePresenter error: " . mysqli_error($con);
    } else {
        $err = "executePresenter query failed.";
    }
}



//getting room information from database
$roomArray = array();
$roomSql = "SELECT * FROM rooms";
$roomSqlResult = mysqli_query($con, $roomSql);
if ($roomSqlResult) {
    while ($roomSqlResultRowObj = mysqli_fetch_object($roomSqlResult)) {
        $roomArray[] = $roomSqlResultRowObj;
    }
    mysqli_free_result($roomSqlResult);
} else {
    if(DEBUG){
        $err = "roomSqlResult error: " . mysqli_error($con);
    } else {
        $err = "roomSqlResult query failed.";
    }
}



//getting department information from database
$departmentArray = array();
$departmentSql = "SELECT * FROM department";
$departmentSqlResult = mysqli_query($con, $departmentSql);
if ($departmentSqlResult) {
    while ($departmentSqlResultRowObj = mysqli_fetch_object($departmentSqlResult)) {
        $departmentArray[] = $departmentSqlResultRowObj;
    }
    mysqli_free_result($departmentSqlResult);
} else {
    if(DEBUG){
        $err = "departmentSqlResult error: " . mysqli_error($con);
    } else {
        $err = "departmentSqlResult query failed.";
    }
}
?>
<!DOCTYPE html>
<!-- 
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.0.3
Version: 1.5.5
Author: KeenThemes
Website: http://www.keenthemes.com/
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->
    <head>
        <meta charset="utf-8"/>
        <title><?php echo $config['SITE_NAME']; ?> | Admin Create</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1.0" name="viewport"/>
        <meta content="" name="description"/>
        <meta content="" name="author"/>
        <meta name="MobileOptimized" content="320">
        <?php
        include(basePath('admin/header.php'));
        ?>
        <link rel="stylesheet" type="text/css" href="<?php echo baseUrl(); ?>admin/assets/plugins/select2/select2_metro.css"/>
        <link rel="stylesheet" type="text/css" href="<?php echo baseUrl(); ?>admin/assets/plugins/clockface/css/clockface.css"/>
        <link rel="stylesheet" type="text/css" href="<?php echo baseUrl(); ?>admin/assets/plugins/bootstrap-timepicker/compiled/timepicker.css"/>
    </head>
    <!-- END HEAD -->
    <!-- BEGIN BODY -->
    <body class="page-header-fixed">
        <!-- BEGIN HEADER -->
        <?php
        include '../top_navigation.php';
        ?>
        <!-- END HEADER -->
        <div class="clearfix">
        </div>
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <!-- BEGIN SIDEBAR -->
            <div class="page-sidebar-wrapper">
                <div class="page-sidebar navbar-collapse collapse">
                    <!-- BEGIN SIDEBAR MENU -->
                    <?php
                    include(basePath('admin/sidebar.php'));
                    ?>
                    <!-- END SIDEBAR MENU -->
                </div>
            </div>
            <!-- END SIDEBAR -->
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <div class="page-content">
                    <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
                    <?php
                    include '../template_settings.php';
                    ?>
                    <!-- END STYLE CUSTOMIZER -->
                    <!-- BEGIN PAGE HEADER-->
                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <h3 class="page-title">
                                Lecture Module
                            </h3>
                            <ul class="page-breadcrumb breadcrumb">
                                <li class="btn-group">
                                    <button type="button" class="btn blue dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="1000" data-close-others="true">
                                        <span>
                                            Actions
                                        </span>
                                        <i class="fa fa-angle-down"></i>
                                    </button>
                                    <ul class="dropdown-menu pull-right" role="menu">
                                        <li>
                                            <a href="<?php echo baseUrl('admin/'); ?>#">Action</a>
                                        </li>
                                        <li>
                                            <a href="<?php echo baseUrl('admin/'); ?>#">Another action</a>
                                        </li>
                                        <li>
                                            <a href="<?php echo baseUrl('admin/'); ?>#">Something else here</a>
                                        </li>
                                        <li class="divider">
                                        </li>
                                        <li>
                                            <a href="<?php echo baseUrl('admin/'); ?>#">Separated link</a>
                                        </li>
                                    </ul>
                                </li>
                                <li>
                                    <i class="fa fa-home"></i>
                                    <a href="<?php echo baseUrl('admin/dashboard.php'); ?>">Home</a>
                                    <i class="fa fa-angle-right"></i>
                                </li>
                                <li>
                                    <a href="<?php echo baseUrl('admin/lecture/'); ?>">Lecture</a>
                                    <i class="fa fa-angle-right"></i>
                                </li>
                                <li>
                                    <a href="<?php echo baseUrl('admin/lecture/add_lecture.php'); ?>">Add Lecture</a>
                                    <i class="fa fa-angle-right"></i>
                                </li>
                            </ul>
                            <!-- END PAGE TITLE & BREADCRUMB-->
                        </div>
                    </div>
                    <!-- END PAGE HEADER-->
                    <!-- BEGIN PAGE CONTENT-->
                    <div class="row">


                        <div class="col-md-12 ">
                            <?php if ($err != '') { ?>
                                <div class="alert alert-warning alert-dismissable col-md-9">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                    <?php echo $err; ?>
                                </div>
                            <?php } ?>
                            <?php if ($msg != '') { ?>
                                <div class="alert alert-success alert-dismissable">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                    <?php echo $msg; ?>
                                </div>
                            <?php } ?>
                            <!-- BEGIN SAMPLE FORM PORTLET-->
                            <div class="portlet box green ">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-reorder"></i> Add Lecture
                                    </div>
                                    <div class="tools">
                                        <a href="<?php echo baseUrl('admin/'); ?>" class="collapse"></a>
                                        <a href="<?php echo baseUrl('admin/'); ?>#portlet-config" data-toggle="modal" class="config"></a>
                                        <a href="<?php echo baseUrl('admin/'); ?>" class="reload"></a>
                                        <a href="<?php echo baseUrl('admin/'); ?>" class="remove"></a>
                                    </div>
                                </div>
                                <div class="portlet-body form">
                                    <form class="form-horizontal" method="post" action="<?php echo baseUrl('admin/lecture/add_lecture.php'); ?>" role="form">
                                        <div class="form-body">
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Lecture Title:</label>
                                                <div class="col-md-9">
                                                    <input type="text" name="title" class="form-control required" value="<?php echo $title; ?>" required="required"/>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Lecture Overview:</label>
                                                <div class="col-md-9">
                                                    <input type="text" name="overview" class="form-control required" value="<?php echo $overview; ?>"  required="required" />
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Lecture Date:</label>
                                                <div class="col-md-9">
                                                    <input class="form-control form-control-inline input-medium date-picker" data-date-format="yyyy-mm-dd" size="16" type="text" value="" name="date" required="required">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-3">Department</label>
                                                <div class="col-md-9">
                                                    <select class="form-control" name="department">
                                                        <option value=""> -- Select Department -- </option>
                                                    <?php
                                                    $departmentArrayCounter = count($departmentArray);
                                                    if ($departmentArrayCounter > 0):
                                                        ?>
                                                        <?php for ($i = 0; $i < $departmentArrayCounter; $i++): ?>
                                                            <option value="<?php echo $departmentArray[$i]->department_id; ?>" <?php if($department == $departmentArray[$i]->department_id){ echo "selected"; } ?>><?php echo $departmentArray[$i]->department_name; ?></option>
                                                        <?php     endfor; ?>    
                                                    <?php endif; ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-3">Room</label>
                                                <div class="col-md-9">
                                                    <select class="form-control" name="room">
                                                        <option value=""> -- Select Room -- </option>
                                                    <?php
                                                    $roomArrayCounter = count($roomArray);
                                                    if ($roomArrayCounter > 0):
                                                        ?>
                                                        <?php for ($i = 0; $i < $roomArrayCounter; $i++): ?>
                                                            <option value="<?php echo $roomArray[$i]->room_id; ?>" <?php if($room == $roomArray[$i]->room_id){ echo "selected"; } ?>><?php echo $roomArray[$i]->room_name; ?></option>
                                                        <?php     endfor; ?>    
                                                    <?php endif; ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-3">Presenter</label>
                                                <div class="col-md-9">
                                                    <select class="form-control" name="presenter">
                                                        <option value=""> -- Select Presenter -- </option>
                                                    <?php
                                                    $presentersArrayCounter = count($presenterArray);
                                                    if ($presentersArrayCounter > 0):
                                                        ?>
                                                        <?php for ($i = 0; $i < $presentersArrayCounter; $i++): ?>
                                                            <option value="<?php echo $presenterArray[$i]->presenter_id; ?>" <?php if($presenter == $presenterArray[$i]->presenter_id){ echo "selected"; } ?>><?php echo $presenterArray[$i]->name; ?></option>
                                                        <?php     endfor; ?>    
                                                    <?php endif; ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-3">Start Time</label>
                                                <div class="col-md-3">
                                                    <div class="input-group">
                                                        <input type="text" id="clockface_2" value="<?php echo $start; ?>" class="form-control clockface-open" readonly="" name="start">
                                                        <span class="input-group-btn">
                                                                <button class="btn default" type="button" id="clockface_2_toggle"><i class="fa fa-clock-o"></i></button>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-3">End Time</label>
                                                <div class="col-md-3">
                                                    <div class="input-group">
                                                        <input type="text" id="clockface_3" value="<?php echo $end; ?>" class="form-control clockface-open" readonly="" name="end">
                                                        <span class="input-group-btn">
                                                                <button class="btn default" type="button" id="clockface_3_toggle"><i class="fa fa-clock-o"></i></button>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-actions fluid">
                                            <div class="col-md-offset-3 col-md-9">
                                                <button type="submit" name="save" value="Submit" class="btn green">Submit</button>
                                                <button type="reset" class="btn default">Cancel</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>





                        </div>
                    </div>

                </div>
            </div>
            <!-- END CONTENT -->
        </div>
        <!-- END CONTAINER -->
        <!-- BEGIN FOOTER -->
        <?php
        include basePath('admin/footer.php');
        ?>
        <?php
        include (basePath('admin/footer_script.php'));
        include (basePath('admin/form_includes.php'));
        ?>
        <script type="text/javascript" src="<?php echo baseUrl();?>admin/assets/plugins/clockface/js/clockface.js"></script>
        <script type="text/javascript" src="<?php echo baseUrl();?>admin/assets/plugins/select2/select2.min.js"></script>
        <!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="<?php echo baseUrl();?>admin/assets/plugins/select2/select2.min.js"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="<?php echo baseUrl();?>admin/assets/scripts/app.js"></script>
<script src="<?php echo baseUrl();?>admin/assets/scripts/form-samples.js"></script>
<!-- END PAGE LEVEL SCRIPTS -->
<!--        <script src="<?php echo baseUrl();?>admin/assets/custom/admin/admin_create.js"></script>-->
    </body>
    <!-- END BODY -->
</html>