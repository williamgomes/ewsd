<?php
include ('../../config/config.php');
if (!checkAdminLogin()) {
    $link = baseUrl('admin/index.php?err=' . base64_encode('Please login to access admin panel'));
    redirect($link);
}


$lectureTitle = '';
$lectureOverview = '';
$lectureDepartment = '';
$lectureDate = '';
$startTime = '';
$lectureDepartment = '';
$lectureOrganiser = '';
$lecturePresenter = '';

if(isset($_GET['id']) AND $_GET['id'] != ""){
    $lectureID = base64_decode($_GET['id']);
    
    $sqlGetLecture = "SELECT *,lectures.status AS LectStatus,organisers.person_id as OrgID, presenters.person_id AS PreID FROM lectures"
        . " LEFT JOIN department ON department.department_id=lectures.department_id"
        . " LEFT JOIN organisers ON organisers.organiser_id=lectures.organiser_id"
        . " LEFT JOIN presenters ON presenters.presenter_id=lectures.presenter_id"
        . " WHERE lecture_id=$lectureID";
    $lectureSqlResult = mysqli_query($con, $sqlGetLecture);
    if ($lectureSqlResult) {
        $lectureSqlResultRowObj = mysqli_fetch_object($lectureSqlResult);
        if(isset($lectureSqlResultRowObj->lecture_id)){
            $lectureTitle = $lectureSqlResultRowObj->title;
            $lectureOverview = $lectureSqlResultRowObj->overview;
            $lectureDepartment = $lectureSqlResultRowObj->department_name;
            $lectureDate = $lectureSqlResultRowObj->date;
            $startTime = $lectureSqlResultRowObj->start_time;
            $lectureOrganiserID = $lectureSqlResultRowObj->OrgID;
            $lecturePresenterID = $lectureSqlResultRowObj->PreID;
            
            //getting organiser info
            $sqlOrganiser = "SELECT * FROM person WHERE person_id=$lectureOrganiserID";
            $executeOrganiser = mysqli_query($con,$sqlOrganiser);
            if($executeOrganiser){
                $executeOrganiserObj = mysqli_fetch_object($executeOrganiser);
                if(isset($executeOrganiserObj->person_id)){
                    $lectureOrganiser = $executeOrganiserObj->name;
                } else {
                    if(DEBUG){
                        $err = 'executeOrganiser Error : ' . mysqli_error($con);
                    } else {
                        $err = 'executeOrganiser query failed.';
                    }
                }
            }
            
            
            
            //getting presenter info
            $sqlPresenter = "SELECT * FROM person WHERE person_id=$lecturePresenterID";
            $executePresenter = mysqli_query($con,$sqlPresenter);
            if($executePresenter){
                $executePresenterObj = mysqli_fetch_object($executePresenter);
                if(isset($executePresenterObj->person_id)){
                    $lecturePresenter = $executePresenterObj->name;
                } else {
                    if(DEBUG){
                        $err = 'executePresenter Error : ' . mysqli_error($con);
                    } else {
                        $err = 'executePresenter query failed.';
                    }
                }
            }
        }
        mysqli_free_result($lectureSqlResult);
    } else {
        if (DEBUG) {
            $err = 'lectureSqlResult Error : ' . mysqli_error($con);
        }
    }
}


?>

<!DOCTYPE html>
<!-- 
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.0.3
Version: 1.5.5
Author: KeenThemes
Website: http://www.keenthemes.com/
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
<meta charset="utf-8"/>
<title>Metronic | Pages - News View</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<meta content="" name="description"/>
<meta content="" name="author"/>
<meta name="MobileOptimized" content="320">
<?php include(basePath('admin/list_header_style.php')); ?>
<link rel="shortcut icon" href="<?php echo baseUrl('admin/'); ?>favicon.ico"/>
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="page-header-fixed">
        <?php
        include'../top_navigation.php';
        ?>
        <!-- BEGIN HEADER -->
        <?php include(basePath('admin/header.php')); ?>
        <!-- END HEADER -->
<div class="clearfix">
</div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
	<!-- BEGIN SIDEBAR -->
	<div class="page-sidebar-wrapper">
		<div class="page-sidebar navbar-collapse collapse">
                    <!-- BEGIN SIDEBAR MENU -->
                    <?php
                    include (basePath('admin/sidebar.php'));
                    ?>
                    <!-- END SIDEBAR MENU -->
		</div>
	</div>
	<!-- END SIDEBAR -->
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
                    <?php
                    include '../template_settings.php';
                    ?>
                    <!-- END STYLE CUSTOMIZER -->
			<!-- BEGIN PAGE HEADER-->
			<div class="row">
				<div class="col-md-12">
                                    <?php if ($err != '') { ?>
                                        <div class="alert alert-warning alert-dismissable col-md-9">
                                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                            <?php echo $err; ?>
                                        </div>
                                    <?php } ?>
                                    <?php if ($msg != '') { ?>
                                        <div class="alert alert-success alert-dismissable">
                                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                            <?php echo $msg; ?>
                                        </div>
                                    <?php } ?>
					<!-- BEGIN PAGE TITLE & BREADCRUMB-->
					<h3 class="page-title">
					Lecture Module
                                        </h3>
					<ul class="page-breadcrumb breadcrumb">
						<li class="btn-group">
							<button type="button" class="btn blue dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="1000" data-close-others="true">
							<span>
								Actions
							</span>
							<i class="fa fa-angle-down"></i>
							</button>
							<ul class="dropdown-menu pull-right" role="menu">
								<li>
									<a href="#">Action</a>
								</li>
								<li>
									<a href="#">Another action</a>
								</li>
								<li>
									<a href="#">Something else here</a>
								</li>
								<li class="divider">
								</li>
								<li>
									<a href="#">Separated link</a>
								</li>
							</ul>
						</li>
						<li>
                                                    <i class="fa fa-home"></i>
                                                    <a href="<?php echo baseUrl('admin/dashboard.php'); ?>">Home</a>
                                                    <i class="fa fa-angle-right"></i>
                                                </li>
                                                <li>
                                                    <a href="<?php echo baseUrl('admin/lecture/'); ?>">Lectures</a>
                                                    <i class="fa fa-angle-right"></i>
                                                </li>
                                                <li>
                                                    <a href="<?php echo baseUrl('admin/lecture/lecture_details.php?id=' . $_GET['id']); ?>">Lecture Details</a>
                                                    <i class="fa fa-angle-right"></i>
                                                </li>
					</ul>
					<!-- END PAGE TITLE & BREADCRUMB-->
				</div>
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12 news-page blog-page">
					<div class="row">
						<div class="col-md-9 blog-tag-data">
                                                    <h3>Title: <strong><?php echo $lectureTitle; ?></strong></h3>
							<hr>
							<div class="news-item-page">
                                                            <p>
                                                                <u>Overview:</u> <br>
                                                                <?php echo $lectureOverview; ?>
                                                            </p>
							</div>
							<hr>
							
						</div>
						<div class="col-md-3">
							<h3>Important Info</h3>
							<div class="top-news">
								<a href="#" class="btn green">
								<span>
									Date & TIme
								</span>
								<em>Held On: <?php echo $lectureDate; ?></em>
								<em>Start Time: <?php echo $startTime; ?></em>
								<i class="fa fa-calendar top-news-icon"></i>
								</a>
								<a href="#" class="btn yellow">
								<span>
									Department
								</span>
								<em><?php echo $lectureDepartment; ?></em>
								<i class="fa  fa-bookmark-o top-news-icon"></i>
								</a>
								<a href="#" class="btn red">
								<span>
									Organiser
								</span>
								<em><?php echo $lectureOrganiser; ?></em>
								<i class="fa fa-cogs top-news-icon"></i>
								</a>
								<a href="#" class="btn blue">
								<span>
									Presenter
								</span>
								<em><?php echo $lecturePresenter; ?></em>
								<i class="fa fa-comment top-news-icon"></i>
								</a>
							</div>
							<div class="space20">
							</div>
							
							
							
						</div>
					</div>
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<?php include(basePath('admin/footer.php')); ?>
        <?php include(basePath('admin/footer_script.php')); ?>
        <?php include (basePath('admin/list_includes.php')); ?>

<script src="<?php echo baseUrl();?>admin/assets/custom/admin/index.js"></script>
</body>
<!-- END BODY -->
</html>