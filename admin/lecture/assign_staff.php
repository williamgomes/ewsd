<?php
include ('../../config/config.php');
if (!checkAdminLogin()) {
    $link = baseUrl('admin/index.php?err=' . base64_encode('Please login to access admin panel'));
    redirect($link);
}

$lecture = 0;
$staffs = array();

if (isset($_POST['save']) AND $_POST['save'] == 'Submit') {
    
    extract($_POST);

    if ($lecture == '') {
        $err = 'Lecture Title field is required!!';
    } elseif (sizeof($staffs) == 0) {
        $err = 'Staffs field is required!!';
    } else {
        /* Start :Checking the user already exist or not */
        foreach ($staffs as $staff) { //getting tag id from array
            $CheckSql = "SELECT * FROM lecture_staff,lectures WHERE lecture_staff.lecture_id='" . mysqli_real_escape_string($con, $lecture) . "' AND lectures.lecture_id='" . mysqli_real_escape_string($con, $lecture) . "' AND staff_id='" . mysqli_real_escape_string($con, $staff) . "'";
            $CheckSqlResult = mysqli_query($con, $CheckSql);
            if ($CheckSqlResult) {
                $countCheckSqlResult = mysqli_num_rows($CheckSqlResult);
                $CheckSqlResultRowObj = mysqli_fetch_object($CheckSqlResult);
                if ($countCheckSqlResult > 0) {
                    $err .= "Staff already assigned to <b>'$CheckSqlResultRowObj->title'</b>";
                } else {
                    

                    $addstaff = '';
                    $addstaff .=' lecture_id = "' . mysqli_real_escape_string($con, $lecture) . '"';
                    $addstaff .=', staff_id = "' . mysqli_real_escape_string($con, $staff) . '"';

                    $staffInsSql = "INSERT INTO lecture_staff SET $addstaff";
                    $staffInsSqlResult = mysqli_query($con, $staffInsSql);
                    if ($staffInsSqlResult) {
                        $msg = "Staff assigned successfully.";
//                        $link = "index.php?msg=" . base64_encode($msg);
//                        redirect($link);

                    } else {
                        if (DEBUG) {
                            $err = 'staffInsSqlResult Error: ' . mysqli_error($con);
                        } else {
                            $err = "staffInsSqlResult Query failed.";
                        }
                    }
                }
                mysqli_free_result($CheckSqlResult);
            } else {
                if (DEBUG) {
                    $err = 'CheckSqlResult Error: ' . mysqli_error($con);
                } else {
                    $err = "CheckSqlResult Query failed.";
                }
            }
        }
    }
    
}


//getting all lecture from database
$lectureArray = array();
$lectureSql = "SELECT * FROM lectures WHERE status='active'";
$lectureSqlResult = mysqli_query($con, $lectureSql);
if ($lectureSqlResult) {
    while ($lectureSqlResultRowObj = mysqli_fetch_object($lectureSqlResult)) {
        $lectureArray[] = $lectureSqlResultRowObj;
    }
    mysqli_free_result($lectureSqlResult);
} else {
    if (DEBUG) {
        echo 'lectureSqlResult Error : ' . mysqli_error($con);
    }
}



//getting all staff from database
$staffArray = array();
$staffSql = "SELECT * FROM staffs"
        . " LEFT JOIN person ON person.person_id=staffs.person_id"
        . " LEFT JOIN users ON users.user_id=staffs.user_id"
        . " WHERE users.status='active'";
$staffSqlResult = mysqli_query($con, $staffSql);
if ($staffSqlResult) {
    while ($staffSqlResultRowObj = mysqli_fetch_object($staffSqlResult)) {
        $staffArray[] = $staffSqlResultRowObj;
    }
    mysqli_free_result($staffSqlResult);
} else {
    if (DEBUG) {
        echo 'staffSqlResult Error : ' . mysqli_error($con);
    }
}
?>
<!DOCTYPE html>
<!-- 
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.0.3
Version: 1.5.5
Author: KeenThemes
Website: http://www.keenthemes.com/
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->
    <head>
        <meta charset="utf-8"/>
        <title><?php echo $config['SITE_NAME']; ?> | Admin Create</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1.0" name="viewport"/>
        <meta content="" name="description"/>
        <meta content="" name="author"/>
        <meta name="MobileOptimized" content="320">
        <?php
        include(basePath('admin/header.php'));
        ?>
        <link rel="stylesheet" type="text/css" href="<?php echo baseUrl(); ?>admin/assets/plugins/select2/select2_metro.css"/>
        <link rel="stylesheet" type="text/css" href="<?php echo baseUrl(); ?>admin/assets/plugins/clockface/css/clockface.css"/>
        <link rel="stylesheet" type="text/css" href="<?php echo baseUrl(); ?>admin/assets/plugins/bootstrap-timepicker/compiled/timepicker.css"/>
    </head>
    <!-- END HEAD -->
    <!-- BEGIN BODY -->
    <body class="page-header-fixed">
        <!-- BEGIN HEADER -->
        <?php
        include '../top_navigation.php';
        ?>
        <!-- END HEADER -->
        <div class="clearfix">
        </div>
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <!-- BEGIN SIDEBAR -->
            <div class="page-sidebar-wrapper">
                <div class="page-sidebar navbar-collapse collapse">
                    <!-- BEGIN SIDEBAR MENU -->
                    <?php
                    include(basePath('admin/sidebar.php'));
                    ?>
                    <!-- END SIDEBAR MENU -->
                </div>
            </div>
            <!-- END SIDEBAR -->
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <div class="page-content">
                    <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
                    <?php
                    include '../template_settings.php';
                    ?>
                    <!-- END STYLE CUSTOMIZER -->
                    <!-- BEGIN PAGE HEADER-->
                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <h3 class="page-title">
                                Lecture Module
                            </h3>
                            <ul class="page-breadcrumb breadcrumb">
                                <li class="btn-group">
                                    <button type="button" class="btn blue dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="1000" data-close-others="true">
                                        <span>
                                            Actions
                                        </span>
                                        <i class="fa fa-angle-down"></i>
                                    </button>
                                    <ul class="dropdown-menu pull-right" role="menu">
                                        <li>
                                            <a href="<?php echo baseUrl('admin/'); ?>#">Action</a>
                                        </li>
                                        <li>
                                            <a href="<?php echo baseUrl('admin/'); ?>#">Another action</a>
                                        </li>
                                        <li>
                                            <a href="<?php echo baseUrl('admin/'); ?>#">Something else here</a>
                                        </li>
                                        <li class="divider">
                                        </li>
                                        <li>
                                            <a href="<?php echo baseUrl('admin/'); ?>#">Separated link</a>
                                        </li>
                                    </ul>
                                </li>
                                <li>
                                    <i class="fa fa-home"></i>
                                    <a href="<?php echo baseUrl('admin/dashboard.php'); ?>">Home</a>
                                    <i class="fa fa-angle-right"></i>
                                </li>
                                <li>
                                    <a href="<?php echo baseUrl('admin/lecture/'); ?>">Lecture</a>
                                    <i class="fa fa-angle-right"></i>
                                </li>
                                <li>
                                    <a href="<?php echo baseUrl('admin/lecture/add_lecture.php'); ?>">Add Lecture</a>
                                    <i class="fa fa-angle-right"></i>
                                </li>
                            </ul>
                            <!-- END PAGE TITLE & BREADCRUMB-->
                        </div>
                    </div>
                    <!-- END PAGE HEADER-->
                    <!-- BEGIN PAGE CONTENT-->
                    <div class="row">


                        <div class="col-md-12 ">
                            <?php if ($err != '') { ?>
                                <div class="alert alert-warning alert-dismissable col-md-9">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                    <?php echo $err; ?>
                                </div>
                            <?php } ?>
                            <?php if ($msg != '') { ?>
                                <div class="alert alert-success alert-dismissable">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                    <?php echo $msg; ?>
                                </div>
                            <?php } ?>
                            <!-- BEGIN SAMPLE FORM PORTLET-->
                            <div class="portlet box green ">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-reorder"></i> Add Lecture
                                    </div>
                                    <div class="tools">
                                        <a href="<?php echo baseUrl('admin/'); ?>" class="collapse"></a>
                                        <a href="<?php echo baseUrl('admin/'); ?>#portlet-config" data-toggle="modal" class="config"></a>
                                        <a href="<?php echo baseUrl('admin/'); ?>" class="reload"></a>
                                        <a href="<?php echo baseUrl('admin/'); ?>" class="remove"></a>
                                    </div>
                                </div>
                                <div class="portlet-body form">
                                    <form class="form-horizontal" method="post" action="<?php echo baseUrl('admin/lecture/assign_staff.php'); ?>" role="form">
                                        <div class="form-body">
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Lecture Title:</label>
                                                <div class="col-md-9">
                                                    <select class="form-control" name="lecture">
                                                        <option value=""> -- Select Department -- </option>
                                                        <?php
                                                        $lectureArrayCounter = count($lectureArray);
                                                        if ($lectureArrayCounter > 0):
                                                            ?>
                                                            <?php for ($i = 0; $i < $lectureArrayCounter; $i++): ?>
                                                                <option value="<?php echo $lectureArray[$i]->lecture_id; ?>" <?php if($lecture == $lectureArray[$i]->lecture_id){ echo "selected"; } ?>><?php echo $lectureArray[$i]->title; ?></option>
                                                            <?php     endfor; ?>    
                                                        <?php endif; ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                    <label class="col-md-3 control-label">Staffs</label>
                                                    <div class="col-md-9">
                                                            <select multiple="" class="form-control" name="staffs[]">
                                                                <?php
                                                                $staffArrayCounter = count($staffArray);
                                                                if ($staffArrayCounter > 0):
                                                                    ?>
                                                                    <?php for ($i = 0; $i < $staffArrayCounter; $i++): ?>
                                                                <option value="<?php echo $staffArray[$i]->staff_id; ?>" <?php if(in_array($staffArray[$i]->staff_id, $staffs)){ echo "selected"; } ?>><?php echo $staffArray[$i]->name; ?></option>
                                                                    <?php endfor; /* $i=0; i<$adminArrayCounter; $++  */ ?>
                                                                <?php endif; /* count($adminArray) > 0 */ ?>   
                                                            </select>
                                                    </div>
                                            </div>
                                            <div class="form-actions fluid">
                                            <div class="col-md-offset-3 col-md-9">
                                                <button type="submit" name="save" value="Submit" class="btn green">Submit</button>
                                                <button type="reset" class="btn default">Cancel</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>





                        </div>
                    </div>

                </div>
            </div>
            <!-- END CONTENT -->
        </div>
        <!-- END CONTAINER -->
        <!-- BEGIN FOOTER -->
        <?php
        include basePath('admin/footer.php');
        ?>
        <?php
        include (basePath('admin/footer_script.php'));
        include (basePath('admin/form_includes.php'));
        ?>
        <script type="text/javascript" src="<?php echo baseUrl();?>admin/assets/plugins/clockface/js/clockface.js"></script>
        <script type="text/javascript" src="<?php echo baseUrl();?>admin/assets/plugins/select2/select2.min.js"></script>
        <!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="<?php echo baseUrl();?>admin/assets/plugins/select2/select2.min.js"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="<?php echo baseUrl();?>admin/assets/scripts/app.js"></script>
<script src="<?php echo baseUrl();?>admin/assets/scripts/form-samples.js"></script>
<!-- END PAGE LEVEL SCRIPTS -->
<!--        <script src="<?php echo baseUrl();?>admin/assets/custom/admin/admin_create.js"></script>-->
    </body>
    <!-- END BODY -->
</html>