<?php
$adminType = getSession("admin_type");
?>

<!-- BEGIN SIDEBAR MENU -->
<ul class="page-sidebar-menu">
        <li class="sidebar-toggler-wrapper">
                <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
                <div class="sidebar-toggler hidden-phone">
                </div>
                <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
        </li>
        <li class="sidebar-search-wrapper">
                <!-- BEGIN RESPONSIVE QUICK SEARCH FORM -->
                <form class="sidebar-search" action="extra_search.html" method="POST">
                        <div class="form-container">
<!--                                <div class="input-box">
                                        <a href="javascript:;" class="remove"></a>
                                        <input type="text" placeholder="Search..."/>
                                        <input type="button" class="submit" value=" "/>
                                </div>-->
                        </div>
                </form>
                <!-- END RESPONSIVE QUICK SEARCH FORM -->
        </li>
        <li class="start active ">
                <a href="index.html">
                <i class="fa fa-home"></i>
                <span class="title">
                        Dashboard
                </span>
                <span class="selected">
                </span>
                </a>
        </li>
<!--        <li class="">
                <a href="index_horizontal_menu.html">
                <i class="fa fa-briefcase"></i>
                <span class="title">
                        Dashboard 2
                </span>
                </a>
        </li>-->


        <li>
                <a class="active" href="javascript:;">
                <i class="fa fa-user"></i>
                <span class="title">
                        User Management
                </span>
                <span class="arrow ">
                </span>
                </a>
                <ul class="sub-menu">
                        <?php if($adminType == 'admin'): ?>
                        <li>
                                <a href="javascript:;">
                                Admins
                                <span class="arrow">
                                </span>
                                </a>
                                <ul class="sub-menu">
                                        <li>
                                                <a href="<?php echo baseUrl(); ?>admin/admin/add_admin.php">Add Admin</a>
                                        </li>
                                        <li>
                                                <a href="<?php echo baseUrl(); ?>admin/admin/">All Admins</a>
                                        </li>
                                        
                                </ul>
                        </li>
                        <?php endif; ?>
                        <li>
                                <a href="javascript:;">
                                Delegates
                                <span class="arrow">
                                </span>
                                </a>
                                <ul class="sub-menu">
<!--                                        <li>
                                                <a href="#">Add Delegate</a>
                                        </li>-->
                                        <li>
                                                <a href="<?php echo baseUrl(); ?>admin/delegate/">All Delegates</a>
                                        </li>
                                </ul>
                        </li>

                        <?php if($adminType == 'admin'): ?>
                        <li>
                                <a href="javascript:;">
                                Organizers
                                <span class="arrow">
                                </span>
                                </a>
                                <ul class="sub-menu">
                                        <li>
                                                <a href="<?php echo baseUrl(); ?>admin/organizer/add_organizer.php">Add Organizer</a>
                                        </li>
                                        <li>
                                                <a href="<?php echo baseUrl(); ?>admin/organizer/">All Organizers</a>
                                        </li>
                                </ul>
                        </li>
                        <?php endif; ?>
                        <li>
                                <a href="javascript:;">
                                Presenters
                                <span class="arrow">
                                </span>
                                </a>
                                <ul class="sub-menu">
                                        <?php if($adminType == 'admin'): ?>
                                        <li>
                                                <a href="<?php echo baseUrl(); ?>admin/presenter/add_presenter.php">Add Presenter</a>
                                        </li>
                                        <?php endif; ?>
                                        <li>
                                                <a href="<?php echo baseUrl(); ?>admin/presenter/">All Presenters</a>
                                        </li>
                                </ul>
                        </li>
                        <li>
                                <a href="javascript:;">
                                Staffs
                                <span class="arrow">
                                </span>
                                </a>
                                <ul class="sub-menu">
                                        <?php if($adminType == 'admin'): ?>
                                        <li>
                                                <a href="<?php echo baseUrl(); ?>admin/staff/add_staff.php">Add Staff</a>
                                        </li>
                                        <?php endif; ?>
                                        <li>
                                                <a href="<?php echo baseUrl(); ?>admin/staff/">All Staffs</a>
                                        </li>
                                </ul>
                        </li>
                        
                </ul>
        </li>
        
        <li class="">
                <a href="javascript:;">
                <i class="fa fa-bookmark-o"></i>
                <span class="title">
                        Lectures
                </span>
                <span class="arrow ">
                </span>
                </a>
                <ul class="sub-menu">
                        <?php if($adminType == 'organiser'): ?>
                        <li>
                                <a href="<?php echo baseUrl(); ?>admin/lecture/add_lecture.php">Add Lecture</a>
                        </li>
                        <?php endif; ?>
                        
                        <li>
                                <a href="<?php echo baseUrl(); ?>admin/lecture/">All Lectures</a>
                        </li>
                        
                        <?php if($adminType == 'organiser'): ?>
                        <li>
                                <a href="<?php echo baseUrl(); ?>admin/lecture/assign_staff.php">Assign Staffs</a>
                        </li>
                        <?php endif; ?>
                        
                </ul>
        </li>
        
        <li class="">
                <a href="javascript:;">
                <i class="fa fa-bookmark-o"></i>
                <span class="title">
                        Rooms
                </span>
                <span class="arrow ">
                </span>
                </a>
                <ul class="sub-menu">
                        <?php if($adminType == 'admin'): ?>
                        <li>
                                <a href="<?php echo baseUrl(); ?>admin/room/add_room.php">
                                Add New Room</a>
                        </li>
                        <?php endif; ?>
                        <li>
                                <a href="<?php echo baseUrl(); ?>admin/room/">
                                All Room</a>
                        </li>
                        
                </ul>
        </li>
        
        
        
        <li class="">
                <a href="javascript:;">
                <i class="fa fa-bookmark-o"></i>
                <span class="title">
                        Department
                </span>
                <span class="arrow ">
                </span>
                </a>
                <ul class="sub-menu">
                        <?php if($adminType == 'organiser'): ?>
                        <li>
                                <a href="<?php echo baseUrl(); ?>admin/department/add_department.php">
                                Add New Department</a>
                        </li>
                        <?php endif; ?>
                        <li>
                                <a href="<?php echo baseUrl(); ?>admin/department/">
                                All Departments</a>
                        </li>
                        
                </ul>
        </li>
</ul>
<!-- END SIDEBAR MENU -->