<?php
include ('../../config/config.php');
if (!checkAdminLogin()) {
    $link = baseUrl('admin/index.php?err=' . base64_encode('Please login to access admin panel'));
    redirect($link);
}

$name = '';
$capacity = '';


if(isset($_GET['id']) AND $_GET['id'] != ""){
    
    $roomID = base64_decode($_GET['id']);
    
    $getRoomSql = "SELECT * FROM rooms WHERE room_id=$roomID";
    $executeRoomSql = mysqli_query($con, $getRoomSql);
    if($executeRoomSql){
        $executeRoomSqlObj = mysqli_fetch_object($executeRoomSql);
        if(isset($executeRoomSqlObj->room_id)){
            $name = $executeRoomSqlObj->room_name;
            $capacity = $executeRoomSqlObj->capacity;
        }
    } else {
        if(DEBUG){
            $err = "executeRoomSql error: " . mysqli_error($con);
        } else {
            $err = "executeRoomSql query failed.";
        }
    }
    
}

if (isset($_POST['save']) AND $_POST['save'] == 'Submit') {
    extract($_POST);
    
    if(isset($_GET['id']) AND $_GET['id'] != ""){
        
        if ($name == '') {
            $err = 'Room Name field is required!!';
        } elseif ($capacity == '') {
            $err = 'Room Capacity field is required!!';
        } elseif (!is_numeric($capacity)) {
            $err = 'Room Capacity must be numeric';
        }

        if ($err == '') {

            $updateRoom = '';
            $updateRoom .=' room_name = "' . mysqli_real_escape_string($con, $name) . '"';
            $updateRoom .=', capacity = "' . mysqli_real_escape_string($con, $capacity) . '"';

            $roomUpdateSql = "UPDATE rooms SET $updateRoom WHERE room_id=$roomID";
            $roomUpdateSqlResult = mysqli_query($con, $roomUpdateSql);
            if ($roomUpdateSqlResult) {
                $msg = "Room updated successfully.";
                $link = "index.php?msg=" . base64_encode($msg);
                redirect($link);
            } else {
                if (DEBUG) {
                    $err = 'roomUpdateSqlResult Error: ' . mysqli_error($con);
                } else {
                    $err = "roomUpdateSqlResult Query failed.";
                }
            }
        }
        
    } else {
        
        if ($name == '') {
            $err = 'Room Name field is required!!';
        } elseif ($capacity == '') {
            $err = 'Room Capacity field is required!!';
        } elseif (!is_numeric($capacity)) {
            $err = 'Room Capacity must be numeric';
        } else {
            /* Start :Checking the user already exist or not */
            $CheckSql = "SELECT room_id FROM rooms WHERE room_name='" . mysqli_real_escape_string($con, $name) . "' AND capacity='" . mysqli_real_escape_string($con, $capacity) . "'";
            $CheckSqlResult = mysqli_query($con, $CheckSql);
            if ($CheckSqlResult) {
                $countCheckSqlResult = mysqli_num_rows($CheckSqlResult);
                $CheckSqlResultRowObj = mysqli_fetch_object($CheckSqlResult);
                if ($countCheckSqlResult > 0) {
                    $err = '(<b>' . $name . '</b>) with capacity (<b>' . $capacity . '</b>) already exist in databse ';
                }
                mysqli_free_result($CheckSqlResult);
            } else {
                if (DEBUG) {
                    echo 'adminCheckSqlResult Error: ' . mysqli_error($con);
                }
                $err = "Query failed.";
            }

            /* End :Checking the user already exist or not */
        }



        if ($err == '') {

            $addRoom = '';
            $addRoom .=' room_name = "' . mysqli_real_escape_string($con, $name) . '"';
            $addRoom .=', capacity = "' . mysqli_real_escape_string($con, $capacity) . '"';

            $roomInsSql = "INSERT INTO rooms SET $addRoom";
            $roomInsSqlResult = mysqli_query($con, $roomInsSql);
            if ($roomInsSqlResult) {
                $msg = "Room created successfully.";
                $link = "index.php?msg=" . base64_encode($msg);
                redirect($link);
            } else {
                if (DEBUG) {
                    $err = 'roomInsSqlResult Error: ' . mysqli_error($con);
                } else {
                    $err = "roomInsSqlResult Query failed.";
                }
            }
        }
        
    }
    
    

    
}
$base_name = basename(__FILE__);
?>
<!DOCTYPE html>
<!-- 
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.0.3
Version: 1.5.5
Author: KeenThemes
Website: http://www.keenthemes.com/
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->
    <head>
        <meta charset="utf-8"/>
        <title><?php echo $config['SITE_NAME']; ?> | Room Create</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1.0" name="viewport"/>
        <meta content="" name="description"/>
        <meta content="" name="author"/>
        <meta name="MobileOptimized" content="320">
        <?php
        include(basePath('admin/header.php'));
        ?>
    </head>
    <!-- END HEAD -->
    <!-- BEGIN BODY -->
    <body class="page-header-fixed">
        <!-- BEGIN HEADER -->
        <?php
        include '../top_navigation.php';
        ?>
        <!-- END HEADER -->
        <div class="clearfix">
        </div>
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <!-- BEGIN SIDEBAR -->
            <div class="page-sidebar-wrapper">
                <div class="page-sidebar navbar-collapse collapse">
                    <!-- BEGIN SIDEBAR MENU -->
                    <?php
                    include(basePath('admin/sidebar.php'));
                    ?>
                    <!-- END SIDEBAR MENU -->
                </div>
            </div>
            <!-- END SIDEBAR -->
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <div class="page-content">
                    <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
                    <?php
                    include '../template_settings.php';
                    ?>
                    <!-- END STYLE CUSTOMIZER -->
                    <!-- BEGIN PAGE HEADER-->
                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <h3 class="page-title">
                                Room Module
                            </h3>
                            <ul class="page-breadcrumb breadcrumb">
                                <li class="btn-group">
                                    <button type="button" class="btn blue dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="1000" data-close-others="true">
                                        <span>
                                            Actions
                                        </span>
                                        <i class="fa fa-angle-down"></i>
                                    </button>
                                    <ul class="dropdown-menu pull-right" role="menu">
                                        <li>
                                            <a href="<?php echo baseUrl('admin/'); ?>#">Action</a>
                                        </li>
                                        <li>
                                            <a href="<?php echo baseUrl('admin/'); ?>#">Another action</a>
                                        </li>
                                        <li>
                                            <a href="<?php echo baseUrl('admin/'); ?>#">Something else here</a>
                                        </li>
                                        <li class="divider">
                                        </li>
                                        <li>
                                            <a href="<?php echo baseUrl('admin/'); ?>#">Separated link</a>
                                        </li>
                                    </ul>
                                </li>
                                <li>
                                    <i class="fa fa-home"></i>
                                    <a href="<?php echo baseUrl('admin/dashboard.php'); ?>">Home</a>
                                    <i class="fa fa-angle-right"></i>
                                </li>
                                <li>
                                    <a href="<?php echo baseUrl('admin/room/'); ?>">Room</a>
                                    <i class="fa fa-angle-right"></i>
                                </li>
                                <?php if(isset($_GET['id']) AND $_GET['id'] != ""): ?>
                                <li>
                                    <a href="<?php echo baseUrl('admin/room/add_room.php?id=' . $_GET['id']); ?>">Edit Room</a>
                                    <i class="fa fa-angle-right"></i>
                                </li>
                                <?php else: ?>
                                <li>
                                    <a href="<?php echo baseUrl('admin/room/add_room.php'); ?>">Add Room</a>
                                    <i class="fa fa-angle-right"></i>
                                </li>
                                <?php endif; ?>
                                
                            </ul>
                            <!-- END PAGE TITLE & BREADCRUMB-->
                        </div>
                    </div>
                    <!-- END PAGE HEADER-->
                    <!-- BEGIN PAGE CONTENT-->
                    <div class="row">


                        <div class="col-md-12 ">
                            <?php if ($err != '') { ?>
                                <div class="alert alert-warning alert-dismissable col-md-9">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                    <?php echo $err; ?>
                                </div>
                            <?php } ?>
                            <?php if ($msg != '') { ?>
                                <div class="alert alert-success alert-dismissable">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                    <?php echo $msg; ?>
                                </div>
                            <?php } ?>
                            <!-- BEGIN SAMPLE FORM PORTLET-->
                            <div class="portlet box green ">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <?php if(isset($_GET['id']) AND $_GET['id'] != ""): ?>
                                        <i class="fa fa-reorder"></i> Edit Room
                                        <?php else: ?>
                                        <i class="fa fa-reorder"></i> Add Room
                                        <?php endif; ?>
                                        
                                    </div>
                                    <div class="tools">
                                        <a href="<?php echo baseUrl('admin/'); ?>" class="collapse"></a>
                                        <a href="<?php echo baseUrl('admin/'); ?>#portlet-config" data-toggle="modal" class="config"></a>
                                        <a href="<?php echo baseUrl('admin/'); ?>" class="reload"></a>
                                        <a href="<?php echo baseUrl('admin/'); ?>" class="remove"></a>
                                    </div>
                                </div>
                                <div class="portlet-body form">
                                    <?php if(isset($_GET['id']) AND $_GET['id'] != ""): ?>
                                    <form class="form-horizontal" method="post" action="<?php echo baseUrl('admin/room/add_room.php?id=' . $_GET['id']); ?>" role="form">
                                    <?php else: ?>
                                    <form class="form-horizontal" method="post" action="<?php echo baseUrl('admin/room/add_room.php'); ?>" role="form">
                                    <?php endif; ?>
                                    
                                        <div class="form-body">
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Room Name:</label>
                                                <div class="col-md-9">
                                                    <input type="text" name="name" class="form-control required" value="<?php echo $name; ?>" required="required"/>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Room Capacity:</label>
                                                <div class="col-md-9">
                                                    <input type="number" name="capacity" class="form-control required" value="<?php echo $capacity; ?>"  required="required" />
                                                </div>
                                            </div>
                                            
                                        </div>
                                        <div class="form-actions fluid">
                                            <div class="col-md-offset-3 col-md-9">
                                                <button type="submit" name="save" value="Submit" class="btn green">Submit</button>
                                                <button type="reset" class="btn default">Cancel</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>





                        </div>
                    </div>

                </div>
            </div>
            <!-- END CONTENT -->
        </div>
        <!-- END CONTAINER -->
        <!-- BEGIN FOOTER -->
        <?php
        include basePath('admin/footer.php');
        ?>
        <?php
        include (basePath('admin/footer_script.php'));
        include (basePath('admin/form_includes.php'));
        ?>
<!--        <script src="<?php echo baseUrl();?>admin/assets/custom/admin/admin_create.js"></script>-->
    </body>
    <!-- END BODY -->
</html>