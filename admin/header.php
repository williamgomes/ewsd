<link href="<?php echo baseUrl('admin/'); ?>assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo baseUrl('admin/'); ?>assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo baseUrl('admin/'); ?>assets/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
<!-- END GLOBAL MANDATORY STYLES -->

<!-- BEGIN THEME STYLES -->
<link href="<?php echo baseUrl('admin/'); ?>assets/css/style-metronic.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo baseUrl('admin/'); ?>assets/css/style.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo baseUrl('admin/'); ?>assets/css/style-responsive.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo baseUrl('admin/'); ?>assets/css/plugins.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo baseUrl('admin/'); ?>assets/css/themes/default.css" rel="stylesheet" type="text/css" id="style_color"/>
<link href="<?php echo baseUrl('admin/'); ?>assets/css/custom.css" rel="stylesheet" type="text/css"/>


<link rel="stylesheet" type="text/css" href="<?php echo baseUrl('admin/'); ?>assets/plugins/bootstrap-datepicker/css/datepicker.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo baseUrl('admin/'); ?>assets/plugins/bootstrap-timepicker/compiled/timepicker.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo baseUrl('admin/'); ?>assets/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo baseUrl('admin/'); ?>assets/plugins/bootstrap-datetimepicker/css/datetimepicker.css"/>


<!-- END THEME STYLES -->
<link rel="shortcut icon" href="<?php echo baseUrl('admin/'); ?>favicon.ico"/>