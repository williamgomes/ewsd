<?php
include '../config/config.php';
?>

<!DOCTYPE html>
<!-- 
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.0.3
Version: 1.5.5
Author: KeenThemes
Website: http://www.keenthemes.com/
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">

    <!--<![endif]-->
    <!-- BEGIN HEAD -->
    <head>
        <meta charset="utf-8"/>
        <title><?php echo $config['SITE_NAME']; ?></title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1.0" name="viewport"/>
        <meta content="" name="description"/>
        <meta content="" name="author"/>
        <meta name="MobileOptimized" content="320">
        <?php include(basePath('admin/list_header_style.php')); ?>
        <link rel="shortcut icon" href="favicon.ico"/>
    </head>
    <!-- END HEAD -->
    <!-- BEGIN BODY -->
    <body class="page-header-fixed">
        <!-- BEGIN HEADER -->
        <?php include(basePath('admin/header_menu.php')); ?>
        <!-- END HEADER -->
        <div class="clearfix">
        </div>
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <!-- BEGIN SIDEBAR -->
            <div class="page-sidebar-wrapper">
                <div class="page-sidebar navbar-collapse collapse">
                    <!-- BEGIN SIDEBAR MENU -->
                    <?php include_once(basePath('admin/sidebar.php')); ?>
                    <!-- END SIDEBAR MENU -->
                </div>
            </div>
            <!-- END SIDEBAR -->
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <div class="page-content">
                    <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
                    <div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                    <h4 class="modal-title">Modal title</h4>
                                </div>
                                <div class="modal-body">
                                    Widget settings form goes here
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn blue">Save changes</button>
                                    <button type="button" class="btn default" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                            <!-- /.modal-content -->
                        </div>
                        <!-- /.modal-dialog -->
                    </div>
                    <!-- /.modal -->
                    <!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
                    <!-- BEGIN STYLE CUSTOMIZER -->
                    <div class="theme-panel hidden-xs hidden-sm">
                        <div class="toggler">
                        </div>
                        <div class="toggler-close">
                        </div>
                        <div class="theme-options">
                            <div class="theme-option theme-colors clearfix">
                                <span>
                                    THEME COLOR
                                </span>
                                <ul>
                                    <li class="color-black current color-default" data-style="default">
                                    </li>
                                    <li class="color-blue" data-style="blue">
                                    </li>
                                    <li class="color-brown" data-style="brown">
                                    </li>
                                    <li class="color-purple" data-style="purple">
                                    </li>
                                    <li class="color-grey" data-style="grey">
                                    </li>
                                    <li class="color-white color-light" data-style="light">
                                    </li>
                                </ul>
                            </div>
                            <div class="theme-option">
                                <span>
                                    Layout
                                </span>
                                <select class="layout-option form-control input-small">
                                    <option value="fluid" selected="selected">Fluid</option>
                                    <option value="boxed">Boxed</option>
                                </select>
                            </div>
                            <div class="theme-option">
                                <span>
                                    Header
                                </span>
                                <select class="header-option form-control input-small">
                                    <option value="fixed" selected="selected">Fixed</option>
                                    <option value="default">Default</option>
                                </select>
                            </div>
                            <div class="theme-option">
                                <span>
                                    Sidebar
                                </span>
                                <select class="sidebar-option form-control input-small">
                                    <option value="fixed">Fixed</option>
                                    <option value="default" selected="selected">Default</option>
                                </select>
                            </div>
                            <div class="theme-option">
                                <span>
                                    Sidebar Position
                                </span>
                                <select class="sidebar-pos-option form-control input-small">
                                    <option value="left" selected="selected">Left</option>
                                    <option value="right">Right</option>
                                </select>
                            </div>
                            <div class="theme-option">
                                <span>
                                    Footer
                                </span>
                                <select class="footer-option form-control input-small">
                                    <option value="fixed">Fixed</option>
                                    <option value="default" selected="selected">Default</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <!-- END STYLE CUSTOMIZER -->
                    <!-- BEGIN PAGE HEADER-->
                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <h3 class="page-title">
                                Managed Datatables <small>managed datatable samples</small>
                            </h3>
                            <ul class="page-breadcrumb breadcrumb">
                                <li class="btn-group">
                                    <button type="button" class="btn blue dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="1000" data-close-others="true">
                                        <span>
                                            Actions
                                        </span>
                                        <i class="fa fa-angle-down"></i>
                                    </button>
                                    <ul class="dropdown-menu pull-right" role="menu">
                                        <li>
                                            <a href="#">Action</a>
                                        </li>
                                        <li>
                                            <a href="#">Another action</a>
                                        </li>
                                        <li>
                                            <a href="#">Something else here</a>
                                        </li>
                                        <li class="divider">
                                        </li>
                                        <li>
                                            <a href="#">Separated link</a>
                                        </li>
                                    </ul>
                                </li>
                                <li>
                                    <i class="fa fa-home"></i>
                                    <a href="index.html">Home</a>
                                    <i class="fa fa-angle-right"></i>
                                </li>
                                <li>
                                    <a href="#">Data Tables</a>
                                    <i class="fa fa-angle-right"></i>
                                </li>
                                <li>
                                    <a href="#">Managed Datatables</a>
                                </li>
                            </ul>
                            <!-- END PAGE TITLE & BREADCRUMB-->
                        </div>
                    </div>
                    <!-- END PAGE HEADER-->
                    <!-- BEGIN PAGE CONTENT-->
                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN EXAMPLE TABLE PORTLET-->
                            <div class="portlet box light-grey">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-globe"></i>Managed Table
                                    </div>
                                    <div class="tools">
                                        <a href="javascript:;" class="collapse"></a>
                                        <a href="#portlet-config" data-toggle="modal" class="config"></a>
                                        <a href="javascript:;" class="reload"></a>
                                        <a href="javascript:;" class="remove"></a>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div class="table-toolbar">
                                        <div class="btn-group">
                                            <button id="sample_editable_1_new" class="btn green">
                                                Add New <i class="fa fa-plus"></i>
                                            </button>
                                        </div>
                                        <div class="btn-group pull-right">
                                            <button class="btn dropdown-toggle" data-toggle="dropdown">Tools <i class="fa fa-angle-down"></i>
                                            </button>
                                            <ul class="dropdown-menu pull-right">
                                                <li>
                                                    <a href="#">Print</a>
                                                </li>
                                                <li>
                                                    <a href="#">Save as PDF</a>
                                                </li>
                                                <li>
                                                    <a href="#">Export to Excel</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <table class="table table-striped table-bordered table-hover" id="sample_1">
                                        <thead>
                                            <tr>
                                                <th class="table-checkbox">
                                                    <input type="checkbox" class="group-checkable" data-set="#sample_1 .checkboxes"/>
                                                </th>
                                                <th>
                                                    Username
                                                </th>
                                                <th>
                                                    Email
                                                </th>
                                                <th>
                                                    Points
                                                </th>
                                                <th>
                                                    Joined
                                                </th>
                                                <th>
                                                    &nbsp;
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr class="odd gradeX">
                                                <td>
                                                    <input type="checkbox" class="checkboxes" value="1"/>
                                                </td>
                                                <td>
                                                    shuxer
                                                </td>
                                                <td>
                                                    <a href="mailto:shuxer@gmail.com">shuxer@gmail.com</a>
                                                </td>
                                                <td>
                                                    120
                                                </td>
                                                <td class="center">
                                                    12 Jan 2012
                                                </td>
                                                <td>
                                                    <span class="label label-sm label-success">
                                                        Approved
                                                    </span>
                                                </td>
                                            </tr>
                                            <tr class="odd gradeX">
                                                <td>
                                                    <input type="checkbox" class="checkboxes" value="1"/>
                                                </td>
                                                <td>
                                                    looper
                                                </td>
                                                <td>
                                                    <a href="mailto:looper90@gmail.com">looper90@gmail.com</a>
                                                </td>
                                                <td>
                                                    120
                                                </td>
                                                <td class="center">
                                                    12.12.2011
                                                </td>
                                                <td>
                                                    <span class="label label-sm label-warning">
                                                        Suspended
                                                    </span>
                                                </td>
                                            </tr>
                                            <tr class="odd gradeX">
                                                <td>
                                                    <input type="checkbox" class="checkboxes" value="1"/>
                                                </td>
                                                <td>
                                                    userwow
                                                </td>
                                                <td>
                                                    <a href="mailto:userwow@yahoo.com">userwow@yahoo.com</a>
                                                </td>
                                                <td>
                                                    20
                                                </td>
                                                <td class="center">
                                                    12.12.2012
                                                </td>
                                                <td>
                                                    <span class="label label-sm label-success">
                                                        Approved
                                                    </span>
                                                </td>
                                            </tr>
                                            <tr class="odd gradeX">
                                                <td>
                                                    <input type="checkbox" class="checkboxes" value="1"/>
                                                </td>
                                                <td>
                                                    user1wow
                                                </td>
                                                <td>
                                                    <a href="mailto:userwow@gmail.com">userwow@gmail.com</a>
                                                </td>
                                                <td>
                                                    20
                                                </td>
                                                <td class="center">
                                                    12.12.2012
                                                </td>
                                                <td>
                                                    <span class="label label-sm label-default">
                                                        Blocked
                                                    </span>
                                                </td>
                                            </tr>
                                            <tr class="odd gradeX">
                                                <td>
                                                    <input type="checkbox" class="checkboxes" value="1"/>
                                                </td>
                                                <td>
                                                    restest
                                                </td>
                                                <td>
                                                    <a href="mailto:userwow@gmail.com">test@gmail.com</a>
                                                </td>
                                                <td>
                                                    20
                                                </td>
                                                <td class="center">
                                                    12.12.2012
                                                </td>
                                                <td>
                                                    <span class="label label-sm label-success">
                                                        Approved
                                                    </span>
                                                </td>
                                            </tr>
                                            <tr class="odd gradeX">
                                                <td>
                                                    <input type="checkbox" class="checkboxes" value="1"/>
                                                </td>
                                                <td>
                                                    foopl
                                                </td>
                                                <td>
                                                    <a href="mailto:userwow@gmail.com">good@gmail.com</a>
                                                </td>
                                                <td>
                                                    20
                                                </td>
                                                <td class="center">
                                                    19.11.2010
                                                </td>
                                                <td>
                                                    <span class="label label-sm label-success">
                                                        Approved
                                                    </span>
                                                </td>
                                            </tr>
                                            <tr class="odd gradeX">
                                                <td>
                                                    <input type="checkbox" class="checkboxes" value="1"/>
                                                </td>
                                                <td>
                                                    weep
                                                </td>
                                                <td>
                                                    <a href="mailto:userwow@gmail.com">good@gmail.com</a>
                                                </td>
                                                <td>
                                                    20
                                                </td>
                                                <td class="center">
                                                    19.11.2010
                                                </td>
                                                <td>
                                                    <span class="label label-sm label-success">
                                                        Approved
                                                    </span>
                                                </td>
                                            </tr>
                                            <tr class="odd gradeX">
                                                <td>
                                                    <input type="checkbox" class="checkboxes" value="1"/>
                                                </td>
                                                <td>
                                                    coop
                                                </td>
                                                <td>
                                                    <a href="mailto:userwow@gmail.com">good@gmail.com</a>
                                                </td>
                                                <td>
                                                    20
                                                </td>
                                                <td class="center">
                                                    19.11.2010
                                                </td>
                                                <td>
                                                    <span class="label label-sm label-success">
                                                        Approved
                                                    </span>
                                                </td>
                                            </tr>
                                            <tr class="odd gradeX">
                                                <td>
                                                    <input type="checkbox" class="checkboxes" value="1"/>
                                                </td>
                                                <td>
                                                    pppol
                                                </td>
                                                <td>
                                                    <a href="mailto:userwow@gmail.com">good@gmail.com</a>
                                                </td>
                                                <td>
                                                    20
                                                </td>
                                                <td class="center">
                                                    19.11.2010
                                                </td>
                                                <td>
                                                    <span class="label label-sm label-success">
                                                        Approved
                                                    </span>
                                                </td>
                                            </tr>
                                            <tr class="odd gradeX">
                                                <td>
                                                    <input type="checkbox" class="checkboxes" value="1"/>
                                                </td>
                                                <td>
                                                    test
                                                </td>
                                                <td>
                                                    <a href="mailto:userwow@gmail.com">good@gmail.com</a>
                                                </td>
                                                <td>
                                                    20
                                                </td>
                                                <td class="center">
                                                    19.11.2010
                                                </td>
                                                <td>
                                                    <span class="label label-sm label-success">
                                                        Approved
                                                    </span>
                                                </td>
                                            </tr>
                                            <tr class="odd gradeX">
                                                <td>
                                                    <input type="checkbox" class="checkboxes" value="1"/>
                                                </td>
                                                <td>
                                                    userwow
                                                </td>
                                                <td>
                                                    <a href="mailto:userwow@gmail.com">userwow@gmail.com</a>
                                                </td>
                                                <td>
                                                    20
                                                </td>
                                                <td class="center">
                                                    12.12.2012
                                                </td>
                                                <td>
                                                    <span class="label label-sm label-default">
                                                        Blocked
                                                    </span>
                                                </td>
                                            </tr>
                                            <tr class="odd gradeX">
                                                <td>
                                                    <input type="checkbox" class="checkboxes" value="1"/>
                                                </td>
                                                <td>
                                                    test
                                                </td>
                                                <td>
                                                    <a href="mailto:userwow@gmail.com">test@gmail.com</a>
                                                </td>
                                                <td>
                                                    20
                                                </td>
                                                <td class="center">
                                                    12.12.2012
                                                </td>
                                                <td>
                                                    <span class="label label-sm label-success">
                                                        Approved
                                                    </span>
                                                </td>
                                            </tr>
                                            <tr class="odd gradeX">
                                                <td>
                                                    <input type="checkbox" class="checkboxes" value="1"/>
                                                </td>
                                                <td>
                                                    goop
                                                </td>
                                                <td>
                                                    <a href="mailto:userwow@gmail.com">good@gmail.com</a>
                                                </td>
                                                <td>
                                                    20
                                                </td>
                                                <td class="center">
                                                    12.11.2010
                                                </td>
                                                <td>
                                                    <span class="label label-sm label-success">
                                                        Approved
                                                    </span>
                                                </td>
                                            </tr>
                                            <tr class="odd gradeX">
                                                <td>
                                                    <input type="checkbox" class="checkboxes" value="1"/>
                                                </td>
                                                <td>
                                                    weep
                                                </td>
                                                <td>
                                                    <a href="mailto:userwow@gmail.com">good@gmail.com</a>
                                                </td>
                                                <td>
                                                    20
                                                </td>
                                                <td class="center">
                                                    15.11.2011
                                                </td>
                                                <td>
                                                    <span class="label label-sm label-default">
                                                        Blocked
                                                    </span>
                                                </td>
                                            </tr>
                                            <tr class="odd gradeX">
                                                <td>
                                                    <input type="checkbox" class="checkboxes" value="1"/>
                                                </td>
                                                <td>
                                                    toopl
                                                </td>
                                                <td>
                                                    <a href="mailto:userwow@gmail.com">good@gmail.com</a>
                                                </td>
                                                <td>
                                                    20
                                                </td>
                                                <td class="center">
                                                    16.11.2010
                                                </td>
                                                <td>
                                                    <span class="label label-sm label-success">
                                                        Approved
                                                    </span>
                                                </td>
                                            </tr>
                                            <tr class="odd gradeX">
                                                <td>
                                                    <input type="checkbox" class="checkboxes" value="1"/>
                                                </td>
                                                <td>
                                                    userwow
                                                </td>
                                                <td>
                                                    <a href="mailto:userwow@gmail.com">userwow@gmail.com</a>
                                                </td>
                                                <td>
                                                    20
                                                </td>
                                                <td class="center">
                                                    9.12.2012
                                                </td>
                                                <td>
                                                    <span class="label label-sm label-default">
                                                        Blocked
                                                    </span>
                                                </td>
                                            </tr>
                                            <tr class="odd gradeX">
                                                <td>
                                                    <input type="checkbox" class="checkboxes" value="1"/>
                                                </td>
                                                <td>
                                                    tes21t
                                                </td>
                                                <td>
                                                    <a href="mailto:userwow@gmail.com">test@gmail.com</a>
                                                </td>
                                                <td>
                                                    20
                                                </td>
                                                <td class="center">
                                                    14.12.2012
                                                </td>
                                                <td>
                                                    <span class="label label-sm label-success">
                                                        Approved
                                                    </span>
                                                </td>
                                            </tr>
                                            <tr class="odd gradeX">
                                                <td>
                                                    <input type="checkbox" class="checkboxes" value="1"/>
                                                </td>
                                                <td>
                                                    fop
                                                </td>
                                                <td>
                                                    <a href="mailto:userwow@gmail.com">good@gmail.com</a>
                                                </td>
                                                <td>
                                                    20
                                                </td>
                                                <td class="center">
                                                    13.11.2010
                                                </td>
                                                <td>
                                                    <span class="label label-sm label-warning">
                                                        Suspended
                                                    </span>
                                                </td>
                                            </tr>
                                            <tr class="odd gradeX">
                                                <td>
                                                    <input type="checkbox" class="checkboxes" value="1"/>
                                                </td>
                                                <td>
                                                    kop
                                                </td>
                                                <td>
                                                    <a href="mailto:userwow@gmail.com">good@gmail.com</a>
                                                </td>
                                                <td>
                                                    20
                                                </td>
                                                <td class="center">
                                                    17.11.2010
                                                </td>
                                                <td>
                                                    <span class="label label-sm label-success">
                                                        Approved
                                                    </span>
                                                </td>
                                            </tr>
                                            <tr class="odd gradeX">
                                                <td>
                                                    <input type="checkbox" class="checkboxes" value="1"/>
                                                </td>
                                                <td>
                                                    vopl
                                                </td>
                                                <td>
                                                    <a href="mailto:userwow@gmail.com">good@gmail.com</a>
                                                </td>
                                                <td>
                                                    20
                                                </td>
                                                <td class="center">
                                                    19.11.2010
                                                </td>
                                                <td>
                                                    <span class="label label-sm label-success">
                                                        Approved
                                                    </span>
                                                </td>
                                            </tr>
                                            <tr class="odd gradeX">
                                                <td>
                                                    <input type="checkbox" class="checkboxes" value="1"/>
                                                </td>
                                                <td>
                                                    userwow
                                                </td>
                                                <td>
                                                    <a href="mailto:userwow@gmail.com">userwow@gmail.com</a>
                                                </td>
                                                <td>
                                                    20
                                                </td>
                                                <td class="center">
                                                    12.12.2012
                                                </td>
                                                <td>
                                                    <span class="label label-sm label-default">
                                                        Blocked
                                                    </span>
                                                </td>
                                            </tr>
                                            <tr class="odd gradeX">
                                                <td>
                                                    <input type="checkbox" class="checkboxes" value="1"/>
                                                </td>
                                                <td>
                                                    wap
                                                </td>
                                                <td>
                                                    <a href="mailto:userwow@gmail.com">test@gmail.com</a>
                                                </td>
                                                <td>
                                                    20
                                                </td>
                                                <td class="center">
                                                    12.12.2012
                                                </td>
                                                <td>
                                                    <span class="label label-sm label-success">
                                                        Approved
                                                    </span>
                                                </td>
                                            </tr>
                                            <tr class="odd gradeX">
                                                <td>
                                                    <input type="checkbox" class="checkboxes" value="1"/>
                                                </td>
                                                <td>
                                                    test
                                                </td>
                                                <td>
                                                    <a href="mailto:userwow@gmail.com">good@gmail.com</a>
                                                </td>
                                                <td>
                                                    20
                                                </td>
                                                <td class="center">
                                                    19.12.2010
                                                </td>
                                                <td>
                                                    <span class="label label-sm label-success">
                                                        Approved
                                                    </span>
                                                </td>
                                            </tr>
                                            <tr class="odd gradeX">
                                                <td>
                                                    <input type="checkbox" class="checkboxes" value="1"/>
                                                </td>
                                                <td>
                                                    toop
                                                </td>
                                                <td>
                                                    <a href="mailto:userwow@gmail.com">good@gmail.com</a>
                                                </td>
                                                <td>
                                                    20
                                                </td>
                                                <td class="center">
                                                    17.12.2010
                                                </td>
                                                <td>
                                                    <span class="label label-sm label-success">
                                                        Approved
                                                    </span>
                                                </td>
                                            </tr>
                                            <tr class="odd gradeX">
                                                <td>
                                                    <input type="checkbox" class="checkboxes" value="1"/>
                                                </td>
                                                <td>
                                                    weep
                                                </td>
                                                <td>
                                                    <a href="mailto:userwow@gmail.com">good@gmail.com</a>
                                                </td>
                                                <td>
                                                    20
                                                </td>
                                                <td class="center">
                                                    15.11.2011
                                                </td>
                                                <td>
                                                    <span class="label label-sm label-success">
                                                        Approved
                                                    </span>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- END EXAMPLE TABLE PORTLET-->
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-sm-12">
                            <!-- BEGIN EXAMPLE TABLE PORTLET-->
                            <div class="portlet box grey">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-user"></i>Table
                                    </div>
                                    <div class="actions">
                                        <a href="#" class="btn blue"><i class="fa fa-pencil"></i> Add</a>
                                        <div class="btn-group">
                                            <a class="btn green" href="#" data-toggle="dropdown">
                                                <i class="fa fa-cogs"></i> Tools <i class="fa fa-angle-down"></i>
                                            </a>
                                            <ul class="dropdown-menu pull-right">
                                                <li>
                                                    <a href="#"><i class="fa fa-pencil"></i> Edit</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-trash-o"></i> Delete</a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-ban"></i> Ban</a>
                                                </li>
                                                <li class="divider">
                                                </li>
                                                <li>
                                                    <a href="#"><i class="i"></i> Make admin</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <table class="table table-striped table-bordered table-hover" id="sample_2">
                                        <thead>
                                            <tr>
                                                <th style="width1:8px;">
                                                    <input type="checkbox" class="group-checkable" data-set="#sample_2 .checkboxes"/>
                                                </th>
                                                <th>
                                                    Username
                                                </th>
                                                <th>
                                                    Email
                                                </th>
                                                <th>
                                                    Status
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr class="odd gradeX">
                                                <td>
                                                    <input type="checkbox" class="checkboxes" value="1"/>
                                                </td>
                                                <td>
                                                    shuxer
                                                </td>
                                                <td>
                                                    <a href="mailto:shuxer@gmail.com">shuxer@gmail.com</a>
                                                </td>
                                                <td>
                                                    <span class="label label-sm label-success">
                                                        Approved
                                                    </span>
                                                </td>
                                            </tr>
                                            <tr class="odd gradeX">
                                                <td>
                                                    <input type="checkbox" class="checkboxes" value="1"/>
                                                </td>
                                                <td>
                                                    looper
                                                </td>
                                                <td>
                                                    <a href="mailto:looper90@gmail.com">looper90@gmail.com</a>
                                                </td>
                                                <td>
                                                    <span class="label label-sm label-warning">
                                                        Suspended
                                                    </span>
                                                </td>
                                            </tr>
                                            <tr class="odd gradeX">
                                                <td>
                                                    <input type="checkbox" class="checkboxes" value="1"/>
                                                </td>
                                                <td>
                                                    userwow
                                                </td>
                                                <td>
                                                    <a href="mailto:userwow@yahoo.com">userwow@yahoo.com</a>
                                                </td>
                                                <td>
                                                    <span class="label label-sm label-success">
                                                        Approved
                                                    </span>
                                                </td>
                                            </tr>
                                            <tr class="odd gradeX">
                                                <td>
                                                    <input type="checkbox" class="checkboxes" value="1"/>
                                                </td>
                                                <td>
                                                    user1wow
                                                </td>
                                                <td>
                                                    <a href="mailto:userwow@gmail.com">userwow@gmail.com</a>
                                                </td>
                                                <td>
                                                    <span class="label label-sm label-default">
                                                        Blocked
                                                    </span>
                                                </td>
                                            </tr>
                                            <tr class="odd gradeX">
                                                <td>
                                                    <input type="checkbox" class="checkboxes" value="1"/>
                                                </td>
                                                <td>
                                                    restest
                                                </td>
                                                <td>
                                                    <a href="mailto:userwow@gmail.com">test@gmail.com</a>
                                                </td>
                                                <td>
                                                    <span class="label label-sm label-success">
                                                        Approved
                                                    </span>
                                                </td>
                                            </tr>
                                            <tr class="odd gradeX">
                                                <td>
                                                    <input type="checkbox" class="checkboxes" value="1"/>
                                                </td>
                                                <td>
                                                    foopl
                                                </td>
                                                <td>
                                                    <a href="mailto:userwow@gmail.com">good@gmail.com</a>
                                                </td>
                                                <td>
                                                    <span class="label label-sm label-success">
                                                        Approved
                                                    </span>
                                                </td>
                                            </tr>
                                            <tr class="odd gradeX">
                                                <td>
                                                    <input type="checkbox" class="checkboxes" value="1"/>
                                                </td>
                                                <td>
                                                    weep
                                                </td>
                                                <td>
                                                    <a href="mailto:userwow@gmail.com">good@gmail.com</a>
                                                </td>
                                                <td>
                                                    <span class="label label-sm label-success">
                                                        Approved
                                                    </span>
                                                </td>
                                            </tr>
                                            <tr class="odd gradeX">
                                                <td>
                                                    <input type="checkbox" class="checkboxes" value="1"/>
                                                </td>
                                                <td>
                                                    coop
                                                </td>
                                                <td>
                                                    <a href="mailto:userwow@gmail.com">good@gmail.com</a>
                                                </td>
                                                <td>
                                                    <span class="label label-sm label-success">
                                                        Approved
                                                    </span>
                                                </td>
                                            </tr>
                                            <tr class="odd gradeX">
                                                <td>
                                                    <input type="checkbox" class="checkboxes" value="1"/>
                                                </td>
                                                <td>
                                                    pppol
                                                </td>
                                                <td>
                                                    <a href="mailto:userwow@gmail.com">good@gmail.com</a>
                                                </td>
                                                <td>
                                                    <span class="label label-sm label-success">
                                                        Approved
                                                    </span>
                                                </td>
                                            </tr>
                                            <tr class="odd gradeX">
                                                <td>
                                                    <input type="checkbox" class="checkboxes" value="1"/>
                                                </td>
                                                <td>
                                                    test
                                                </td>
                                                <td>
                                                    <a href="mailto:userwow@gmail.com">good@gmail.com</a>
                                                </td>
                                                <td>
                                                    <span class="label label-sm label-success">
                                                        Approved
                                                    </span>
                                                </td>
                                            </tr>
                                            <tr class="odd gradeX">
                                                <td>
                                                    <input type="checkbox" class="checkboxes" value="1"/>
                                                </td>
                                                <td>
                                                    userwow
                                                </td>
                                                <td>
                                                    <a href="mailto:userwow@gmail.com">userwow@gmail.com</a>
                                                </td>
                                                <td>
                                                    <span class="label label-sm label-default">
                                                        Blocked
                                                    </span>
                                                </td>
                                            </tr>
                                            <tr class="odd gradeX">
                                                <td>
                                                    <input type="checkbox" class="checkboxes" value="1"/>
                                                </td>
                                                <td>
                                                    test
                                                </td>
                                                <td>
                                                    <a href="mailto:userwow@gmail.com">test@gmail.com</a>
                                                </td>
                                                <td>
                                                    <span class="label label-sm label-success">
                                                        Approved
                                                    </span>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- END EXAMPLE TABLE PORTLET-->
                        </div>
                        <div class="col-md-6 col-sm-12">
                            <!-- BEGIN EXAMPLE TABLE PORTLET-->
                            <div class="portlet box purple">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-cogs"></i>Table
                                    </div>
                                    <div class="actions">
                                        <a href="#" class="btn green"><i class="fa fa-plus"></i> Add</a>
                                        <a href="#" class="btn yellow"><i class="fa fa-print"></i> Print</a>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <table class="table table-striped table-bordered table-hover" id="sample_3">
                                        <thead>
                                            <tr>
                                                <th class="table-checkbox">
                                                    <input type="checkbox" class="group-checkable" data-set="#sample_3 .checkboxes"/>
                                                </th>
                                                <th>
                                                    Username
                                                </th>
                                                <th>
                                                    Email
                                                </th>
                                                <th>
                                                    Status
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr class="odd gradeX">
                                                <td>
                                                    <input type="checkbox" class="checkboxes" value="1"/>
                                                </td>
                                                <td>
                                                    shuxer
                                                </td>
                                                <td>
                                                    <a href="mailto:shuxer@gmail.com">shuxer@gmail.com</a>
                                                </td>
                                                <td>
                                                    <span class="label label-sm label-success">
                                                        Approved
                                                    </span>
                                                </td>
                                            </tr>
                                            <tr class="odd gradeX">
                                                <td>
                                                    <input type="checkbox" class="checkboxes" value="1"/>
                                                </td>
                                                <td>
                                                    looper
                                                </td>
                                                <td>
                                                    <a href="mailto:looper90@gmail.com">looper90@gmail.com</a>
                                                </td>
                                                <td>
                                                    <span class="label label-sm label-warning">
                                                        Suspended
                                                    </span>
                                                </td>
                                            </tr>
                                            <tr class="odd gradeX">
                                                <td>
                                                    <input type="checkbox" class="checkboxes" value="1"/>
                                                </td>
                                                <td>
                                                    userwow
                                                </td>
                                                <td>
                                                    <a href="mailto:userwow@yahoo.com">userwow@yahoo.com</a>
                                                </td>
                                                <td>
                                                    <span class="label label-sm label-success">
                                                        Approved
                                                    </span>
                                                </td>
                                            </tr>
                                            <tr class="odd gradeX">
                                                <td>
                                                    <input type="checkbox" class="checkboxes" value="1"/>
                                                </td>
                                                <td>
                                                    user1wow
                                                </td>
                                                <td>
                                                    <a href="mailto:userwow@gmail.com">userwow@gmail.com</a>
                                                </td>
                                                <td>
                                                    <span class="label label-sm label-default">
                                                        Blocked
                                                    </span>
                                                </td>
                                            </tr>
                                            <tr class="odd gradeX">
                                                <td>
                                                    <input type="checkbox" class="checkboxes" value="1"/>
                                                </td>
                                                <td>
                                                    restest
                                                </td>
                                                <td>
                                                    <a href="mailto:userwow@gmail.com">test@gmail.com</a>
                                                </td>
                                                <td>
                                                    <span class="label label-sm label-success">
                                                        Approved
                                                    </span>
                                                </td>
                                            </tr>
                                            <tr class="odd gradeX">
                                                <td>
                                                    <input type="checkbox" class="checkboxes" value="1"/>
                                                </td>
                                                <td>
                                                    foopl
                                                </td>
                                                <td>
                                                    <a href="mailto:userwow@gmail.com">good@gmail.com</a>
                                                </td>
                                                <td>
                                                    <span class="label label-sm label-success">
                                                        Approved
                                                    </span>
                                                </td>
                                            </tr>
                                            <tr class="odd gradeX">
                                                <td>
                                                    <input type="checkbox" class="checkboxes" value="1"/>
                                                </td>
                                                <td>
                                                    weep
                                                </td>
                                                <td>
                                                    <a href="mailto:userwow@gmail.com">good@gmail.com</a>
                                                </td>
                                                <td>
                                                    <span class="label label-sm label-success">
                                                        Approved
                                                    </span>
                                                </td>
                                            </tr>
                                            <tr class="odd gradeX">
                                                <td>
                                                    <input type="checkbox" class="checkboxes" value="1"/>
                                                </td>
                                                <td>
                                                    coop
                                                </td>
                                                <td>
                                                    <a href="mailto:userwow@gmail.com">good@gmail.com</a>
                                                </td>
                                                <td>
                                                    <span class="label label-sm label-success">
                                                        Approved
                                                    </span>
                                                </td>
                                            </tr>
                                            <tr class="odd gradeX">
                                                <td>
                                                    <input type="checkbox" class="checkboxes" value="1"/>
                                                </td>
                                                <td>
                                                    pppol
                                                </td>
                                                <td>
                                                    <a href="mailto:userwow@gmail.com">good@gmail.com</a>
                                                </td>
                                                <td>
                                                    <span class="label label-sm label-success">
                                                        Approved
                                                    </span>
                                                </td>
                                            </tr>
                                            <tr class="odd gradeX">
                                                <td>
                                                    <input type="checkbox" class="checkboxes" value="1"/>
                                                </td>
                                                <td>
                                                    test
                                                </td>
                                                <td>
                                                    <a href="mailto:userwow@gmail.com">good@gmail.com</a>
                                                </td>
                                                <td>
                                                    <span class="label label-sm label-success">
                                                        Approved
                                                    </span>
                                                </td>
                                            </tr>
                                            <tr class="odd gradeX">
                                                <td>
                                                    <input type="checkbox" class="checkboxes" value="1"/>
                                                </td>
                                                <td>
                                                    userwow
                                                </td>
                                                <td>
                                                    <a href="mailto:userwow@gmail.com">userwow@gmail.com</a>
                                                </td>
                                                <td>
                                                    <span class="label label-sm label-default">
                                                        Blocked
                                                    </span>
                                                </td>
                                            </tr>
                                            <tr class="odd gradeX">
                                                <td>
                                                    <input type="checkbox" class="checkboxes" value="1"/>
                                                </td>
                                                <td>
                                                    test
                                                </td>
                                                <td>
                                                    <a href="mailto:userwow@gmail.com">test@gmail.com</a>
                                                </td>
                                                <td>
                                                    <span class="label label-sm label-success">
                                                        Approved
                                                    </span>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- END EXAMPLE TABLE PORTLET-->
                        </div>
                    </div>
                    <!--                        basic table content -->
                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <h3 class="page-title">
                                Basic Datatables <small>basic datatable samples</small>
                            </h3>
                            <ul class="page-breadcrumb breadcrumb">
                                <li class="btn-group">
                                    <button type="button" class="btn blue dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="1000" data-close-others="true">
                                        <span>
                                            Actions
                                        </span>
                                        <i class="fa fa-angle-down"></i>
                                    </button>
                                    <ul class="dropdown-menu pull-right" role="menu">
                                        <li>
                                            <a href="#">Action</a>
                                        </li>
                                        <li>
                                            <a href="#">Another action</a>
                                        </li>
                                        <li>
                                            <a href="#">Something else here</a>
                                        </li>
                                        <li class="divider">
                                        </li>
                                        <li>
                                            <a href="#">Separated link</a>
                                        </li>
                                    </ul>
                                </li>
                                <li>
                                    <i class="fa fa-home"></i>
                                    <a href="index.html">Home</a>
                                    <i class="fa fa-angle-right"></i>
                                </li>
                                <li>
                                    <a href="#">Data Tables</a>
                                    <i class="fa fa-angle-right"></i>
                                </li>
                                <li>
                                    <a href="#">Basic Datatables</a>
                                </li>
                            </ul>
                            <!-- END PAGE TITLE & BREADCRUMB-->
                        </div>
                    </div>
                    <!-- END PAGE HEADER-->
                    <!-- BEGIN PAGE CONTENT-->
                    <div class="row">
                        <div class="col-md-6">
                            <!-- BEGIN SAMPLE TABLE PORTLET-->
                            <div class="portlet box red">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-cogs"></i>Simple Table
                                    </div>
                                    <div class="tools">
                                        <a href="javascript:;" class="collapse"></a>
                                        <a href="#portlet-config" data-toggle="modal" class="config"></a>
                                        <a href="javascript:;" class="reload"></a>
                                        <a href="javascript:;" class="remove"></a>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div class="table-responsive">
                                        <table class="table table-hover">
                                            <thead>
                                                <tr>
                                                    <th>
                                                        #
                                                    </th>
                                                    <th>
                                                        First Name
                                                    </th>
                                                    <th>
                                                        Last Name
                                                    </th>
                                                    <th>
                                                        Username
                                                    </th>
                                                    <th>
                                                        Status
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        1
                                                    </td>
                                                    <td>
                                                        Mark
                                                    </td>
                                                    <td>
                                                        Otto
                                                    </td>
                                                    <td>
                                                        makr124
                                                    </td>
                                                    <td>
                                                        <span class="label label-sm label-success">
                                                            Approved
                                                        </span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        2
                                                    </td>
                                                    <td>
                                                        Jacob
                                                    </td>
                                                    <td>
                                                        Nilson
                                                    </td>
                                                    <td>
                                                        jac123
                                                    </td>
                                                    <td>
                                                        <span class="label label-sm label-info">
                                                            Pending
                                                        </span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        3
                                                    </td>
                                                    <td>
                                                        Larry
                                                    </td>
                                                    <td>
                                                        Cooper
                                                    </td>
                                                    <td>
                                                        lar
                                                    </td>
                                                    <td>
                                                        <span class="label label-sm label-warning">
                                                            Suspended
                                                        </span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        4
                                                    </td>
                                                    <td>
                                                        Sandy
                                                    </td>
                                                    <td>
                                                        Lim
                                                    </td>
                                                    <td>
                                                        sanlim
                                                    </td>
                                                    <td>
                                                        <span class="label label-sm label-danger">
                                                            Blocked
                                                        </span>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <!-- END SAMPLE TABLE PORTLET-->
                        </div>
                        <div class="col-md-6">
                            <!-- BEGIN BORDERED TABLE PORTLET-->
                            <div class="portlet box yellow">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-coffee"></i>Bordered Table
                                    </div>
                                    <div class="tools">
                                        <a href="javascript:;" class="collapse"></a>
                                        <a href="#portlet-config" data-toggle="modal" class="config"></a>
                                        <a href="javascript:;" class="reload"></a>
                                        <a href="javascript:;" class="remove"></a>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div class="table-responsive">
                                        <table class="table table-bordered table-hover">
                                            <thead>
                                                <tr>
                                                    <th>
                                                        #
                                                    </th>
                                                    <th>
                                                        First Name
                                                    </th>
                                                    <th>
                                                        Last Name
                                                    </th>
                                                    <th>
                                                        Username
                                                    </th>
                                                    <th>
                                                        Status
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td rowspan="2">
                                                        1
                                                    </td>
                                                    <td>
                                                        Mark
                                                    </td>
                                                    <td>
                                                        Otto
                                                    </td>
                                                    <td>
                                                        makr124
                                                    </td>
                                                    <td>
                                                        <span class="label label-sm label-success">
                                                            Approved
                                                        </span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        Jacob
                                                    </td>
                                                    <td>
                                                        Nilson
                                                    </td>
                                                    <td>
                                                        jac123
                                                    </td>
                                                    <td>
                                                        <span class="label label-sm label-info">
                                                            Pending
                                                        </span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        2
                                                    </td>
                                                    <td>
                                                        Larry
                                                    </td>
                                                    <td>
                                                        Cooper
                                                    </td>
                                                    <td>
                                                        lar
                                                    </td>
                                                    <td>
                                                        <span class="label label-sm label-warning">
                                                            Suspended
                                                        </span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        3
                                                    </td>
                                                    <td>
                                                        Sandy
                                                    </td>
                                                    <td>
                                                        Lim
                                                    </td>
                                                    <td>
                                                        sanlim
                                                    </td>
                                                    <td>
                                                        <span class="label label-sm label-danger">
                                                            Blocked
                                                        </span>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <!-- END BORDERED TABLE PORTLET-->
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <!-- BEGIN SAMPLE TABLE PORTLET-->
                            <div class="portlet box purple">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-comments"></i>Striped Table
                                    </div>
                                    <div class="tools">
                                        <a href="javascript:;" class="collapse"></a>
                                        <a href="#portlet-config" data-toggle="modal" class="config"></a>
                                        <a href="javascript:;" class="reload"></a>
                                        <a href="javascript:;" class="remove"></a>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div class="table-responsive">
                                        <table class="table table-striped table-hover">
                                            <thead>
                                                <tr>
                                                    <th>
                                                        #
                                                    </th>
                                                    <th>
                                                        First Name
                                                    </th>
                                                    <th>
                                                        Last Name
                                                    </th>
                                                    <th>
                                                        Username
                                                    </th>
                                                    <th>
                                                        Status
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        1
                                                    </td>
                                                    <td>
                                                        Mark
                                                    </td>
                                                    <td>
                                                        Otto
                                                    </td>
                                                    <td>
                                                        makr124
                                                    </td>
                                                    <td>
                                                        <span class="label label-sm label-success">
                                                            Approved
                                                        </span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        2
                                                    </td>
                                                    <td>
                                                        Jacob
                                                    </td>
                                                    <td>
                                                        Nilson
                                                    </td>
                                                    <td>
                                                        jac123
                                                    </td>
                                                    <td>
                                                        <span class="label label-sm label-info">
                                                            Pending
                                                        </span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        3
                                                    </td>
                                                    <td>
                                                        Larry
                                                    </td>
                                                    <td>
                                                        Cooper
                                                    </td>
                                                    <td>
                                                        lar
                                                    </td>
                                                    <td>
                                                        <span class="label label-sm label-warning">
                                                            Suspended
                                                        </span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        4
                                                    </td>
                                                    <td>
                                                        Sandy
                                                    </td>
                                                    <td>
                                                        Lim
                                                    </td>
                                                    <td>
                                                        sanlim
                                                    </td>
                                                    <td>
                                                        <span class="label label-sm label-danger">
                                                            Blocked
                                                        </span>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <!-- END SAMPLE TABLE PORTLET-->
                        </div>
                        <div class="col-md-6">
                            <!-- BEGIN CONDENSED TABLE PORTLET-->
                            <div class="portlet box green">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-picture"></i>Condensed Table
                                    </div>
                                    <div class="tools">
                                        <a href="javascript:;" class="collapse"></a>
                                        <a href="#portlet-config" data-toggle="modal" class="config"></a>
                                        <a href="javascript:;" class="reload"></a>
                                        <a href="javascript:;" class="remove"></a>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div class="table-responsive">
                                        <table class="table table-condensed table-hover">
                                            <thead>
                                                <tr>
                                                    <th>
                                                        #
                                                    </th>
                                                    <th>
                                                        First Name
                                                    </th>
                                                    <th>
                                                        Last Name
                                                    </th>
                                                    <th>
                                                        Username
                                                    </th>
                                                    <th>
                                                        Status
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        1
                                                    </td>
                                                    <td>
                                                        Mark
                                                    </td>
                                                    <td>
                                                        Otto
                                                    </td>
                                                    <td>
                                                        makr124
                                                    </td>
                                                    <td>
                                                        <span class="label label-sm label-success">
                                                            Approved
                                                        </span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        2
                                                    </td>
                                                    <td>
                                                        Jacob
                                                    </td>
                                                    <td>
                                                        Nilson
                                                    </td>
                                                    <td>
                                                        jac123
                                                    </td>
                                                    <td>
                                                        <span class="label label-sm label-info">
                                                            Pending
                                                        </span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        3
                                                    </td>
                                                    <td>
                                                        Larry
                                                    </td>
                                                    <td>
                                                        Cooper
                                                    </td>
                                                    <td>
                                                        lar
                                                    </td>
                                                    <td>
                                                        <span class="label label-sm label-warning">
                                                            Suspended
                                                        </span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        4
                                                    </td>
                                                    <td>
                                                        Sandy
                                                    </td>
                                                    <td>
                                                        Lim
                                                    </td>
                                                    <td>
                                                        sanlim
                                                    </td>
                                                    <td>
                                                        <span class="label label-sm label-danger">
                                                            Blocked
                                                        </span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        5
                                                    </td>
                                                    <td>
                                                        Sandy
                                                    </td>
                                                    <td>
                                                        Lim
                                                    </td>
                                                    <td>
                                                        sanlim
                                                    </td>
                                                    <td>
                                                        <span class="label label-sm label-danger">
                                                            Blocked
                                                        </span>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <!-- END CONDENSED TABLE PORTLET-->
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <!-- BEGIN SAMPLE TABLE PORTLET-->
                            <div class="portlet box blue">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-comments"></i>Contextual Rows
                                    </div>
                                    <div class="tools">
                                        <a href="javascript:;" class="collapse"></a>
                                        <a href="#portlet-config" data-toggle="modal" class="config"></a>
                                        <a href="javascript:;" class="reload"></a>
                                        <a href="javascript:;" class="remove"></a>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div class="table-responsive">
                                        <table class="table table-bordered table-hover">
                                            <thead>
                                                <tr>
                                                    <th>
                                                        #
                                                    </th>
                                                    <th>
                                                        Class Name
                                                    </th>
                                                    <th>
                                                        Column
                                                    </th>
                                                    <th>
                                                        Column
                                                    </th>
                                                    <th>
                                                        Column
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr class="active">
                                                    <td>
                                                        1
                                                    </td>
                                                    <td>
                                                        active
                                                    </td>
                                                    <td>
                                                        Column heading
                                                    </td>
                                                    <td>
                                                        Column heading
                                                    </td>
                                                    <td>
                                                        Column heading
                                                    </td>
                                                </tr>
                                                <tr class="success">
                                                    <td>
                                                        2
                                                    </td>
                                                    <td>
                                                        success
                                                    </td>
                                                    <td>
                                                        Column heading
                                                    </td>
                                                    <td>
                                                        Column heading
                                                    </td>
                                                    <td>
                                                        Column heading
                                                    </td>
                                                </tr>
                                                <tr class="warning">
                                                    <td>
                                                        3
                                                    </td>
                                                    <td>
                                                        warning
                                                    </td>
                                                    <td>
                                                        Column heading
                                                    </td>
                                                    <td>
                                                        Column heading
                                                    </td>
                                                    <td>
                                                        Column heading
                                                    </td>
                                                </tr>
                                                <tr class="danger">
                                                    <td>
                                                        4
                                                    </td>
                                                    <td>
                                                        danger
                                                    </td>
                                                    <td>
                                                        Column heading
                                                    </td>
                                                    <td>
                                                        Column heading
                                                    </td>
                                                    <td>
                                                        Column heading
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <!-- END SAMPLE TABLE PORTLET-->
                        </div>
                        <div class="col-md-6">
                            <!-- BEGIN SAMPLE TABLE PORTLET-->
                            <div class="portlet box red">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-comments"></i>Contextual Columns
                                    </div>
                                    <div class="tools">
                                        <a href="javascript:;" class="collapse"></a>
                                        <a href="#portlet-config" data-toggle="modal" class="config"></a>
                                        <a href="javascript:;" class="reload"></a>
                                        <a href="javascript:;" class="remove"></a>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div class="table-responsive">
                                        <table class="table table-bordered table-hover">
                                            <thead>
                                                <tr>
                                                    <th>
                                                        #
                                                    </th>
                                                    <th>
                                                        Column
                                                    </th>
                                                    <th>
                                                        Column
                                                    </th>
                                                    <th>
                                                        Column
                                                    </th>
                                                    <th>
                                                        Column
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        1
                                                    </td>
                                                    <td class="active">
                                                        active
                                                    </td>
                                                    <td class="success">
                                                        success
                                                    </td>
                                                    <td class="warning">
                                                        warning
                                                    </td>
                                                    <td class="danger">
                                                        danger
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        2
                                                    </td>
                                                    <td class="active">
                                                        active
                                                    </td>
                                                    <td class="success">
                                                        success
                                                    </td>
                                                    <td class="warning">
                                                        warning
                                                    </td>
                                                    <td class="danger">
                                                        danger
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        3
                                                    </td>
                                                    <td class="active">
                                                        active
                                                    </td>
                                                    <td class="success">
                                                        success
                                                    </td>
                                                    <td class="warning">
                                                        warning
                                                    </td>
                                                    <td class="danger">
                                                        danger
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        4
                                                    </td>
                                                    <td class="active">
                                                        active
                                                    </td>
                                                    <td class="success">
                                                        success
                                                    </td>
                                                    <td class="warning">
                                                        warning
                                                    </td>
                                                    <td class="danger">
                                                        danger
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <!-- END SAMPLE TABLE PORTLET-->
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <!-- BEGIN SAMPLE TABLE PORTLET-->
                            <div class="portlet">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-bell-o"></i>Advance Table
                                    </div>
                                    <div class="tools">
                                        <a href="javascript:;" class="collapse"></a>
                                        <a href="#portlet-config" data-toggle="modal" class="config"></a>
                                        <a href="javascript:;" class="reload"></a>
                                        <a href="javascript:;" class="remove"></a>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div class="table-responsive">
                                        <table class="table table-striped table-bordered table-advance table-hover">
                                            <thead>
                                                <tr>
                                                    <th>
                                                        <i class="fa fa-briefcase"></i> Company
                                                    </th>
                                                    <th class="hidden-xs">
                                                        <i class="fa fa-user"></i> Contact
                                                    </th>
                                                    <th>
                                                        <i class="fa fa-shopping-cart"></i> Total
                                                    </th>
                                                    <th>
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td class="highlight">
                                                        <div class="success">
                                                        </div>
                                                        <a href="#">RedBull</a>
                                                    </td>
                                                    <td class="hidden-xs">
                                                        Mike Nilson
                                                    </td>
                                                    <td>
                                                        2560.60$
                                                    </td>
                                                    <td>
                                                        <a href="#" class="btn default btn-xs purple"><i class="fa fa-edit"></i> Edit</a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="highlight">
                                                        <div class="info">
                                                        </div>
                                                        <a href="#">Google</a>
                                                    </td>
                                                    <td class="hidden-xs">
                                                        Adam Larson
                                                    </td>
                                                    <td>
                                                        560.60$
                                                    </td>
                                                    <td>
                                                        <a href="#" class="btn default btn-xs black"><i class="fa fa-trash-o"></i> Delete</a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="highlight">
                                                        <div class="important">
                                                        </div>
                                                        <a href="#">Apple</a>
                                                    </td>
                                                    <td class="hidden-xs">
                                                        Daniel Kim
                                                    </td>
                                                    <td>
                                                        3460.60$
                                                    </td>
                                                    <td>
                                                        <a href="#" class="btn default btn-xs purple"><i class="fa fa-edit"></i> Edit</a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="highlight">
                                                        <div class="warning">
                                                        </div>
                                                        <a href="#">Microsoft</a>
                                                    </td>
                                                    <td class="hidden-xs">
                                                        Nick
                                                    </td>
                                                    <td>
                                                        2560.60$
                                                    </td>
                                                    <td>
                                                        <a href="#" class="btn default btn-xs blue"><i class="fa fa-share"></i> Share</a>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <!-- END SAMPLE TABLE PORTLET-->
                        </div>
                        <div class="col-md-6">
                            <!-- BEGIN SAMPLE TABLE PORTLET-->
                            <div class="portlet">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-shopping-cart"></i>Advance Table
                                    </div>
                                    <div class="tools">
                                        <a href="javascript:;" class="collapse"></a>
                                        <a href="#portlet-config" data-toggle="modal" class="config"></a>
                                        <a href="javascript:;" class="reload"></a>
                                        <a href="javascript:;" class="remove"></a>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div class="table-responsive">
                                        <table class="table table-striped table-bordered table-advance table-hover">
                                            <thead>
                                                <tr>
                                                    <th>
                                                        <i class="fa fa-briefcase"></i> From
                                                    </th>
                                                    <th class="hidden-xs">
                                                        <i class="fa fa-question"></i> Descrition
                                                    </th>
                                                    <th>
                                                        <i class="fa fa-bookmark"></i> Total
                                                    </th>
                                                    <th>
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        <a href="#">Pixel Ltd</a>
                                                    </td>
                                                    <td class="hidden-xs">
                                                        Server hardware purchase
                                                    </td>
                                                    <td>
                                                        52560.10$
                                                        <span class="label label-sm label-success label-mini">
                                                            Paid
                                                        </span>
                                                    </td>
                                                    <td>
                                                        <a href="#" class="btn default btn-xs green-stripe">View</a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <a href="#">
                                                            Smart House </a>
                                                    </td>
                                                    <td class="hidden-xs">
                                                        Office furniture purchase
                                                    </td>
                                                    <td>
                                                        5760.00$
                                                        <span class="label label-sm label-warning label-mini">
                                                            Pending
                                                        </span>
                                                    </td>
                                                    <td>
                                                        <a href="#" class="btn default btn-xs blue-stripe">View</a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <a href="#">
                                                            FoodMaster Ltd </a>
                                                    </td>
                                                    <td class="hidden-xs">
                                                        Company Anual Dinner Catering
                                                    </td>
                                                    <td>
                                                        12400.00$
                                                        <span class="label label-sm label-success label-mini">
                                                            Paid
                                                        </span>
                                                    </td>
                                                    <td>
                                                        <a href="#" class="btn default btn-xs blue-stripe">View</a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <a href="#">
                                                            WaterPure Ltd </a>
                                                    </td>
                                                    <td class="hidden-xs">
                                                        Payment for Jan 2013
                                                    </td>
                                                    <td>
                                                        610.50$
                                                        <span class="label label-sm label-danger label-mini">
                                                            Overdue
                                                        </span>
                                                    </td>
                                                    <td>
                                                        <a href="#" class="btn default btn-xs red-stripe">View</a>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <!-- END SAMPLE TABLE PORTLET-->
                        </div>
                    </div>

                    <!--                       // basic table content -->
                    <!--                       responsive table content -->
                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <h3 class="page-title">
                                Responsive Datatables <small>responsive datatable samples</small>
                            </h3>
                            <ul class="page-breadcrumb breadcrumb">
                                <li class="btn-group">
                                    <button type="button" class="btn blue dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="1000" data-close-others="true">
                                        <span>
                                            Actions
                                        </span>
                                        <i class="fa fa-angle-down"></i>
                                    </button>
                                    <ul class="dropdown-menu pull-right" role="menu">
                                        <li>
                                            <a href="#">Action</a>
                                        </li>
                                        <li>
                                            <a href="#">Another action</a>
                                        </li>
                                        <li>
                                            <a href="#">Something else here</a>
                                        </li>
                                        <li class="divider">
                                        </li>
                                        <li>
                                            <a href="#">Separated link</a>
                                        </li>
                                    </ul>
                                </li>
                                <li>
                                    <i class="fa fa-home"></i>
                                    <a href="index.html">Home</a>
                                    <i class="fa fa-angle-right"></i>
                                </li>
                                <li>
                                    <a href="#">Data Tables</a>
                                    <i class="fa fa-angle-right"></i>
                                </li>
                                <li>
                                    <a href="#">Responsive Datatables</a>
                                </li>
                            </ul>
                            <!-- END PAGE TITLE & BREADCRUMB-->
                        </div>
                    </div>
                    <!-- END PAGE HEADER-->
                    <!-- BEGIN PAGE CONTENT-->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="note note-success">
                                <p>
                                    Please try to re-size your browser window in order to see the tables in responsive mode.
                                </p>
                            </div>
                            <!-- BEGIN SAMPLE TABLE PORTLET-->
                            <div class="portlet box green">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-cogs"></i>Responsive Flip Scroll Tables
                                    </div>
                                    <div class="tools">
                                        <a href="javascript:;" class="collapse"></a>
                                        <a href="#portlet-config" data-toggle="modal" class="config"></a>
                                        <a href="javascript:;" class="reload"></a>
                                        <a href="javascript:;" class="remove"></a>
                                    </div>
                                </div>
                                <div class="portlet-body flip-scroll">
                                    <table class="table table-bordered table-striped table-condensed flip-content">
                                        <thead class="flip-content">
                                            <tr>
                                                <th>
                                                    Code
                                                </th>
                                                <th>
                                                    Company
                                                </th>
                                                <th class="numeric">
                                                    Price
                                                </th>
                                                <th class="numeric">
                                                    Change
                                                </th>
                                                <th class="numeric">
                                                    Change %
                                                </th>
                                                <th class="numeric">
                                                    Open
                                                </th>
                                                <th class="numeric">
                                                    High
                                                </th>
                                                <th class="numeric">
                                                    Low
                                                </th>
                                                <th class="numeric">
                                                    Volume
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>
                                                    AAC
                                                </td>
                                                <td>
                                                    AUSTRALIAN AGRICULTURAL COMPANY LIMITED.
                                                </td>
                                                <td class="numeric">
                                                    $1.38
                                                </td>
                                                <td class="numeric">
                                                    -0.01
                                                </td>
                                                <td class="numeric">
                                                    -0.36%
                                                </td>
                                                <td class="numeric">
                                                    $1.39
                                                </td>
                                                <td class="numeric">
                                                    $1.39
                                                </td>
                                                <td class="numeric">
                                                    $1.38
                                                </td>
                                                <td class="numeric">
                                                    9,395
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    AAD
                                                </td>
                                                <td>
                                                    ARDENT LEISURE GROUP
                                                </td>
                                                <td class="numeric">
                                                    $1.15
                                                </td>
                                                <td class="numeric">
                                                    +0.02
                                                </td>
                                                <td class="numeric">
                                                    1.32%
                                                </td>
                                                <td class="numeric">
                                                    $1.14
                                                </td>
                                                <td class="numeric">
                                                    $1.15
                                                </td>
                                                <td class="numeric">
                                                    $1.13
                                                </td>
                                                <td class="numeric">
                                                    56,431
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    AAX
                                                </td>
                                                <td>
                                                    AUSENCO LIMITED
                                                </td>
                                                <td class="numeric">
                                                    $4.00
                                                </td>
                                                <td class="numeric">
                                                    -0.04
                                                </td>
                                                <td class="numeric">
                                                    -0.99%
                                                </td>
                                                <td class="numeric">
                                                    $4.01
                                                </td>
                                                <td class="numeric">
                                                    $4.05
                                                </td>
                                                <td class="numeric">
                                                    $4.00
                                                </td>
                                                <td class="numeric">
                                                    90,641
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    ABC
                                                </td>
                                                <td>
                                                    ADELAIDE BRIGHTON LIMITED
                                                </td>
                                                <td class="numeric">
                                                    $3.00
                                                </td>
                                                <td class="numeric">
                                                    +0.06
                                                </td>
                                                <td class="numeric">
                                                    2.04%
                                                </td>
                                                <td class="numeric">
                                                    $2.98
                                                </td>
                                                <td class="numeric">
                                                    $3.00
                                                </td>
                                                <td class="numeric">
                                                    $2.96
                                                </td>
                                                <td class="numeric">
                                                    862,518
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    ABP
                                                </td>
                                                <td>
                                                    ABACUS PROPERTY GROUP
                                                </td>
                                                <td class="numeric">
                                                    $1.91
                                                </td>
                                                <td class="numeric">
                                                    0.00
                                                </td>
                                                <td class="numeric">
                                                    0.00%
                                                </td>
                                                <td class="numeric">
                                                    $1.92
                                                </td>
                                                <td class="numeric">
                                                    $1.93
                                                </td>
                                                <td class="numeric">
                                                    $1.90
                                                </td>
                                                <td class="numeric">
                                                    595,701
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    ABY
                                                </td>
                                                <td>
                                                    ADITYA BIRLA MINERALS LIMITED
                                                </td>
                                                <td class="numeric">
                                                    $0.77
                                                </td>
                                                <td class="numeric">
                                                    +0.02
                                                </td>
                                                <td class="numeric">
                                                    2.00%
                                                </td>
                                                <td class="numeric">
                                                    $0.76
                                                </td>
                                                <td class="numeric">
                                                    $0.77
                                                </td>
                                                <td class="numeric">
                                                    $0.76
                                                </td>
                                                <td class="numeric">
                                                    54,567
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    ACR
                                                </td>
                                                <td>
                                                    ACRUX LIMITED
                                                </td>
                                                <td class="numeric">
                                                    $3.71
                                                </td>
                                                <td class="numeric">
                                                    +0.01
                                                </td>
                                                <td class="numeric">
                                                    0.14%
                                                </td>
                                                <td class="numeric">
                                                    $3.70
                                                </td>
                                                <td class="numeric">
                                                    $3.72
                                                </td>
                                                <td class="numeric">
                                                    $3.68
                                                </td>
                                                <td class="numeric">
                                                    191,373
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    ADU
                                                </td>
                                                <td>
                                                    ADAMUS RESOURCES LIMITED
                                                </td>
                                                <td class="numeric">
                                                    $0.72
                                                </td>
                                                <td class="numeric">
                                                    0.00
                                                </td>
                                                <td class="numeric">
                                                    0.00%
                                                </td>
                                                <td class="numeric">
                                                    $0.73
                                                </td>
                                                <td class="numeric">
                                                    $0.74
                                                </td>
                                                <td class="numeric">
                                                    $0.72
                                                </td>
                                                <td class="numeric">
                                                    8,602,291
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    AGG
                                                </td>
                                                <td>
                                                    ANGLOGOLD ASHANTI LIMITED
                                                </td>
                                                <td class="numeric">
                                                    $7.81
                                                </td>
                                                <td class="numeric">
                                                    -0.22
                                                </td>
                                                <td class="numeric">
                                                    -2.74%
                                                </td>
                                                <td class="numeric">
                                                    $7.82
                                                </td>
                                                <td class="numeric">
                                                    $7.82
                                                </td>
                                                <td class="numeric">
                                                    $7.81
                                                </td>
                                                <td class="numeric">
                                                    148
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    AGK
                                                </td>
                                                <td>
                                                    AGL ENERGY LIMITED
                                                </td>
                                                <td class="numeric">
                                                    $13.82
                                                </td>
                                                <td class="numeric">
                                                    +0.02
                                                </td>
                                                <td class="numeric">
                                                    0.14%
                                                </td>
                                                <td class="numeric">
                                                    $13.83
                                                </td>
                                                <td class="numeric">
                                                    $13.83
                                                </td>
                                                <td class="numeric">
                                                    $13.67
                                                </td>
                                                <td class="numeric">
                                                    846,403
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    AGO
                                                </td>
                                                <td>
                                                    ATLAS IRON LIMITED
                                                </td>
                                                <td class="numeric">
                                                    $3.17
                                                </td>
                                                <td class="numeric">
                                                    -0.02
                                                </td>
                                                <td class="numeric">
                                                    -0.47%
                                                </td>
                                                <td class="numeric">
                                                    $3.11
                                                </td>
                                                <td class="numeric">
                                                    $3.22
                                                </td>
                                                <td class="numeric">
                                                    $3.10
                                                </td>
                                                <td class="numeric">
                                                    5,416,303
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- END SAMPLE TABLE PORTLET-->
                            <!-- BEGIN SAMPLE TABLE PORTLET-->
                            <div class="portlet box red">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-cogs"></i>Basic Bootstrap 3.0 Responsive Table
                                    </div>
                                    <div class="tools">
                                        <a href="javascript:;" class="collapse"></a>
                                        <a href="#portlet-config" data-toggle="modal" class="config"></a>
                                        <a href="javascript:;" class="reload"></a>
                                        <a href="javascript:;" class="remove"></a>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div class="table-responsive">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th>
                                                        #
                                                    </th>
                                                    <th>
                                                        Table heading
                                                    </th>
                                                    <th>
                                                        Table heading
                                                    </th>
                                                    <th>
                                                        Table heading
                                                    </th>
                                                    <th>
                                                        Table heading
                                                    </th>
                                                    <th>
                                                        Table heading
                                                    </th>
                                                    <th>
                                                        Table heading
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        1
                                                    </td>
                                                    <td>
                                                        Table cell
                                                    </td>
                                                    <td>
                                                        Table cell
                                                    </td>
                                                    <td>
                                                        Table cell
                                                    </td>
                                                    <td>
                                                        Table cell
                                                    </td>
                                                    <td>
                                                        Table cell
                                                    </td>
                                                    <td>
                                                        Table cell
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        2
                                                    </td>
                                                    <td>
                                                        Table cell
                                                    </td>
                                                    <td>
                                                        Table cell
                                                    </td>
                                                    <td>
                                                        Table cell
                                                    </td>
                                                    <td>
                                                        Table cell
                                                    </td>
                                                    <td>
                                                        Table cell
                                                    </td>
                                                    <td>
                                                        Table cell
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        3
                                                    </td>
                                                    <td>
                                                        Table cell
                                                    </td>
                                                    <td>
                                                        Table cell
                                                    </td>
                                                    <td>
                                                        Table cell
                                                    </td>
                                                    <td>
                                                        Table cell
                                                    </td>
                                                    <td>
                                                        Table cell
                                                    </td>
                                                    <td>
                                                        Table cell
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <!-- END SAMPLE TABLE PORTLET-->
                            <!-- BEGIN SAMPLE TABLE PORTLET-->
                            <div class="portlet box blue">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-cogs"></i>Bordered Bootstrap 3.0 Responsive Table
                                    </div>
                                    <div class="tools">
                                        <a href="javascript:;" class="collapse"></a>
                                        <a href="#portlet-config" data-toggle="modal" class="config"></a>
                                        <a href="javascript:;" class="reload"></a>
                                        <a href="javascript:;" class="remove"></a>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div class="table-responsive">
                                        <table class="table table-bordered">
                                            <thead>
                                                <tr>
                                                    <th>
                                                        #
                                                    </th>
                                                    <th>
                                                        Table heading
                                                    </th>
                                                    <th>
                                                        Table heading
                                                    </th>
                                                    <th>
                                                        Table heading
                                                    </th>
                                                    <th>
                                                        Table heading
                                                    </th>
                                                    <th>
                                                        Table heading
                                                    </th>
                                                    <th>
                                                        Table heading
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        1
                                                    </td>
                                                    <td>
                                                        Table cell
                                                    </td>
                                                    <td>
                                                        Table cell
                                                    </td>
                                                    <td>
                                                        Table cell
                                                    </td>
                                                    <td>
                                                        Table cell
                                                    </td>
                                                    <td>
                                                        Table cell
                                                    </td>
                                                    <td>
                                                        Table cell
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        2
                                                    </td>
                                                    <td>
                                                        Table cell
                                                    </td>
                                                    <td>
                                                        Table cell
                                                    </td>
                                                    <td>
                                                        Table cell
                                                    </td>
                                                    <td>
                                                        Table cell
                                                    </td>
                                                    <td>
                                                        Table cell
                                                    </td>
                                                    <td>
                                                        Table cell
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        3
                                                    </td>
                                                    <td>
                                                        Table cell
                                                    </td>
                                                    <td>
                                                        Table cell
                                                    </td>
                                                    <td>
                                                        Table cell
                                                    </td>
                                                    <td>
                                                        Table cell
                                                    </td>
                                                    <td>
                                                        Table cell
                                                    </td>
                                                    <td>
                                                        Table cell
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <!-- END SAMPLE TABLE PORTLET-->
                            <!-- BEGIN SAMPLE TABLE PORTLET-->
                            <div class="portlet box yellow">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-cogs"></i>All in One Bootstrap 3.0 Responsive Table
                                    </div>
                                    <div class="tools">
                                        <a href="javascript:;" class="collapse"></a>
                                        <a href="#portlet-config" data-toggle="modal" class="config"></a>
                                        <a href="javascript:;" class="reload"></a>
                                        <a href="javascript:;" class="remove"></a>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div class="table-responsive">
                                        <table class="table table-striped table-bordered table-hover">
                                            <thead>
                                                <tr>
                                                    <th>
                                                        #
                                                    </th>
                                                    <th>
                                                        Table heading
                                                    </th>
                                                    <th>
                                                        Table heading
                                                    </th>
                                                    <th>
                                                        Table heading
                                                    </th>
                                                    <th>
                                                        Table heading
                                                    </th>
                                                    <th>
                                                        Table heading
                                                    </th>
                                                    <th>
                                                        Table heading
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        1
                                                    </td>
                                                    <td>
                                                        Table cell
                                                    </td>
                                                    <td>
                                                        Table cell
                                                    </td>
                                                    <td>
                                                        Table cell
                                                    </td>
                                                    <td>
                                                        Table cell
                                                    </td>
                                                    <td>
                                                        Table cell
                                                    </td>
                                                    <td>
                                                        Table cell
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        2
                                                    </td>
                                                    <td>
                                                        Table cell
                                                    </td>
                                                    <td>
                                                        Table cell
                                                    </td>
                                                    <td>
                                                        Table cell
                                                    </td>
                                                    <td>
                                                        Table cell
                                                    </td>
                                                    <td>
                                                        Table cell
                                                    </td>
                                                    <td>
                                                        Table cell
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        3
                                                    </td>
                                                    <td>
                                                        Table cell
                                                    </td>
                                                    <td>
                                                        Table cell
                                                    </td>
                                                    <td>
                                                        Table cell
                                                    </td>
                                                    <td>
                                                        Table cell
                                                    </td>
                                                    <td>
                                                        Table cell
                                                    </td>
                                                    <td>
                                                        Table cell
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <!-- END SAMPLE TABLE PORTLET-->
                            <!-- BEGIN SAMPLE TABLE PORTLET-->
                            <div class="portlet box purple">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-cogs"></i>Horizontal Scrollable Responsive Table
                                    </div>
                                    <div class="tools">
                                        <a href="javascript:;" class="collapse"></a>
                                        <a href="#portlet-config" data-toggle="modal" class="config"></a>
                                        <a href="javascript:;" class="reload"></a>
                                        <a href="javascript:;" class="remove"></a>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div class="table-scrollable">
                                        <table class="table table-striped table-bordered table-hover">
                                            <thead>
                                                <tr>
                                                    <th scope="col" style="width:450px !important">
                                                        Column header 1
                                                    </th>
                                                    <th scope="col">
                                                        Column header 2
                                                    </th>
                                                    <th scope="col">
                                                        Column header 3
                                                    </th>
                                                    <th scope="col">
                                                        Column header 4
                                                    </th>
                                                    <th scope="col">
                                                        Column header 5
                                                    </th>
                                                    <th scope="col">
                                                        Column header 6
                                                    </th>
                                                    <th scope="col">
                                                        Column header 7
                                                    </th>
                                                    <th scope="col">
                                                        Column header 8
                                                    </th>
                                                    <th scope="col">
                                                        Column header 9
                                                    </th>
                                                    <th scope="col">
                                                        Column header 10
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        Table data
                                                    </td>
                                                    <td>
                                                        Table data
                                                    </td>
                                                    <td>
                                                        Table data
                                                    </td>
                                                    <td>
                                                        Table data
                                                    </td>
                                                    <td>
                                                        Table data
                                                    </td>
                                                    <td>
                                                        Table data
                                                    </td>
                                                    <td>
                                                        Table data
                                                    </td>
                                                    <td>
                                                        Table data
                                                    </td>
                                                    <td>
                                                        Table data
                                                    </td>
                                                    <td>
                                                        Table data
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        Table data
                                                    </td>
                                                    <td>
                                                        Table data
                                                    </td>
                                                    <td>
                                                        Table data
                                                    </td>
                                                    <td>
                                                        Table data
                                                    </td>
                                                    <td>
                                                        Table data
                                                    </td>
                                                    <td>
                                                        Table data
                                                    </td>
                                                    <td>
                                                        Table data
                                                    </td>
                                                    <td>
                                                        Table data
                                                    </td>
                                                    <td>
                                                        Table data
                                                    </td>
                                                    <td>
                                                        Table data
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        Table data
                                                    </td>
                                                    <td>
                                                        Table data
                                                    </td>
                                                    <td>
                                                        Table data
                                                    </td>
                                                    <td>
                                                        Table data
                                                    </td>
                                                    <td>
                                                        Table data
                                                    </td>
                                                    <td>
                                                        Table data
                                                    </td>
                                                    <td>
                                                        Table data
                                                    </td>
                                                    <td>
                                                        Table data
                                                    </td>
                                                    <td>
                                                        Table data
                                                    </td>
                                                    <td>
                                                        Table data
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        Table data
                                                    </td>
                                                    <td>
                                                        Table data
                                                    </td>
                                                    <td>
                                                        Table data
                                                    </td>
                                                    <td>
                                                        Table data
                                                    </td>
                                                    <td>
                                                        Table data
                                                    </td>
                                                    <td>
                                                        Table data
                                                    </td>
                                                    <td>
                                                        Table data
                                                    </td>
                                                    <td>
                                                        Table data
                                                    </td>
                                                    <td>
                                                        Table data
                                                    </td>
                                                    <td>
                                                        Table data
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        Table data
                                                    </td>
                                                    <td>
                                                        Table data
                                                    </td>
                                                    <td>
                                                        Table data
                                                    </td>
                                                    <td>
                                                        Table data
                                                    </td>
                                                    <td>
                                                        Table data
                                                    </td>
                                                    <td>
                                                        Table data
                                                    </td>
                                                    <td>
                                                        Table data
                                                    </td>
                                                    <td>
                                                        Table data
                                                    </td>
                                                    <td>
                                                        Table data
                                                    </td>
                                                    <td>
                                                        Table data
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        Table data
                                                    </td>
                                                    <td>
                                                        Table data
                                                    </td>
                                                    <td>
                                                        Table data
                                                    </td>
                                                    <td>
                                                        Table data
                                                    </td>
                                                    <td>
                                                        Table data
                                                    </td>
                                                    <td>
                                                        Table data
                                                    </td>
                                                    <td>
                                                        Table data
                                                    </td>
                                                    <td>
                                                        Table data
                                                    </td>
                                                    <td>
                                                        Table data
                                                    </td>
                                                    <td>
                                                        Table data
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <!-- END SAMPLE TABLE PORTLET-->
                        </div>
                    </div>

                    <!--                    //   responsive table content -->
                    <!--                    editable table content -->
                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <h3 class="page-title">
                                Editable Datatables <small>editable datatable samples</small>
                            </h3>
                            <ul class="page-breadcrumb breadcrumb">
                                <li class="btn-group">
                                    <button type="button" class="btn blue dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="1000" data-close-others="true">
                                        <span>
                                            Actions
                                        </span>
                                        <i class="fa fa-angle-down"></i>
                                    </button>
                                    <ul class="dropdown-menu pull-right" role="menu">
                                        <li>
                                            <a href="#">Action</a>
                                        </li>
                                        <li>
                                            <a href="#">Another action</a>
                                        </li>
                                        <li>
                                            <a href="#">Something else here</a>
                                        </li>
                                        <li class="divider">
                                        </li>
                                        <li>
                                            <a href="#">Separated link</a>
                                        </li>
                                    </ul>
                                </li>
                                <li>
                                    <i class="fa fa-home"></i>
                                    <a href="index.html">Home</a>
                                    <i class="fa fa-angle-right"></i>
                                </li>
                                <li>
                                    <a href="#">Data Tables</a>
                                    <i class="fa fa-angle-right"></i>
                                </li>
                                <li>
                                    <a href="#">Editable Datatables</a>
                                </li>
                            </ul>
                            <!-- END PAGE TITLE & BREADCRUMB-->
                        </div>
                    </div>
                    <!-- END PAGE HEADER-->
                    <!-- BEGIN PAGE CONTENT-->
                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN EXAMPLE TABLE PORTLET-->
                            <div class="portlet box blue">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-edit"></i>Editable Table
                                    </div>
                                    <div class="tools">
                                        <a href="javascript:;" class="collapse"></a>
                                        <a href="#portlet-config" data-toggle="modal" class="config"></a>
                                        <a href="javascript:;" class="reload"></a>
                                        <a href="javascript:;" class="remove"></a>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div class="table-toolbar">
                                        <div class="btn-group">
                                            <button id="sample_editable_1_new" class="btn green">
                                                Add New <i class="fa fa-plus"></i>
                                            </button>
                                        </div>
                                        <div class="btn-group pull-right">
                                            <button class="btn dropdown-toggle" data-toggle="dropdown">Tools <i class="fa fa-angle-down"></i>
                                            </button>
                                            <ul class="dropdown-menu pull-right">
                                                <li>
                                                    <a href="#">Print</a>
                                                </li>
                                                <li>
                                                    <a href="#">Save as PDF</a>
                                                </li>
                                                <li>
                                                    <a href="#">Export to Excel</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <table class="table table-striped table-hover table-bordered" id="sample_editable_1">
                                        <thead>
                                            <tr>
                                                <th>
                                                    Username
                                                </th>
                                                <th>
                                                    Full Name
                                                </th>
                                                <th>
                                                    Points
                                                </th>
                                                <th>
                                                    Notes
                                                </th>
                                                <th>
                                                    Edit
                                                </th>
                                                <th>
                                                    Delete
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>
                                                    alex
                                                </td>
                                                <td>
                                                    Alex Nilson
                                                </td>
                                                <td>
                                                    1234
                                                </td>
                                                <td class="center">
                                                    power user
                                                </td>
                                                <td>
                                                    <a class="edit" href="javascript:;">Edit</a>
                                                </td>
                                                <td>
                                                    <a class="delete" href="javascript:;">Delete</a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    lisa
                                                </td>
                                                <td>
                                                    Lisa Wong
                                                </td>
                                                <td>
                                                    434
                                                </td>
                                                <td class="center">
                                                    new user
                                                </td>
                                                <td>
                                                    <a class="edit" href="javascript:;">Edit</a>
                                                </td>
                                                <td>
                                                    <a class="delete" href="javascript:;">Delete</a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    nick12
                                                </td>
                                                <td>
                                                    Nick Roberts
                                                </td>
                                                <td>
                                                    232
                                                </td>
                                                <td class="center">
                                                    power user
                                                </td>
                                                <td>
                                                    <a class="edit" href="javascript:;">Edit</a>
                                                </td>
                                                <td>
                                                    <a class="delete" href="javascript:;">Delete</a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    goldweb
                                                </td>
                                                <td>
                                                    Sergio Jackson
                                                </td>
                                                <td>
                                                    132
                                                </td>
                                                <td class="center">
                                                    elite user
                                                </td>
                                                <td>
                                                    <a class="edit" href="javascript:;">Edit</a>
                                                </td>
                                                <td>
                                                    <a class="delete" href="javascript:;">Delete</a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    webriver
                                                </td>
                                                <td>
                                                    Antonio Sanches
                                                </td>
                                                <td>
                                                    462
                                                </td>
                                                <td class="center">
                                                    new user
                                                </td>
                                                <td>
                                                    <a class="edit" href="javascript:;">Edit</a>
                                                </td>
                                                <td>
                                                    <a class="delete" href="javascript:;">Delete</a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    gist124
                                                </td>
                                                <td>
                                                    Nick Roberts
                                                </td>
                                                <td>
                                                    62
                                                </td>
                                                <td class="center">
                                                    new user
                                                </td>
                                                <td>
                                                    <a class="edit" href="javascript:;">Edit</a>
                                                </td>
                                                <td>
                                                    <a class="delete" href="javascript:;">Delete</a>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- END EXAMPLE TABLE PORTLET-->
                        </div>
                    </div>


                    <!--                   // editable table content -->

                    <!-- END PAGE CONTENT-->
                </div>
            </div>
            <!-- END CONTENT -->
        </div>
        <!-- END CONTAINER -->
        <!-- BEGIN FOOTER -->

        <?php include(basePath('admin/footer.php')); ?>
        <?php include(basePath('admin/footer_script.php')); ?>


        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script type="text/javascript" src="assets/plugins/select2/select2.min.js"></script>
        <script type="text/javascript" src="assets/plugins/data-tables/jquery.dataTables.js"></script>
        <script type="text/javascript" src="assets/plugins/data-tables/DT_bootstrap.js"></script>

        <!-- END PAGE LEVEL PLUGINS -->

        <!-- BEGIN PAGE LEVEL SCRIPTS -->
        <script src="assets/scripts/app.js"></script>
        <script src="assets/scripts/table-advanced.js"></script>
        <script src="assets/scripts/table-managed.js"></script>
        <script src="assets/scripts/table-editable.js"></script>
        <script src="assets/scripts/table-ajax.js"></script>

        <script>
    jQuery(document).ready(function() {
        App.init();
        TableManaged.init();
        TableEditable.init();
    });

</script>
            </body>
<!-- END BODY -->
            </html>
