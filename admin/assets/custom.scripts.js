

function changeStatus(uid){
    
    var checkStatus = '';
    
    if($('#statusChecker' + uid).is(':checked')){
        checkStatus = 'checked';
    } else {
        checkStatus = 'unchecked';
    }
    $.ajax({ url: '../ajax/ajaxChangeUserStatus.php',
             data: {userID:uid, status:checkStatus, type:"statuschange"}, //Modify this
             type: 'post',
             success:   function(output) {
//                alert(output);
                
                var result=$.parseJSON(output);
                if(result.error == 0){
                    alert("Task Complete.");
                 } else {
                    alert("Task Failed.Reason: " + result.error_txt);
                 }
              }
   });
}



function changeDeptStatus(uid){
    
    var checkStatus = '';
    
    if($('#statusChecker' + uid).is(':checked')){
        checkStatus = 'checked';
    } else {
        checkStatus = 'unchecked';
    }
    $.ajax({ url: '../ajax/ajaxChangeDepartmentStatus.php',
             data: {deptID:uid, status:checkStatus, type:"statuschange"}, //Modify this
             type: 'post',
             success:   function(output) {
//                alert(output);
                
                var result=$.parseJSON(output);
                if(result.error == 0){
                    alert("Task Complete.");
                 } else {
                    alert("Task Failed.Reason: " + result.error_txt);
                 }
              }
   });
}



function changeRoomStatus(uid){
    
    var checkStatus = '';
    
    if($('#statusChecker' + uid).is(':checked')){
        checkStatus = 'checked';
    } else {
        checkStatus = 'unchecked';
    }
    $.ajax({ url: '../ajax/ajaxChangeRoomStatus.php',
             data: {roomID:uid, status:checkStatus, type:"statuschange"}, //Modify this
             type: 'post',
             success:   function(output) {
//                alert(output);
                
                var result=$.parseJSON(output);
                if(result.error == 0){
                    alert("Task Complete.");
                 } else {
                    alert("Task Failed.Reason: " + result.error_txt);
                 }
              }
   });
}



function changeLectureStatus(uid){
    
    var checkStatus = '';
    
    if($('#statusChecker' + uid).is(':checked')){
        checkStatus = 'checked';
    } else {
        checkStatus = 'unchecked';
    }
    $.ajax({ url: '../ajax/ajaxChangeLectureStatus.php',
             data: {lectID:uid, status:checkStatus, type:"statuschange"}, //Modify this
             type: 'post',
             success:   function(output) {
//                alert(output);
                
                var result=$.parseJSON(output);
                if(result.error == 0){
                    alert("Task Complete.");
                 } else {
                    alert("Task Failed.Reason: " + result.error_txt);
                 }
              }
   });
}

