<?php if (isset($msg) AND $msg != ''): ?>
    <div class="alert alert-success alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
        <strong>Success!</strong> <?php echo $msg; ?>
    </div>          
<?php endif; /* (isset($msg) AND $msg != '') */ ?>

<?php if (isset($err) AND $err != ''): ?>
    <div class="alert alert-danger alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
        <strong>Error!</strong> <?php echo $err; ?>
    </div>  
<?php endif; /* isset($err) AND $err !='' */ ?>