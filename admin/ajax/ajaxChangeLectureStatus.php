<?php
include ('../../config/config.php');

$lectID = 0;
$type = "";
$status = '';
$data = array();
$data['error'] = 0;
$data['error_txt'] = '';

extract($_POST);

if($lectID > 0){
    if($status == "checked"){
        $updateLecture = "UPDATE lectures SET status='active' WHERE lecture_id=$lectID";
    } else {
        $updateLecture = "UPDATE lectures SET status='inactive' WHERE lecture_id=$lectID";
    }
    $executeUpdateLecture = mysqli_query($con, $updateLecture);
    
    if($executeUpdateLecture){
        $data['error'] = 0;
    } else {
        $data['error'] = 1;
        if(DEBUG){
            $data['error_txt'] = "executeUpdateLecture error: " . mysqli_error($con);
        } else {
            $data['error_txt'] = "executeUpdateLecture query failed.";
        }
    }
}
echo json_encode($data);
?>