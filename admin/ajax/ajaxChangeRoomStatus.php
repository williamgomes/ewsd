<?php
include ('../../config/config.php');

$roomID = 0;
$type = "";
$status = '';
$data = array();
$data['error'] = 0;
$data['error_txt'] = '';

extract($_POST);

if($roomID > 0){
    if($status == "checked"){
        $updateRoom = "UPDATE rooms SET status='active' WHERE room_id=$roomID";
    } else {
        $updateRoom = "UPDATE rooms SET status='inactive' WHERE room_id=$roomID";
    }
    $executeUpdateRoom = mysqli_query($con, $updateRoom);
    
    if($executeUpdateRoom){
        $data['error'] = 0;
    } else {
        $data['error'] = 1;
        if(DEBUG){
            $data['error_txt'] = "executeUpdateRoom error: " . mysqli_error($con);
        } else {
            $data['error_txt'] = "executeUpdateRoom query failed.";
        }
    }
}
echo json_encode($data);
?>