<?php
include ('../../config/config.php');

$deptID = 0;
$type = "";
$status = '';
$data = array();
$data['error'] = 0;
$data['error_txt'] = '';

extract($_POST);

if($deptID > 0){
    if($status == "checked"){
        $updateDepartment = "UPDATE department SET status='active' WHERE department_id=$deptID";
    } else {
        $updateDepartment = "UPDATE department SET status='inactive' WHERE department_id=$deptID";
    }
    $executeUpdateDepartment = mysqli_query($con, $updateDepartment);
    
    if($executeUpdateDepartment){
        $data['error'] = 0;
    } else {
        $data['error'] = 1;
        if(DEBUG){
            $data['error_txt'] = "executeUpdateDepartment error: " . mysqli_error($con);
        } else {
            $data['error_txt'] = "executeUpdateDepartment query failed.";
        }
    }
}
echo json_encode($data);
?>