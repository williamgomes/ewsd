<?php
include ('../../config/config.php');

$userID = 0;
$type = "";
$status = '';
$data = array();
$data['error'] = 0;
$data['error_txt'] = '';

extract($_POST);

if($userID > 0){
    if($status == "checked"){
        $updateUser = "UPDATE users SET status='active' WHERE user_id=$userID";
    } else {
        $updateUser = "UPDATE users SET status='inactive' WHERE user_id=$userID";
    }
    $executeUpdateUser = mysqli_query($con, $updateUser);
    
    if($executeUpdateUser){
        $data['error'] = 0;
    } else {
        $data['error'] = 1;
        if(DEBUG){
            $data['error_txt'] = "executeUpdateUser error: " . mysqli_error($con);
        } else {
            $data['error_txt'] = "executeUpdateUser query failed.";
        }
    }
}
echo json_encode($data);
?>