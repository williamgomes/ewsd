<!-- END FOOTER -->
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="<?php echo baseUrl('admin/'); ?>assets/plugins/respond.min.js"></script>
<script src="<?php echo baseUrl('admin/'); ?>assets/plugins/excanvas.min.js"></script> 
<![endif]-->
<script src="<?php echo baseUrl('admin/'); ?>assets/plugins/jquery-1.10.2.min.js" type="text/javascript"></script>
<script src="<?php echo baseUrl('admin/'); ?>assets/plugins/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
<script src="<?php echo baseUrl('admin/'); ?>assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?php echo baseUrl('admin/'); ?>assets/custom.scripts.js" type="text/javascript"></script>

<script src="<?php echo baseUrl('admin/'); ?>assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="<?php echo baseUrl('admin/'); ?>assets/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="<?php echo baseUrl('admin/'); ?>assets/plugins/jquery.cokie.min.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo baseUrl('admin/'); ?>assets/plugins/fuelux/js/spinner.min.js"></script>
<script src="<?php echo baseUrl('admin/'); ?>assets/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
<!-- END JAVASCRIPTS -->

<script src="<?php echo baseUrl();?>admin/assets/plugins/bootstrap-switch/static/js/bootstrap-switch.min.js" type="text/javascript"></script>