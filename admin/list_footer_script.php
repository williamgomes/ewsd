<!-- END FOOTER -->
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="<?php echo baseUrl('admin/'); ?>assets/plugins/respond.min.js"></script>
<script src="<?php echo baseUrl('admin/'); ?>assets/plugins/excanvas.min.js"></script> 
<![endif]-->
<script src="<?php echo baseUrl('admin/'); ?>assets/plugins/jquery-1.10.2.min.js" type="text/javascript"></script>
<script src="<?php echo baseUrl('admin/'); ?>assets/plugins/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
<script src="<?php echo baseUrl('admin/'); ?>assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo baseUrl('admin/'); ?>assets/plugins/fuelux/js/spinner.min.js"></script>
<script src="<?php echo baseUrl('admin/'); ?>assets/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>

<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="<?php echo baseUrl('admin/'); ?>assets/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="<?php echo baseUrl('admin/'); ?>assets/plugins/data-tables/jquery.dataTables.js"></script>
<script type="text/javascript" src="<?php echo baseUrl('admin/'); ?>assets/plugins/data-tables/DT_bootstrap.js"></script>

<!-- END PAGE LEVEL PLUGINS -->

<!-- BEGIN PAGE LEVEL SCRIPTS -->

<script src="<?php echo baseUrl('admin/'); ?>assets/scripts/table-advanced.js"></script>
<script src="<?php echo baseUrl('admin/'); ?>assets/scripts/table-managed.js"></script>
<script src="<?php echo baseUrl('admin/'); ?>assets/scripts/table-editable.js"></script>
<script src="<?php echo baseUrl('admin/'); ?>assets/scripts/table-ajax.js"></script>



<script src="<?php echo baseUrl('admin/'); ?>assets/scripts/app.js"></script>
<script src="<?php echo baseUrl('admin/'); ?>assets/scripts/portfolio.js"></script>
<script>
    jQuery(document).ready(function() {
        App.init();
        TableManaged.init();
        TableEditable.init();
    });

</script>
<script>
jQuery(document).ready(function() {    
   App.init();
   Portfolio.init();
});
</script>

