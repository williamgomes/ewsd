<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link href="<?php echo baseUrl('admin/');?>assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo baseUrl('admin/');?>assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo baseUrl('admin/');?>assets/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" type="text/css" href="<?php echo baseUrl('admin/');?>assets/plugins/bootstrap-switch/static/stylesheets/bootstrap-switch-metro.css"/>
<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN PAGE LEVEL STYLES -->

<link rel="stylesheet" type="text/css" href="<?php echo baseUrl('admin/');?>assets/plugins/select2/select2_metro.css"/>
<link rel="stylesheet" href="<?php echo baseUrl('admin/');?>assets/plugins/data-tables/DT_bootstrap.css"/>
<!-- END PAGE LEVEL STYLES -->
<!-- BEGIN THEME STYLES -->

<link href="<?php echo baseUrl('admin/');?>assets/plugins/fancybox/source/jquery.fancybox.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo baseUrl('admin/');?>assets/css/pages/portfolio.css" rel="stylesheet" type="text/css"/>

<link href="<?php echo baseUrl('admin/');?>assets/css/style-metronic.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo baseUrl('admin/');?>assets/css/style.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo baseUrl('admin/');?>assets/css/style-responsive.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo baseUrl('admin/');?>assets/css/plugins.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo baseUrl('admin/');?>assets/css/themes/default.css" rel="stylesheet" type="text/css" id="style_color"/>
<link href="<?php echo baseUrl('admin/');?>assets/css/custom.css" rel="stylesheet" type="text/css"/>

<!-- END THEME STYLES -->

<link rel="shortcut icon" href="favicon.ico"/>