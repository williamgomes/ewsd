<?php
include ('../../config/config.php');
if (!checkAdminLogin()) {
    $link = baseUrl('admin/index.php?err=' . base64_encode('Please login to access admin panel'));
    redirect($link);
}

$name = '';


if(isset($_GET['id']) AND $_GET['id'] != ""){
    $departID = base64_decode($_GET['id']);
    $departSql = "SELECT * FROM department WHERE department_id=$departID";
    $executeDepart = mysqli_query($con, $departSql);
    if($executeDepart){
        $executeDepartObj = mysqli_fetch_object($executeDepart);
        if(isset($executeDepartObj->department_id)){
            $name = $executeDepartObj->department_name;
        }
    } else {
        if (DEBUG) {
            $err = 'executeDepart Error: ' . mysqli_error($con);
        } else {
            $err = "executeDepart query failed.";
        }
    }
}

if (isset($_POST['save']) AND $_POST['save'] == 'Submit') {
    extract($_POST);
    
    if(isset($_GET['id']) AND $_GET['id'] != ""){
        
        
        if ($name == '') {
            $err = 'Department Name field is required!!';
        }

        if ($err == '') {

            $updateDepartment = '';
            $updateDepartment .=' department_name = "' . mysqli_real_escape_string($con, $name) . '"';

            $departmentUpdateSql = "UPDATE department SET $updateDepartment WHERE department_id=$departID";
            $departmentUpdateSqlResult = mysqli_query($con, $departmentUpdateSql);
            if ($departmentUpdateSqlResult) {
                $msg = "Department updated successfully.";
                $link = "index.php?msg=" . base64_encode($msg);
                redirect($link);
            } else {
                if (DEBUG) {
                    $err = 'departmentUpdateSqlResult Error: ' . mysqli_error($con);
                } else {
                    $err = "departmentUpdateSqlResult Query failed.";
                }
            }
        }
        
    } else {
        
        if ($name == '') {
            $err = 'Department Name field is required!!';
        } else {
            /* Start :Checking the user already exist or not */
            $CheckSql = "SELECT department_id FROM department WHERE department_name='" . mysqli_real_escape_string($con, $name) . "'";
            $CheckSqlResult = mysqli_query($con, $CheckSql);
            if ($CheckSqlResult) {
                $countCheckSqlResult = mysqli_num_rows($CheckSqlResult);
                $CheckSqlResultRowObj = mysqli_fetch_object($CheckSqlResult);
                if ($countCheckSqlResult > 0) {
                    $err = '(<b>' . $name . '</b>) already exist in databse ';
                }
                mysqli_free_result($CheckSqlResult);
            } else {
                if (DEBUG) {
                    $err = 'CheckSqlResult Error: ' . mysqli_error($con);
                } else {
                    $err = "Query failed.";
                }
            }

            /* End :Checking the user already exist or not */
        }



        if ($err == '') {

            $addDepartment = '';
            $addDepartment .=' department_name = "' . mysqli_real_escape_string($con, $name) . '"';

            $departmentInsSql = "INSERT INTO department SET $addDepartment";
            $departmentInsSqlResult = mysqli_query($con, $departmentInsSql);
            if ($departmentInsSqlResult) {
                $msg = "Department created successfully.";
                $link = "index.php?msg=" . base64_encode($msg);
                redirect($link);
            } else {
                if (DEBUG) {
                    $err = 'departmentInsSqlResult Error: ' . mysqli_error($con);
                } else {
                    $err = "departmentInsSqlResult Query failed.";
                }
            }
        }
        
    }
    
    
    

    
}
$base_name = basename(__FILE__);
?>
<!DOCTYPE html>
<!-- 
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.0.3
Version: 1.5.5
Author: KeenThemes
Website: http://www.keenthemes.com/
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->
    <head>
        <meta charset="utf-8"/>
        <title><?php echo $config['SITE_NAME']; ?> | Admin Create</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1.0" name="viewport"/>
        <meta content="" name="description"/>
        <meta content="" name="author"/>
        <meta name="MobileOptimized" content="320">
        <?php
        include(basePath('admin/header.php'));
        ?>
    </head>
    <!-- END HEAD -->
    <!-- BEGIN BODY -->
    <body class="page-header-fixed">
        <!-- BEGIN HEADER -->
        <?php
        include '../top_navigation.php';
        ?>
        <!-- END HEADER -->
        <div class="clearfix">
        </div>
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <!-- BEGIN SIDEBAR -->
            <div class="page-sidebar-wrapper">
                <div class="page-sidebar navbar-collapse collapse">
                    <!-- BEGIN SIDEBAR MENU -->
                    <?php
                    include(basePath('admin/sidebar.php'));
                    ?>
                    <!-- END SIDEBAR MENU -->
                </div>
            </div>
            <!-- END SIDEBAR -->
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <div class="page-content">
                    <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
                    <?php
                    include '../template_settings.php';
                    ?>
                    <!-- END STYLE CUSTOMIZER -->
                    <!-- BEGIN PAGE HEADER-->
                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <h3 class="page-title">
                                User Management Module
                            </h3>
                            <ul class="page-breadcrumb breadcrumb">
                                <li class="btn-group">
                                    <button type="button" class="btn blue dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="1000" data-close-others="true">
                                        <span>
                                            Actions
                                        </span>
                                        <i class="fa fa-angle-down"></i>
                                    </button>
                                    <ul class="dropdown-menu pull-right" role="menu">
                                        <li>
                                            <a href="<?php echo baseUrl('admin/'); ?>#">Action</a>
                                        </li>
                                        <li>
                                            <a href="<?php echo baseUrl('admin/'); ?>#">Another action</a>
                                        </li>
                                        <li>
                                            <a href="<?php echo baseUrl('admin/'); ?>#">Something else here</a>
                                        </li>
                                        <li class="divider">
                                        </li>
                                        <li>
                                            <a href="<?php echo baseUrl('admin/'); ?>#">Separated link</a>
                                        </li>
                                    </ul>
                                </li>
                                <li>
                                    <i class="fa fa-home"></i>
                                    <a href="<?php echo baseUrl('admin/dashboard.php'); ?>">Home</a>
                                    <i class="fa fa-angle-right"></i>
                                </li>
                                <li>
                                    <a href="<?php echo baseUrl('admin/department/'); ?>">Department</a>
                                    <i class="fa fa-angle-right"></i>
                                </li>
                                <?php if(isset($_GET['id']) AND $_GET['id'] != ""): ?>
                                <li>
                                    <a href="<?php echo baseUrl('admin/department/add_department.php'); ?>">Edit Department</a>
                                    <i class="fa fa-angle-right"></i>
                                </li>
                                <?php else: ?>
                                <li>
                                    <a href="<?php echo baseUrl('admin/department/add_department.php'); ?>">Add Department</a>
                                    <i class="fa fa-angle-right"></i>
                                </li>
                                <?php endif; ?>
                                
                            </ul>
                            <!-- END PAGE TITLE & BREADCRUMB-->
                        </div>
                    </div>
                    <!-- END PAGE HEADER-->
                    <!-- BEGIN PAGE CONTENT-->
                    <div class="row">


                        <div class="col-md-12 ">
                            <?php if ($err != '') { ?>
                                <div class="alert alert-warning alert-dismissable col-md-9">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                    <?php echo $err; ?>
                                </div>
                            <?php } ?>
                            <?php if ($msg != '') { ?>
                                <div class="alert alert-success alert-dismissable">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                    <?php echo $msg; ?>
                                </div>
                            <?php } ?>
                            <!-- BEGIN SAMPLE FORM PORTLET-->
                            <div class="portlet box green ">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-reorder"></i> Add Department
                                    </div>
                                    <div class="tools">
                                        <a href="<?php echo baseUrl('admin/'); ?>" class="collapse"></a>
                                        <a href="<?php echo baseUrl('admin/'); ?>#portlet-config" data-toggle="modal" class="config"></a>
                                        <a href="<?php echo baseUrl('admin/'); ?>" class="reload"></a>
                                        <a href="<?php echo baseUrl('admin/'); ?>" class="remove"></a>
                                    </div>
                                </div>
                                <div class="portlet-body form">
                                    <?php if(isset($_GET['id']) AND $_GET['id'] != ""): ?>
                                    <form class="form-horizontal" method="post" action="<?php echo baseUrl('admin/department/add_department.php?id=' . $_GET['id']); ?>" role="form">
                                    <?php else: ?>
                                    <form class="form-horizontal" method="post" action="<?php echo baseUrl('admin/department/add_department.php'); ?>" role="form">
                                    <?php endif; ?>
                                    
                                        <div class="form-body">
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Department Name:</label>
                                                <div class="col-md-9">
                                                    <input type="text" name="name" class="form-control required" value="<?php echo $name; ?>" required="required"/>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-actions fluid">
                                            <div class="col-md-offset-3 col-md-9">
                                                <button type="submit" name="save" value="Submit" class="btn green">Submit</button>
                                                <button type="reset" class="btn default">Cancel</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>





                        </div>
                    </div>

                </div>
            </div>
            <!-- END CONTENT -->
        </div>
        <!-- END CONTAINER -->
        <!-- BEGIN FOOTER -->
        <?php
        include basePath('admin/footer.php');
        ?>
        <?php
        include (basePath('admin/footer_script.php'));
        include (basePath('admin/form_includes.php'));
        ?>
<!--        <script src="<?php echo baseUrl();?>admin/assets/custom/admin/admin_create.js"></script>-->
    </body>
    <!-- END BODY -->
</html>