<?php
include ('../../config/config.php');
if (!checkAdminLogin()) {
    $link = baseUrl('admin/index.php?err=' . ('Please login to access admin panel'));
    redirect($link);
}
$departmentArray = array();
$departmentSql = "SELECT * FROM department";
$departmentSqlResult = mysqli_query($con, $departmentSql);
if ($departmentSqlResult) {
    while ($departmentSqlResultRowObj = mysqli_fetch_object($departmentSqlResult)) {
        $departmentArray[] = $departmentSqlResultRowObj;
    }
    mysqli_free_result($departmentSqlResult);
} else {
    if (DEBUG) {
        echo 'departmentSqlResult Error : ' . mysqli_error($con);
    }
}

$base_name = basename(__FILE__);
?>
<!DOCTYPE html>
<!-- 
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.0.3
Version: 1.5.5
Author: KeenThemes
Website: http://www.keenthemes.com/
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->
    <head>
        <meta charset="utf-8"/>
        <title><?php echo $config['SITE_NAME']; ?> | Admin List</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1.0" name="viewport"/>
        <meta content="" name="description"/>
        <meta content="" name="author"/>
        <meta name="MobileOptimized" content="320">
        <?php include(basePath('admin/list_header_style.php')); ?>
        <link rel="shortcut icon" href="<?php echo baseUrl('admin/'); ?>favicon.ico"/>
    </head>
    <!-- END HEAD -->
    <!-- BEGIN BODY -->
    <body class="page-header-fixed">
        <?php
        include'../top_navigation.php';
        ?>
        <!-- BEGIN HEADER -->
        <?php include(basePath('admin/header.php')); ?>
        <!-- END HEADER -->
        <div class="clearfix">
        </div>
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <!-- BEGIN SIDEBAR -->
            <div class="page-sidebar-wrapper">
                <div class="page-sidebar navbar-collapse collapse">
                    <!-- BEGIN SIDEBAR MENU -->
                    <?php
                    include (basePath('admin/sidebar.php'));
                    ?>
                    <!-- END SIDEBAR MENU -->
                </div>
            </div>
            <!-- END SIDEBAR -->
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <div class="page-content">
                    <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
                    <?php
                    include '../template_settings.php';
                    ?>
                    <!-- END STYLE CUSTOMIZER -->
                    <!-- BEGIN PAGE HEADER-->
                    <div class="row">
                        <div class="col-md-12">
                            <?php if ($err != '') { ?>
                                <div class="alert alert-warning alert-dismissable col-md-9">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                    <?php echo $err; ?>
                                </div>
                            <?php } ?>
                            <?php if ($msg != '') { ?>
                                <div class="alert alert-success alert-dismissable">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                    <?php echo $msg; ?>
                                </div>
                            <?php } ?>
                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <h3 class="page-title">
                                Department Module
                            </h3>
                            <ul class="page-breadcrumb breadcrumb">
                                <li class="btn-group">
                                    <button type="button" class="btn blue dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="1000" data-close-others="true">
                                        <span>
                                            Actions
                                        </span>
                                        <i class="fa fa-angle-down"></i>
                                    </button>
                                    <ul class="dropdown-menu pull-right" role="menu">
                                        <li>
                                            <a href="#">Action</a>
                                        </li>
                                        <li>
                                            <a href="#">Another action</a>
                                        </li>
                                        <li>
                                            <a href="#">Something else here</a>
                                        </li>
                                        <li class="divider">
                                        </li>
                                        <li>
                                            <a href="#">Separated link</a>
                                        </li>
                                    </ul>
                                </li>
                                <li>
                                    <i class="fa fa-home"></i>
                                    <a href="<?php echo baseUrl('admin/dashboard.php'); ?>">Home</a>
                                    <i class="fa fa-angle-right"></i>
                                </li>
                                <li>
                                    <a href="<?php echo baseUrl('admin/department/'); ?>">Department</a>
                                    <i class="fa fa-angle-right"></i>
                                </li>
                            </ul>
                            <!-- END PAGE TITLE & BREADCRUMB-->
                        </div>
                    </div>
                    <!-- END PAGE HEADER-->
                    <!-- BEGIN PAGE CONTENT-->
                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN EXAMPLE TABLE PORTLET-->
                            <div class="portlet box light-grey">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-globe"></i>Organiser List
                                    </div>
                                    <div class="tools">
                                        <a href="javascript:;" class="collapse"></a>
                                        <a href="#portlet-config" data-toggle="modal" class="config"></a>
                                        <a href="javascript:;" class="reload"></a>
                                        <a href="javascript:;" class="remove"></a>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div class="table-toolbar">
                                        <div class="btn-group">
                                            <button id="sample_editable_1_new" class="btn green" onclick="window.location.href = '<?php echo baseUrl('admin/department/add_department.php'); ?>'">
                                                Add New <i class="fa fa-plus"></i>
                                            </button>
                                        </div>
                                        <div class="btn-group pull-right">
                                            <button class="btn dropdown-toggle" data-toggle="dropdown">Tools <i class="fa fa-angle-down"></i>
                                            </button>
                                            <ul class="dropdown-menu pull-right">
                                                <li>
                                                    <a href="#">Print</a>
                                                </li>
                                                <li>
                                                    <a href="#">Save as PDF</a>
                                                </li>
                                                <li>
                                                    <a href="#">Export to Excel</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <table class="table table-striped table-bordered table-hover table-full-width" id="sample_2">
                                        <thead>
                                            <tr>

                                                <th>
                                                    Name
                                                </th>
                                                <th>
                                                    Status
                                                </th>
                                                <th>
                                                    Action
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $departmentArrayCounter = count($departmentArray);
                                            if ($departmentArrayCounter > 0):
                                                ?>
                                                <?php for ($i = 0; $i < $departmentArrayCounter; $i++): ?>

                                                    <tr class="odd gradeX">

                                                        <td>
                                                            <?php echo $departmentArray[$i]->department_name; ?>

                                                        </td>
                                                        <td>
                                                            <div class="make-switch switch-small">
                                                                <?php if($departmentArray[$i]->status == 'active'): ?>
                                                                <input id="statusChecker<?php echo $departmentArray[$i]->department_id; ?>" checked="checked" type="checkbox" class="toggle" onchange="changeDeptStatus(<?php echo $departmentArray[$i]->department_id; ?>);"/>
                                                                <?php else: ?>
                                                                <input id="statusChecker<?php echo $departmentArray[$i]->department_id; ?>" type="checkbox" class="toggle" onchange="changeDeptStatus(<?php echo $departmentArray[$i]->department_id; ?>);"/>
                                                                <?php endif; ?>
                                                            </div>
                                                        </td>
                                                        <td class="center">
                                                            <a style="margin-left: 10px;" href="add_department.php?id=<?php echo base64_encode($departmentArray[$i]->department_id); ?>" class="margin-right-10"><i class="fa fa-pencil"></i> </a>

                                                        </td>
                                                    </tr>
                                                <?php endfor; /* $i=0; i<$adminArrayCounter; $++  */ ?>
                                            <?php endif; /* count($adminArray) > 0 */ ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php include(basePath('admin/footer.php')); ?>
        <?php include(basePath('admin/footer_script.php')); ?>
        <?php include (basePath('admin/list_includes.php')); ?>

<script src="<?php echo baseUrl();?>admin/assets/custom/admin/index.js"></script>
    </body>
    <!-- END BODY -->
</html>