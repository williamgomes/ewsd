<?php
include ('../../config/config.php');
if (!checkAdminLogin()) {
    $link = baseUrl('admin/index.php?err=' . base64_encode('Please login to access admin panel'));
    redirect($link);
}

$full_name = '';
$address = '';
$phone = '';
$dob = '';
$experience = '';
$username = '';
$password = '';
$conf_password = '';

if (isset($_POST['save']) AND $_POST['save'] == 'Submit') {
    
    extract($_POST);

    if ($full_name == '') {
        $err = 'Name field is required!!';
    } elseif ($address == '') {
        $err = 'Address field is required!!';
    } elseif ($phone == '') {
        $err = 'Phone field is required';
    } elseif ($dob == '') {
        $err = 'Date of Birth field is required!!';
    } elseif ($username == '') {
        $err = 'Date of Birth field is required!!';
    } elseif ($password == '') {
        $err = 'Date of Birth field is required!!';
    } elseif ($conf_password == '') {
        $err = 'Confirm password  field is required!!';
    } elseif ($conf_password != $password) {
        $err = 'Confirm Password should match with Admin password  !!';
    } else {
        /* Start :Checking the user already exist or not */
        $CheckSql = "SELECT user_id FROM users WHERE user_name='" . mysqli_real_escape_string($con, $username) . "' AND role='presenter'";
        $CheckSqlResult = mysqli_query($con, $CheckSql);
        if ($CheckSqlResult) {
            $countCheckSqlResult = mysqli_num_rows($CheckSqlResult);
            $CheckSqlResultRowObj = mysqli_fetch_object($CheckSqlResult);
            if ($countCheckSqlResult > 0) {
                $err = '(<b>' . $username . '</b>) already exist in databse ';
            }
            mysqli_free_result($CheckSqlResult);
        } else {
            if (DEBUG) {
                $err = 'adminCheckSqlResult Error: ' . mysqli_error($con);
            } else {
                $err = "Query failed.";
            }
        }

        /* End :Checking the user already exist or not */
    }
    
    
    
    if ($err == '') {

        $secured_password = md5($password);
        $addUser = '';
        $addUser .=' user_name = "' . mysqli_real_escape_string($con, $username) . '"';
        $addUser .=', pass = "' . mysqli_real_escape_string($con, $secured_password) . '"';
        $addUser .=', role = "presenter"';
        $addUser .=', status = 1';

        $userInsSql = "INSERT INTO users SET $addUser";
        $userInsSqlResult = mysqli_query($con, $userInsSql);
        if ($userInsSqlResult) {
            
            $userID = mysqli_insert_id($con);
            
            $addPerson = '';
            $addPerson .=' name = "' . mysqli_real_escape_string($con, $full_name) . '"';
            $addPerson .=', address = "' . mysqli_real_escape_string($con, $address) . '"';
            $addPerson .=', phone = "' . mysqli_real_escape_string($con, $phone) . '"';
            $addPerson .=', dob ="' . mysqli_real_escape_string($con, $dob) . '"';
            
            $personInsSql = "INSERT INTO person SET $addPerson";
            $personInsSqlResult = mysqli_query($con, $personInsSql);
            if ($personInsSqlResult) {

                $personID = mysqli_insert_id($con);

                $addAll = '';
                $addAll .=' person_id = "' . mysqli_real_escape_string($con, $personID) . '"';
                $addAll .=', user_id = "' . mysqli_real_escape_string($con, $userID) . '"';
                $addAll .=', experience = "' . mysqli_real_escape_string($con, $experience) . '"';
                
                $allInsSql = "INSERT INTO presenters SET $addAll";
                $allInsSqlResult = mysqli_query($con, $allInsSql);
                
                if($allInsSqlResult){
                    $msg = "Organiser created successfully.";
                    $link = "index.php?msg=" . base64_encode($msg);
                    redirect($link);
                } else {
                    if(DEBUG){
                        $err = "allInsSqlResult error: " . mysqli_error($con);
                    } else {
                        $err = "allInsSqlResult query failed.";
                    }
                }
            } else {
                if (DEBUG) {
                    $err = 'personInsSqlResult Error: ' . mysqli_error($con);
                } else {
                    $err = "personInsSqlResult Query failed.";
                }
            }
            
        } else {
            if (DEBUG) {
                $err = 'userInsSqlResult Error: ' . mysqli_error($con);
            } else {
                $err = "userInsSqlResult Query failed.";
            }
        }
    }
}
$base_name = basename(__FILE__);
?>
<!DOCTYPE html>
<!-- 
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.0.3
Version: 1.5.5
Author: KeenThemes
Website: http://www.keenthemes.com/
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->
    <head>
        <meta charset="utf-8"/>
        <title><?php echo $config['SITE_NAME']; ?> | Admin Create</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1.0" name="viewport"/>
        <meta content="" name="description"/>
        <meta content="" name="author"/>
        <meta name="MobileOptimized" content="320">
        <?php
        include(basePath('admin/header.php'));
        ?>
    </head>
    <!-- END HEAD -->
    <!-- BEGIN BODY -->
    <body class="page-header-fixed">
        <!-- BEGIN HEADER -->
        <?php
        include '../top_navigation.php';
        ?>
        <!-- END HEADER -->
        <div class="clearfix">
        </div>
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <!-- BEGIN SIDEBAR -->
            <div class="page-sidebar-wrapper">
                <div class="page-sidebar navbar-collapse collapse">
                    <!-- BEGIN SIDEBAR MENU -->
                    <?php
                    include(basePath('admin/sidebar.php'));
                    ?>
                    <!-- END SIDEBAR MENU -->
                </div>
            </div>
            <!-- END SIDEBAR -->
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <div class="page-content">
                    <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
                    <?php
                    include '../template_settings.php';
                    ?>
                    <!-- END STYLE CUSTOMIZER -->
                    <!-- BEGIN PAGE HEADER-->
                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <h3 class="page-title">
                                User Management Module
                            </h3>
                            <ul class="page-breadcrumb breadcrumb">
                                <li class="btn-group">
                                    <button type="button" class="btn blue dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="1000" data-close-others="true">
                                        <span>
                                            Actions
                                        </span>
                                        <i class="fa fa-angle-down"></i>
                                    </button>
                                    <ul class="dropdown-menu pull-right" role="menu">
                                        <li>
                                            <a href="<?php echo baseUrl('admin/'); ?>#">Action</a>
                                        </li>
                                        <li>
                                            <a href="<?php echo baseUrl('admin/'); ?>#">Another action</a>
                                        </li>
                                        <li>
                                            <a href="<?php echo baseUrl('admin/'); ?>#">Something else here</a>
                                        </li>
                                        <li class="divider">
                                        </li>
                                        <li>
                                            <a href="<?php echo baseUrl('admin/'); ?>#">Separated link</a>
                                        </li>
                                    </ul>
                                </li>
                                <li>
                                    <i class="fa fa-home"></i>
                                    <a href="<?php echo baseUrl('admin/dashboard.php'); ?>">Home</a>
                                    <i class="fa fa-angle-right"></i>
                                </li>
                                <li>
                                    <a href="<?php echo baseUrl('admin/presenter/'); ?>">Presenter</a>
                                    <i class="fa fa-angle-right"></i>
                                </li>
                                <li>
                                    <a href="<?php echo baseUrl('admin/presenter/add_presenter.php'); ?>">Add Presenter</a>
                                    <i class="fa fa-angle-right"></i>
                                </li>
                            </ul>
                            <!-- END PAGE TITLE & BREADCRUMB-->
                        </div>
                    </div>
                    <!-- END PAGE HEADER-->
                    <!-- BEGIN PAGE CONTENT-->
                    <div class="row">


                        <div class="col-md-12 ">
                            <?php if ($err != '') { ?>
                                <div class="alert alert-warning alert-dismissable col-md-9">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                    <?php echo $err; ?>
                                </div>
                            <?php } ?>
                            <?php if ($msg != '') { ?>
                                <div class="alert alert-success alert-dismissable">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                    <?php echo $msg; ?>
                                </div>
                            <?php } ?>
                            <!-- BEGIN SAMPLE FORM PORTLET-->
                            <div class="portlet box green ">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-reorder"></i> Add Presenter
                                    </div>
                                    <div class="tools">
                                        <a href="<?php echo baseUrl('admin/'); ?>" class="collapse"></a>
                                        <a href="<?php echo baseUrl('admin/'); ?>#portlet-config" data-toggle="modal" class="config"></a>
                                        <a href="<?php echo baseUrl('admin/'); ?>" class="reload"></a>
                                        <a href="<?php echo baseUrl('admin/'); ?>" class="remove"></a>
                                    </div>
                                </div>
                                <div class="portlet-body form">
                                    <form class="form-horizontal" method="post" action="<?php echo baseUrl('admin/presenter/add_presenter.php'); ?>" role="form">
                                        <div class="form-body">
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Name:</label>
                                                <div class="col-md-9">
                                                    <input type="text" name="full_name" class="form-control required" value="<?php echo $full_name; ?>" required="required"/>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Address:</label>
                                                <div class="col-md-9">
                                                    <input type="text" name="address" class="form-control required" value="<?php echo $address; ?>"  required="required" />
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Phone:</label>
                                                <div class="col-md-9">
                                                    <input type="text" name="phone" class="form-control required" value="<?php echo $phone; ?>" required="required"  />
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Date of Birth:</label>
                                                <div class="col-md-9">
                                                    <input class="form-control form-control-inline input-medium date-picker" data-date-format="yyyy-mm-dd" size="16" type="text" value="" name="dob" required="required">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Experience:</label>
                                                <div class="col-md-9">
                                                    <input type="text" name="experience" class="form-control required" value="<?php echo $experience; ?>"  />
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Login Username:</label>
                                                <div class="col-md-9">
                                                    <input type="text" name="username" class="form-control required" value="<?php echo $username; ?>" required="required"  />
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Password:</label>
                                                <div class="col-md-9">
                                                    <div class="input-group">
                                                        <input type="password" name="password" class="form-control required" required="required">
                                                        <span class="input-group-addon">
                                                            <i class="fa fa-user"></i>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Confirm Password:</label>
                                                <div class="col-md-9">
                                                    <div class="input-group">
                                                        <input type="password" name="conf_password" class="form-control required" required="required">
                                                        <span class="input-group-addon">
                                                            <i class="fa fa-user"></i>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-actions fluid">
                                            <div class="col-md-offset-3 col-md-9">
                                                <button type="submit" name="save" value="Submit" class="btn green">Submit</button>
                                                <button type="reset" class="btn default">Cancel</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>





                        </div>
                    </div>

                </div>
            </div>
            <!-- END CONTENT -->
        </div>
        <!-- END CONTAINER -->
        <!-- BEGIN FOOTER -->
        <?php
        include basePath('admin/footer.php');
        ?>
        <?php
        include (basePath('admin/footer_script.php'));
        include (basePath('admin/form_includes.php'));
        ?>
<!--        <script src="<?php echo baseUrl();?>admin/assets/custom/admin/admin_create.js"></script>-->
    </body>
    <!-- END BODY -->
</html>