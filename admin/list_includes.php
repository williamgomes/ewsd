
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="<?php echo baseUrl('admin/'); ?>assets/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="<?php echo baseUrl('admin/'); ?>assets/plugins/data-tables/jquery.dataTables.js"></script>
<script type="text/javascript" src="<?php echo baseUrl('admin/'); ?>assets/plugins/data-tables/DT_bootstrap.js"></script>

<!-- END PAGE LEVEL PLUGINS -->

<!-- BEGIN PAGE LEVEL SCRIPTS -->




<script type="text/javascript" src="<?php echo baseUrl('admin/'); ?>assets/plugins/jquery-mixitup/jquery.mixitup.min.js"></script>
<script type="text/javascript" src="<?php echo baseUrl('admin/'); ?>assets/plugins/fancybox/source/jquery.fancybox.pack.js"></script>

<script src="<?php echo baseUrl('admin/'); ?>assets/scripts/app.js"></script>
<script src="<?php echo baseUrl('admin/'); ?>assets/scripts/table-advanced.js"></script>
<script src="<?php echo baseUrl('admin/'); ?>assets/scripts/portfolio.js"></script>
<script>
    jQuery(document).ready(function() {
        App.init();
        TableAdvanced.init();
    });
</script>




