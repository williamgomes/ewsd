<?php include("header.php"); ?>
      
    <!-- Being Page Title -->
    <div class="container">
        <div class="page-title clearfix">
            <div class="row">
                <div class="col-md-12">
                    <h6><a href="index.php">Home</a></h6>
                    <h6><span class="page-active">Edit Account</span></h6>
                </div>
            </div>
        </div>
    </div>
<?php
    if(isset($_POST['update_info'])){ ?>
        <div class="container">
        <div class="page-title clearfix">
            <div class="row">
                <div class="col-md-12">
                    <h6>
                        <?php
                        $role= $_SESSION['role'];
                        $table = $role.'s';

                         $sql = "SELECT person_id FROM {$table} WHERE user_id = ".$_SESSION['id'];
                        $result     = mysql_query($sql);
                        $row    = mysql_fetch_array($result);
                        $person_id = $row['person_id'];

                        $user_name=$_POST['user_name'];
                        $email=$_POST['email'];
                        $password=md5($_POST['password']);
                        //user table data
                        

                        //This is the directory where images will be saved
                        $target = "upload/";
                        $target = $target . basename( $_FILES['photo']['name']);
                        $photo=($_FILES['photo']['name']);


                        $name=$_POST['name'];
                        $address=$_POST['address'];
                        $phone=$_POST['phone'];
                        $dob=$_POST['dob'];
                        //----person table data

                        if($_SESSION['role'] == 'delegate') {
                        $study=$_POST['study'];
                        //---delegates table data..
                        }

                        //Writes the information to the database table useer
                        $sql = "UPDATE users SET user_name = '{$user_name}', email = '{$email}', pass = '{$password}' WHERE user_id =".$_SESSION['id'];
                        
                        $insert_user     = mysql_query($sql);
                        if($insert_user){
                            //Writes the information to the database table useer
                            $sql = "UPDATE person SET name = '{$name}', photo = '{$photo}', address = '{$address}', phone = '{$phone}', dob = '{$dob}' WHERE person_id = ".$person_id;
                            
                            $insert_person     = mysql_query($sql);

                            if($insert_person){
                                if($role == 'delegate'){
                                    $sql = "UPDATE {$table} SET study = '{$study}' WHERE user_id=".$_SESSION['id'];
                                    $result     = mysql_query($sql);
                                }

                                
                                 echo "Congratulation...!!! Your Data successfully updated..";
                                

                                if($photo!= ''){
                                    if(move_uploaded_file($_FILES['photo']['tmp_name'], $target))
                                    {
                                    
                                    //Tells you if its all ok
                                    echo "Your photo ". basename( $_FILES['photo']['name']). " has been uploaded.";
                                    }
                                    else {
                                    
                                    //Gives and error if its not
                                    echo "Sorry, there was a problem uploading your photo.";
                                    }
                                }
                            }else{
                                echo "Could not store into the database". mysql_error();
                            }
                        }else{
                            echo "Could not store into the database". mysql_error();
                        }
                        ?>
                        </h6>
                </div>
            </div>
        </div>
    </div>
    <?php
    }
    ?>

    <script type="text/javascript">
    function validateForm()
    {
    var x=document.forms["edit"]["email"].value;
    var atpos=x.indexOf("@");
    var dotpos=x.lastIndexOf(".");
    if (atpos<1 || dotpos<atpos+2 || dotpos+2>=x.length)
      {
      alert("Please enter a valid e-mail address");
      return false;
      }
    }
    </script>

    <?php 
    $sql    = "SELECT * FROM users as u, {$_SESSION['role']}s as r, person as p WHERE u.user_id = {$_SESSION['id']} AND u.user_id = r.user_id AND r.person_id = p.person_id";
    $result = mysql_query($sql, $con);
    if(mysql_num_rows($result) > 0){
        $row    = mysql_fetch_array($result);
    }
    ?>
    <div class="container">
        <div class="row">
            
            <div class="col-md-12">
                <div class="contact-page-content">
                    <div class="contact-heading">
                        <h3>Edit Information:</h3>
                        <p>&nbsp;</p>
                    </div>
                    <form name="edit" class="contact-form clearfix" action="" method="post" onsubmit="return validateForm();"  enctype="multipart/form-data">
                        <p class="full-row">
                            <span class="contact-label">
                                <label for="name-id">Name:</label>
                            </span>
                            <input type="text" id="name-id" name="name" value="<?php echo $row['name']; ?>">
                        </p>
                        
                            <p class="full-row">
                            <span class="contact-label">
                                <label for="name-id">UserName:</label>
                            </span>
                            <input type="text" id="name-id" name="user_name" value="<?php echo $row['user_name']; ?>">
                        </p>    <p class="full-row">
                            <span class="contact-label">
                                <label for="name-id">Email:</label>
                            </span>
                            <input type="text" id="name-id" name="email" value="<?php echo $row['email']; ?>">
                        </p>    
                        <p class="full-row">
                            <span class="contact-label">
                                <label for="name-id">Password:</label>
                            </span>
                            <input type="password" id="name-id" name="password">
                        </p>    
                        <p class="full-row">
                            <span class="contact-label">
                                <label for="name-id">Photo:</label>
                            </span>
                            <input type="file" id="name-id" name="photo" value="<?php echo $row['photo']; ?>">
                        </p>
                        <?php if($_SESSION['role'] == 'delegate') { ?>
                        <p class="full-row"> 
                            <span class="contact-label">
                                <label for="surname-id">Study Level:</label>
                            </span>
                            <div class="input-select">
                                <select name="department" id="cat" class="postform">
                                    <option value="-1">-- select --</option>
                                    <option value="Post Graduation" <?php echo ($row['study']=='Post Graduation'? 'selected=selected' : ''); ?>>Post Graduation</option>
                                    <option value="Graduation" <?php echo ($row['study']=='Graduation'? 'selected=selected' : ''); ?>>Graduation</option>
                                    <option value="Bachelor" <?php echo ($row['study']=='Bachelor'? 'selected=selected' : ''); ?>>Bachelor</option>
                                </select>
                            </div> <!-- /.input-select -->
                        </p>
                        <?php }?>                        
                         <p class="full-row">
                            <span class="contact-label">
                                <label for="name-id">Address:</label>
                            </span>
                            <input type="text" id="name-id" name="address" value="<?php echo $row['address']; ?>">
                        </p>
                         
                          <p class="full-row">
                            <span class="contact-label">
                                <label for="name-id">Phone:</label>
                            </span>
                            <input type="text" id="name-id" name="phone" value="<?php echo $row['phone']; ?>">
                        </p>
                        
                          <p class="full-row">
                            <span class="contact-label">
                                <label for="name-id">DOB:</label>
                            </span>
                            <input type="text" id="name-id" name="dob" value="<?php echo $row['dob']; ?>">
                        </p>

                        </p>

                        </p>
                        <p class="full-row">
                            <input class="mainBtn" type="submit" name="update_info" value="Update">
                        </p>
                    </form>
                </div>
            </div> <!-- /.col-md-7 -->

        </div> <!-- /.row -->
    </div> <!-- /.container -->
<?php include("footer.php"); ?>