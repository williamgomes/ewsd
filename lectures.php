<?php include("header.php"); ?>
    
    <!-- Being Page Title -->
    <div class="container">
        <div class="page-title clearfix">
            <div class="row">
                <div class="col-md-12">
                    <h6><a href="index.php">Home</a></h6>
                    <h6><span class="page-active">Upcoming Lectures</span></h6>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">

            <!-- Here begin Sidebar -->
            <div class="col-md-4">

                <?php include("lecture.php"); ?>

            </div> <!-- /.col-md-4 -->

            <!-- Here begin Main Content -->
            <div class="col-md-8">
                <div class="row">
                    <?php 
                    $sql    = "SELECT lecture_id,title,date,start_time,overview,room_name FROM lectures,rooms WHERE lectures.room_id = rooms.room_id LIMIT 6";
                    $result = mysql_query($sql, $con);
                    while ($row    = mysql_fetch_array($result)){
                        if(date ('Y m d H i s',strtotime($row['date'])) >= date('Y m d H i s')) {
                        ?>
                        <div class="col-md-6">
                            <div class="grid-event-item">
                                <div class="grid-event-header">
                                    <span class="event-place small-text"><i class="fa fa-globe"></i><?php echo $row['room_name']; ?></span>
                                    <span class="event-date small-text"><i class="fa fa-calendar-o"></i><?php echo date ('F d, Y',strtotime($row['date'])); ?></span>
                                </div>
                                <div class="box-content-inner">
                                    <h5 class="event-title"><a href="lecture-single.php?lecture_id=<?php echo $row['lecture_id'] ?>"><?php echo $row['title'] ?></a></h5>
                                    <p><?php echo mb_substr($row['overview'],0,200)." ... "; ?><a href="lecture-single.php?lecture_id=<?php echo $row['lecture_id'] ?>">View Details &rarr;</a></p>
                                </div>
                            </div> <!-- /.grid-event-item -->
                        </div> <!-- /.col-md-6 -->
                    <?php 
                        }
                    } ?>
                </div> <!-- /.row -->
            </div> <!-- /.col-md-8 -->
    
        </div> <!-- /.row -->
    </div> <!-- /.container -->
<?php include("footer.php"); ?>