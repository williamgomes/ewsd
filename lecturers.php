<div class="widget-main">
    <div class="widget-main-title">
        <h4 class="widget-title">Top Professors</h4>
    </div>
    <div class="widget-inner">
    <?php 
        $sql    = "SELECT name,photo,experience FROM person,presenters
                    WHERE presenters.presenter_id = person.person_id LIMIT 3";
        $result = mysql_query($sql, $con);
        while ($row    = mysql_fetch_array($result)){ ?>
            <div class="prof-list-item clearfix">
               <div class="prof-thumb">
                    <img src="upload/<?php echo $row['photo']; ?>" alt="<?php echo $row['name']; ?>">
                </div> <!-- /.prof-thumb -->
                <div class="prof-details">
                    <h5 class="prof-name-list"><?php echo $row['name']; ?></h5>
                    <p class="small-text"><?php echo $row['experience']; ?></p>
                </div> <!-- /.prof-details -->
            </div> <!-- /.prof-list-item -->
        <?php } ?>
    </div> <!-- /.widget-inner -->
</div> <!-- /.widget-main -->