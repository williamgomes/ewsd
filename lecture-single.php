<?php include("header.php"); ?>
    <!-- Being Page Title -->
    <?php if(isset($_GET['lecture_id'])) {
        $lecture_id = $_GET['lecture_id'];
    }else if (isset($_GET['apply'])){ 
        $lecture_id = $_GET['apply'];
    } ?>
    <?php if(isset($_GET['apply'])){
        $time = date('Y-m-d');
        $delegate_id = $_SESSION['id'];
        $sql = "INSERT INTO lecture_delegate (lecture_id, delegate_id, booking_time)
        VALUES ('{$lecture_id}', '{$delegate_id}','{$time}')" ;
        
        $insert_person     = mysql_query($sql);
        } ?>
    <?php 
    $sql    = "SELECT title,overview, start_time, end_time, date, room_name, capacity, department_name
                FROM lectures, department, rooms
                WHERE lectures.room_id = rooms.room_id
                AND lectures.department_id = department.department_id
                AND lecture_id =".$lecture_id;

    $result = mysql_query($sql, $con);
    $row    = mysql_fetch_array($result);

    $lecture_title = $row['title'];
    $lecture_overview = $row['overview'];
    $lecture_start_time = $row['start_time'];
    $lecture_end_time = $row['end_time'];
    $lecture_date = $row['date'];
    $lecture_room_name = $row['room_name'];
    $lecture_capacity = $row['capacity'];
    $lecture_department_name = $row['department_name'];

    $sql    = "SELECT name,email,phone,address,photo FROM lectures, organisers,person,users
                WHERE lectures.organiser_id = organisers.organiser_id
                AND organisers.person_id = person.person_id
                AND organisers.user_id = users.user_id
                AND lecture_id = ".$lecture_id;

    $result = mysql_query($sql, $con);
    $row    = mysql_fetch_array($result);

    $Organiger_name = $row['name'];
    $Organiger_email = $row['email'];
    $Organiger_phone = $row['phone'];
    $Organiger_address = $row['address'];
    $Organiger_photo = $row['photo'];

    $sql    = "SELECT name,experience,photo FROM lectures,presenters,person
                WHERE lectures.presenter_id = presenters.presenter_id
                AND presenters.person_id = person.person_id
                AND lecture_id = ".$lecture_id;

    $result = mysql_query($sql, $con);
    $row    = mysql_fetch_array($result);

    $presenter_name = $row['name'];
    $presenter_exp = $row['experience'];
    $presenter_photo = $row['photo'];


    ?>
    
    <div class="container">
        <div class="page-title clearfix">
            <div class="row">
                <div class="col-md-12">
                    <h6><a href="index.php">Home</a></h6>
                    <h6><a href="lectures.php">Lecture</a></h6>
                    <h6><span class="page-active"><?php echo $lecture_title; ?></span></h6>
                </div>
            </div>
        </div>
    </div>


    <div class="container">
        <div class="row">

            <!-- Here begin Main Content -->
            <div class="col-md-8">

                <div class="row">
                    <div class="col-md-12">
                        
                        <div class="course-post">
                            <div class="course-details clearfix event-container">
                                <div class="left-event-content">
                                    <div class="event-contact">
                                        <h4>Contact Details</h4>
                                        <ul>
                                            <li><?php echo $Organiger_name; ?></li>
                                            <li><?php echo $Organiger_email; ?></li>
                                            <li><?php echo $Organiger_phone; ?></li>
                                            <li><?php echo $Organiger_address; ?></li>
                                        </ul>
                                    </div>
                                </div> <!-- /.left-event-content -->
                                <div class="right-event-content">
                                    <h3 class="course-post-title"><?php echo $lecture_title; ?></h3>
                                    <span class="event-time"><?php echo date ('F d, Y',strtotime($lecture_date)) ." : ".date ('H:i a',strtotime($lecture_start_time)).' - '.date ('H:i a',strtotime($lecture_end_time)); ?></span>
                                    <p><strong class="dark-text">Room: <?php echo $lecture_room_name; ?></strong></p>
                                    <p><strong class="dark-text">Speaker:</strong> <?php echo $presenter_name; ?> - <?php echo $presenter_exp; ?> </p>
                                    <p><strong class="dark-text">Staff:</strong> Amanda Burls, Amanda Burls, Amanda Burls</p>
                                    <hr>
                                    <p><?php echo $lecture_overview; ?> </p>
                                    <?php
                                    $sql    = "SELECT count(delegate_id) as total from lecture_delegate
                                                where lecture_id =".$lecture_id;
                                    $result = mysql_query($sql, $con);
                                    $row    = mysql_fetch_array($result);
                                    if($row['total'] < $lecture_capacity){
                                        if(isset($_SESSION['login'])){
                                            if($_SESSION['role'] != 'delegate'){
                                                $lecture_id = $lecture_id;
                                                $delegate_id = $_SESSION['id'];
                                                $sql    = "SELECT * FROM lecture_delegate
                                                            WHERE lecture_id = {$lecture_id}
                                                            AND delegate_id = ".$delegate_id;

                                                $result = mysql_query($sql, $con);
                                                $row    = mysql_fetch_array($result);
                                                if(!empty($row)){?>
                                                    <span class="label label-danger">You are already registered</span>
                                                <?php } else if(date ('Y m d H i s',strtotime($lecture_date)) <= date('Y m d H i s')) { ?>
                                                    <span class="label label-danger">Registration period expired</span>
                                                <?php }  else { ?>
                                                <a href="?apply=<?php echo $lecture_id ?>" class="mainBtn pull-right">Apply to this course</a>
                                                <?php }

                                            } else { ?>
                                                        <span class="label label-danger">You are not eligible to apply the lecture</span>
                                                <?php }
                                        } else {?>
                                                <span class="label label-danger">Please login to apply this lecture</span>
                                        <?php }
                                    }else {?>
                                        <span class="label label-danger">No more seats available for this lecture</span>
                                    <?php 
                                    }?>
                                </div>
                            </div> <!-- /.course-details -->
                        </div> <!-- /.course-post -->

                    </div> <!-- /.col-md-12 -->
                </div> <!-- /.row -->


                <div class="row">
                    <div class="col-md-12">
                        <div id="disqus_thread"></div>
                        <script type="text/javascript">
                                /* * * CONFIGURATION VARIABLES: EDIT BEFORE PASTING INTO YOUR WEBPAGE * * */
                                var disqus_shortname = 'esmeth'; // required: replace example with your forum shortname

                                /* * * DON'T EDIT BELOW THIS LINE * * */
                                (function() {
                                    var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
                                    dsq.src = '//' + disqus_shortname + '.disqus.com/embed.js';
                                    (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
                                })();
                        </script>
                        <noscript>Please enable JavaScript to view the <a href="http://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
                        <a href="http://disqus.com" class="dsq-brlink">comments powered by <span class="logo-disqus">Disqus</span></a>
                    </div> <!-- /.col-md-12 -->
                </div> <!-- /.row -->

            </div> <!-- /.col-md-8 -->


            <!-- Here begin Sidebar -->
            <div class="col-md-4">

                <div class="widget-main">
                    <div class="widget-main-title">
                        <h4 class="widget-title">Organiger of this lecture</h4>
                    </div>
                    <div class="widget-inner">
                        <div class="prof-list-item clearfix">
                           <div class="prof-thumb">
                                <img src="upload/<?php echo $Organiger_photo; ?>" alt="<?php echo $Organiger_name; ?>">
                            </div> <!-- /.prof-thumb -->
                            <div class="prof-details">
                                <h5 class="prof-name-list"><?php echo $Organiger_name; ?></h5>
                                <p class="small-text"><?php echo $Organiger_email; ?></p>
                            </div> <!-- /.prof-details -->
                        </div> <!-- /.prof-list-item -->
                    </div> <!-- /.widget-inner -->
                </div> <!-- /.widget-main -->

                <div class="widget-main">
                    <div class="widget-main-title">
                        <h4 class="widget-title">Lecturer of this lecture</h4>
                    </div>
                    <div class="widget-inner">
                        <div class="prof-list-item clearfix">
                           <div class="prof-thumb">
                                <img src="upload/<?php echo $presenter_photo; ?>" alt="<?php echo $presenter_name; ?>">
                            </div> <!-- /.prof-thumb -->
                            <div class="prof-details">
                                <h5 class="prof-name-list"><?php echo $presenter_name; ?></h5>
                                <p class="small-text"><?php echo $presenter_exp; ?></p>
                            </div> <!-- /.prof-details -->
                        </div> <!-- /.prof-list-item -->
                    </div> <!-- /.widget-inner -->
                </div> <!-- /.widget-main -->

                <div class="widget-main">
                    <div class="widget-main-title">
                        <h4 class="widget-title">Staff of this lecture</h4>
                    </div>
                    <?php 
                    $sql    = "SELECT name,photo,responsibility from lecture_staff,staffs,person
                                where lecture_staff.staff_id = staffs.staff_id
                                AND staffs.person_id = person.person_id
                                AND lecture_staff.lecture_id =".$lecture_id;
                    $result = mysql_query($sql, $con);
                    while ( $row    = mysql_fetch_array($result)) { ?>
                        <div class="widget-inner">
                            <div class="prof-list-item clearfix">
                               <div class="prof-thumb">
                                    <img src="upload/<?php echo $row['photo']; ?>" alt="<?php echo $row['name']; ?>">
                                </div> <!-- /.prof-thumb -->
                                <div class="prof-details">
                                    <h5 class="prof-name-list"><?php echo $row['name']; ?></h5>
                                    <p class="small-text"><?php echo $row['responsibility']; ?></p>
                                </div> <!-- /.prof-details -->
                            </div> <!-- /.prof-list-item -->
                        </div> <!-- /.widget-inner -->
                     <?php }
                    ?>

                </div> <!-- /.widget-main -->

            </div> <!-- /.col-md-4 -->
    
        </div> <!-- /.row -->
    </div> <!-- /.container -->
<?php include("footer.php"); ?>