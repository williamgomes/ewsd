 <?php include("header.php"); ?>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="main-slideshow">
                    <div class="flexslider">
                        <ul class="slides">
                            <li>
                                <img src="img/slide1.jpg" />
                                <div class="slider-caption">
                                    <h2><a href="all-lectures.php">Explore your opportunities</a></h2>
                                </div>
                            </li>
                            <li>
                                <img src="img/slide2.jpg" />
                                <div class="slider-caption">
                                    <h2><a href="all-lectures.php">Expend your knowledge</a></h2>
                                </div>
                            </li>
                        </ul> <!-- /.slides -->
                    </div> <!-- /.flexslider -->
                </div> <!-- /.main-slideshow -->
            </div> <!-- /.col-md-12 -->
            
        </div>
    </div>


    <div class="container">
        <div class="row">
            
            <!-- Here begin Main Content -->
            <div class="col-md-8">
                <div class="row">
                    <div class="col-md-12">
                        <div class="widget-item">
                            <h2 class="welcome-text">Welcome to Universe</h2>
                            <p><strong>Welcome to the Universe Public Lectures website.</strong> Public lectures at Newcastle University are free and open to all.</p>
                            <p>Our aim is to offer a programme that will inform, stimulate, entertain, and excite debate. Many of our speakers are leaders in their field and all have something of special interest to convey. Our lectures cover a wide range of disciplines and frequently include key contemporary issues, particularly those that affect our daily lives.</p>
                            <p>All lectures are free and open to all, with spaces allocated on a first-come first-served basis - for more popular talks an overspill room will be in operation.
                            </p>
                        </div> <!-- /.widget-item -->
                    </div> <!-- /.col-md-12 -->
                </div> <!-- /.row -->

                <div class="row">
                    
                    <!-- Show Latest Events List -->
                    <div class="col-md-12">
                        <?php include("lecture.php"); ?>
                    </div> <!-- /.col-md-12 -->
                    
                </div> <!-- /.row -->

            </div> <!-- /.col-md-8 -->
            
            <!-- Here begin Sidebar -->
            <div class="col-md-4">

                <?php include("lecturers.php"); ?>

                <div class="widget-main">
                    <div class="widget-main-title">
                        <h4 class="widget-title">Testimonial</h4>
                    </div>
                    <div class="widget-inner">
                        <div id="slider-testimonials">
                            <ul>
                                <li>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Impedit, quos, veniam optio voluptas hic delectus soluta odit nemo harum <strong class="dark-text">Shannon D. Edwards</strong></p>
                                </li>
                                <li>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Impedit, quos, veniam optio voluptas hic delectus soluta odit nemo harum <strong class="dark-text">Shannon D. Edwards</strong></p>
                                </li>
                                <li>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Impedit, quos, veniam optio voluptas hic delectus soluta odit nemo harum <strong class="dark-text">Shannon D. Edwards</strong></p>
                                </li>
                            </ul>
                            <a class="prev fa fa-angle-left" href=""></a>
                            <a class="next fa fa-angle-right" href=""></a>
                        </div>
                    </div> <!-- /.widget-inner -->
                </div> <!-- /.widget-main -->

            </div>
        </div>
    </div>
<?php include("footer.php"); ?>