<?php include("header.php"); ?>
    
    <!-- Being Page Title -->
    <div class="container">
        <div class="page-title clearfix">
            <div class="row">
                <div class="col-md-12">
                    <h6><a href="index.php">Home</a></h6>
                    <h6><span class="page-active">Lecture Employees</span></h6>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">

            <!-- Here begin Sidebar -->
            <div class="col-md-4">

                <div class="widget-main">
                    <div class="widget-main-title">
                        <h4 class="widget-title">Organiger</h4>
                    </div>
                    <div class="widget-inner">
                        <?php 
                        $sql    = "SELECT organiser_id,name,email,phone,photo FROM organisers,person,users
                                    WHERE organisers.person_id = person.person_id
                                    AND organisers.user_id = users.user_id";

                            $result = mysql_query($sql, $con);
                            while ( $row    = mysql_fetch_array($result)) {?>
                                
                                <div class="prof-list-item clearfix">
                                   <div class="prof-thumb">
                                        <img src="upload/<?php echo $row['photo']; ?>" alt="<?php echo $row['name']; ?>">
                                    </div> <!-- /.prof-thumb -->
                                    <div class="prof-details">
                                        <h5 class="prof-name-list"><a href="lectures-query.php?role=organiser&id=<?php echo $row['organiser_id']; ?>"><?php echo $row['name']; ?></a></h5>
                                        <p class="small-text"><?php echo $row['email']; ?></p>
                                        <p class="small-text"><?php echo $row['phone']; ?></p>
                                    </div> <!-- /.prof-details -->
                                </div> <!-- /.prof-list-item -->
                        <?php } ?>
                    </div> <!-- /.widget-inner -->
                </div> <!-- /.widget-main -->

            </div> <!-- /.col-md-4 -->

            <!-- Here begin Sidebar -->
            <div class="col-md-4">

                <div class="widget-main">
                    <div class="widget-main-title">
                        <h4 class="widget-title">Professors</h4>
                    </div>
                    <div class="widget-inner">
                    <?php 
                        $sql    = "SELECT presenter_id,name,photo,experience FROM person,presenters
                                    WHERE presenters.presenter_id = person.person_id";
                        $result = mysql_query($sql, $con);
                        while ($row    = mysql_fetch_array($result)){ ?>
                            <div class="prof-list-item clearfix">
                               <div class="prof-thumb">
                                    <img src="upload/<?php echo $row['photo']; ?>" alt="<?php echo $row['name']; ?>">
                                </div> <!-- /.prof-thumb -->
                                <div class="prof-details">
                                    <h5 class="prof-name-list"><a href="lectures-query.php?role=presenter&id=<?php echo $row['presenter_id']; ?>"><?php echo $row['name']; ?></a></h5>
                                    <p class="small-text"><?php echo $row['experience']; ?></p>
                                </div> <!-- /.prof-details -->
                            </div> <!-- /.prof-list-item -->
                        <?php } ?>
                    </div> <!-- /.widget-inner -->
                </div> <!-- /.widget-main -->

            </div> <!-- /.col-md-4 -->

            <!-- Here begin Sidebar -->
            <div class="col-md-4">

                <div class="widget-main">
                    <div class="widget-main-title">
                        <h4 class="widget-title">Staff</h4>
                    </div>
                    <?php 
                    $sql    = "SELECT staff_id,name,photo,responsibility from staffs,person
                                where staffs.person_id = person.person_id";
                    $result = mysql_query($sql, $con);
                    while ( $row    = mysql_fetch_array($result)) { ?>
                        <div class="widget-inner">
                            <div class="prof-list-item clearfix">
                               <div class="prof-thumb">
                                    <img src="upload/<?php echo $row['photo']; ?>" alt="<?php echo $row['name']; ?>">
                                </div> <!-- /.prof-thumb -->
                                <div class="prof-details">
                                    <h5 class="prof-name-list"><a href="lectures-query.php?role=staff&id=<?php echo $row['staff_id']; ?>"><?php echo $row['name']; ?></a></h5>
                                    <p class="small-text"><?php echo $row['responsibility']; ?></p>
                                </div> <!-- /.prof-details -->
                            </div> <!-- /.prof-list-item -->
                        </div> <!-- /.widget-inner -->
                     <?php }
                    ?>

                </div> <!-- /.widget-main -->

            </div> <!-- /.col-md-4 -->

    
        </div> <!-- /.row -->
    </div> <!-- /.container -->
<?php include("footer.php"); ?>