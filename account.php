<?php include("header.php"); ?>
      
    <!-- Being Page Title -->
    <div class="container">
        <div class="page-title clearfix">
            <div class="row">
                <div class="col-md-12">
                    <h6><a href="index.html">Home</a></h6>
                    <h6><span class="page-active">Registration</span></h6>
                </div>
            </div>
        </div>
    </div>
    <?php 
    $sql    = "SELECT * FROM users as u, {$_SESSION['role']}s as r, person as p WHERE u.user_id = {$_SESSION['id']} AND u.user_id = r.user_id AND r.person_id = p.person_id";
        $result = mysql_query($sql, $con);
        if(mysql_num_rows($result) > 0){
            $row    = mysql_fetch_array($result);
        }
        ?>
    <div class="container">
        <div class="row">
            
            <div class="col-md-12">
                <div class="contact-page-content">
                    <table class="table table-bordered">
                        <tr>
                            <th colspan="2"><center>Profile Information:</center></th>
                        </tr>
                        <tr>
                            <td>Name:</td><td><?php echo $row['name']; ?></td>
                        </tr>
                        <tr>
                            <td>UserName:</td><td><?php echo $row['user_name']; ?></td>
                        </tr>
                        <tr>
                            <td>Email:</td><td><?php echo $row['email']; ?></td>
                        </tr>
                        <tr>
                            <td>Photo:</td><td><img src="upload/<?php echo $row['photo']; ?>" alt="<?php echo $row['name']; ?>" height="100" width="150"></td>
                        </tr>
                        <?php if($_SESSION['role'] == 'delegate') { ?>
                        <tr>
                            <td>Study Level:</td><td><?php echo $row['study']; ?></td>
                        </tr>
                        <?php } else if($_SESSION['role']== 'presenter' ) {?>
                        <tr>
                            <td>Experience:</td><td><?php echo $row['experience']; ?></td>
                        </tr>
                        <?php } else if($_SESSION['role']== 'staff' ) {?>
                        <tr>
                            <td>Responsibility:</td><td><?php echo $row['responsibility']; ?></td>
                        </tr>
                        <?php } ?>
                        <tr>
                            <td>Address:</td><td><?php echo $row['address']; ?></td>
                        </tr>
                        <tr>
                            <td>Phone:</td><td><?php echo $row['phone']; ?></td>
                        </tr>
                        <tr>
                            <td>DOB:</td><td><?php echo $row['dob']; ?></td>
                        </tr>
                        <tr>
                            <td colspan="2"><center><a href="edit-account.php" class="mainBtn">Edit Information</a></center></td>
                        </tr>
                    </table> 
                </div>
            </div> <!-- /.col-md-7 -->

        </div> <!-- /.row -->
    </div> <!-- /.container -->
<?php include("footer.php"); ?>