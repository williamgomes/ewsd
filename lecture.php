
<div class="widget-main">
    <div class="widget-main-title">
        <h4 class="widget-title">Upcoming Lectures</h4>
    </div> <!-- /.widget-main-title -->
    <div class="widget-inner">

        <?php 
        $sql    = "SELECT lecture_id,title,date,start_time,end_time,room_name FROM lectures,rooms WHERE lectures.room_id = rooms.room_id LIMIT 3";
        $result = mysql_query($sql, $con);
            while ($row    = mysql_fetch_array($result)){
                if(date ('Y m d H i s',strtotime($row['date'])) >= date('Y m d H i s')) {
        ?>
                    <div class="event-small-list clearfix">
                        <div class="calendar-small">
                            <span class="s-month"><?php echo date ('M',strtotime($row['date'])); ?></span>
                            <span class="s-date"><?php echo date ('d',strtotime($row['date'])); ?></span>
                        </div>
                        <div class="event-small-details">
                            <h5 class="event-small-title"><a href="lecture-single.php?lecture_id=<?php echo $row['lecture_id'] ?>"><?php echo $row['title'] ?></a></h5>
                            <p class="event-small-meta small-text"><?php echo $row['room_name'].' - '.date ('H:i a',strtotime($row['start_time'])).' - '.date ('H:i a',strtotime($row['end_time'])) ?></p>
                        </div>
                    </div>

            <?php 
                }
            } 
            ?>

    </div> <!-- /.widget-inner -->
</div> <!-- /.widget-main -->